import pandas as pd


def generate_values_in_column(df, column_name):
    """
    Generates values from the given input data frame for the given input column.
    :param df: input data frame
    :param column_name: input column name
    :return: generator object to generate values in the given column
    """
    for i in range(len(df)):
        yield df.loc[i, column_name]


def from_csv(file_path, sep=',', columns=None):
    """
    Read a csv file into a pandas data frame.
    :param file_path: path to csv file.
    :param sep: separator to be used for defining columns.
    :param columns: the names of the column
    :return: contents of the csv file in a pandas data frame.
    """
    if columns:
        return pd.read_csv(file_path, sep=sep, names=columns, header=0)
    else:
        return pd.read_csv(file_path, sep=sep)


def from_xlsx(file_path, dtype=None, names=None):
    """
    Read an excel file into a pandas data frame.
    :param file_path: path to input excel file.
    :param dtype: data type used for converting excel spreadsheet data to the specified type using this parameter.
    :param names: names of the columns to be extracted from the excel spreadsheet.
    :return: contents of the excel file in a pandas data frame.
    """
    return pd.read_excel(file_path, dtype=dtype, names=names)


def df_group_by_attr1_list_attr2(df, attr1, attr2):
    """
    For the input data frame group by the attribute 1 (attr1) and collect values in attribute 2 (attr2). Both attributes
    hold the values of the respective column names. To avoid making attr1 as the index, we do reset_index() after
    converting to pandas data frame.   
    :param df: pandas data frame
    :param attr1: attribute 1 for given pandas data frame df (this denotes the name of a column in df)
    :param attr2: attribute 2 for given pandas data frame df (this denotes the name of a column in df)
    :return: pandas data frame grouped by attribute 1 such that column 1 is attribute 1 and column 2 is a list of values
             of attribute 2.
    """
    return pd.DataFrame(df.groupby(attr1)[attr2].apply(list)).reset_index()
