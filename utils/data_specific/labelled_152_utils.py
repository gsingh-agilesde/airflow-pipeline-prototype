import re


def get_substring_till_first_non_digit(string):
    """
    For the given input string extract and return the substring till the first non digit character. This is required to
    extract the patient id from the file name string for the labelled 152 notes dataset.  To do this we split at dot,
    space and finally any alphabet.
    Note: This does not work for other special characters and is very specific to the usecase of labelled 152 notes.
    :param string: input string representing clinical note file name.
    :return: the required substring representing patient id.
    """
    pattern1 = r"[.]"
    pattern2 = r"\s"
    pattern3 = r"[a-zA-Z]"
    res1 = re.split(pattern1, string)
    res2 = re.split(pattern2, res1[0])
    res3 = re.split(pattern3, res2[0])
    return res3[0]
