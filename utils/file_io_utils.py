import os
import _pickle as pickle
from docx import Document
import json


def from_docx(file_path):
    """
    Read a docx file using the python-docx library's Document class. Additionally, use the normal form KD (NFKD) to
    apply the compatibility decomposition, i.e. replace all compatibility characters with their equivalents.
    :param file_path: path to input docx file.
    :return: string with the the input docx file's contents after applying normalization.
    """
    import unicodedata
    doc = Document(file_path)
    list_paragraphs = []
    for p in doc.paragraphs:
        list_paragraphs.append(p.text)
    string_doc = "\n".join(list_paragraphs)
    return unicodedata.normalize("NFKD", string_doc).encode("ascii", "ignore").decode()


def from_text(file_path, complete_text=False):
    """
    Read a text file and return the list of strings, each string representing a single line of text.
    :param file_path: path to the text file.
    :param complete_text: flag to get text as string instead of list
    :return: a list object with each element as a single line in the text file.
    """
    assert isinstance(complete_text, bool), "complete_text should be a bool"
    with open(file_path, "r") as f:
        if not complete_text:
            return f.readlines()
        else:
            return f.read()


def from_pickle(file_path):
    """
    Read a pickle file using encoding as "bytes".
    :param file_path: path to input pickle file.
    :return: contents of the pickle file.
    """
    with open(file_path, "rb") as f:
        return pickle.load(f, encoding='bytes')


def load_json(file_path):
    """
    Return contents of the json file.
    :param file_path: path to input json file.
    :return: contents of the json file.
    """
    with open(file_path, 'r', encoding='utf-8-sig') as file_data:
        return json.load(file_data)


def to_json(file_path, data):
    """
    Stores the data to a json file at the specified file_path
    :param file_path: path to output json file.
    :param data: ata to be stored in the json file.
    :return: None
    """
    with open(file_path, 'w') as file:
        json.dump(data, file, indent=1)


def to_pickle(file_path, data):
    """
    Store the input data to a pickle file at the specified file_path.
    :param file_path: path to output pickle file.
    :param data: data to be stored in the pickle file.
    :return: None
    """
    with open(file_path, "wb") as f:
        pickle.dump(data, f)


def file_names_from_directory(directory_path):
    """
    Returns a list of strings, each string represents a file name in the given directory_path
    :param directory_path: input path of a directory.
    :return: list of strings, each string represents a file name in the given directory_path
    """
    return os.listdir(directory_path)
