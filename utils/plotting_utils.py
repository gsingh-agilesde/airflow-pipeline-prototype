import matplotlib.pyplot as plt
import seaborn as sns


class Columns:
    """
    This class defines the column names that need to be used for some of the data frames used with some of the functions
    used in this module.
    """
    PROPERTY = "property"
    FREQUENCY = "frequency"


class DefaultPlotProperties:
    """
    This class defines the default plotting properties that may be used for plotting using any of the functions defined
    in this module.
    """
    WIDTH = 40
    HEIGHT = 10
    FONT_SCALE = 1
    BAR_PLOT_COLOR = [0, 0.5, 0.7]
    ALPHA = 0.8
    XTICK_ROTATION = 90
    XTICKS = range(0, 1000, 100)


class PlotProperties:
    """
    This class defines all the properties that can be used for plotting using any of the functions defined in this
    module.
    """


    def __init__(self,
                 width=DefaultPlotProperties.WIDTH,
                 height=DefaultPlotProperties.HEIGHT,
                 font_scale=DefaultPlotProperties.FONT_SCALE,
                 bar_plot_color=DefaultPlotProperties.BAR_PLOT_COLOR,
                 alpha=DefaultPlotProperties.ALPHA,
                 xtick_rotation=DefaultPlotProperties.XTICK_ROTATION,
                 xticks=DefaultPlotProperties.XTICKS):
        self.width = width
        self.height = height
        self.font_scale = font_scale
        self.bar_plot_color = bar_plot_color
        self.alpha = alpha
        self.xtick_rotation = xtick_rotation
        self.xticks = xticks


def plot_top_k_frequency_distribution_for_df_property(property_frequency_df, plot_properties, k=100,
                                                      save_figure_file_path=None, show_figure=True):
    """
    Plots the top k frequencies for the given property values in a horizontal plot.
    :param property_frequency_df: Input data frame with two columns titled "property" and "frequency". The "property"
                                  column defines the property values for which we computed the corresponding
                                  frequencies.
    :param plot_properties: an instance of PlotProperties class.
    :param k: the top k property values for which we plot the figure. Here top k is decided as k property values with
              the highest frequencies. Default value is set to 100. If number of unique property values is less than 100
              then k defaults to the total number of unique property values. 
    :param save_figure_file_path: path to store the plot figure.
    :param show_figure: flag to indicate whether plt.show() should be called.
    :return: instance of figure handle.
    """
    x_labels = property_frequency_df[Columns.PROPERTY]
    n_x_labels = len(x_labels)
    if k > n_x_labels:
        k = n_x_labels
    fig = plt.figure(figsize=(plot_properties.width, plot_properties.height))

    sns.set(font_scale=plot_properties.font_scale)
    h1 = sns.barplot(y=Columns.PROPERTY, x=Columns.FREQUENCY, data=property_frequency_df.head(n=k),
                     color=plot_properties.bar_plot_color, alpha=plot_properties.alpha)
    h1.set_yticklabels(list(x_labels[:k]))
    h1.set_xticks(plot_properties.xticks)
    for item in h1.get_xticklabels():
        item.set_rotation(plot_properties.xtick_rotation)
    y_label = "{}".format(Columns.PROPERTY) if k == n_x_labels else "{} (top {} out of {})".format(Columns.PROPERTY, k,
                                                                                                   n_x_labels)
    plt.xlabel(Columns.FREQUENCY)
    plt.ylabel(y_label)
    if save_figure_file_path:
        plt.savefig(save_figure_file_path)
    if show_figure:
        plt.show()
    return fig
