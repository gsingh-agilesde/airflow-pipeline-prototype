def inc_key_in_dict(dict_, k_):
    """
    Increments the dict_ in-place to update value count for k_.
    :param dict_: dictionary with key as k_ and value as the number of times k_ occurred.
    :param k_: key
    :return:
    """
    if k_ in dict_:
        dict_[k_] += 1
    else:
        dict_[k_] = 1


def add_to_k_to_v_to_count_dict(dict_, k_, v_):
    """
    Updates the dict_ in-place for input co-occurrence k_, v_
    :param dict_: a dictionary with key k_ and value as another dictionary with key v_ and value as the number of
                  k_, v_ co-occurrences
    :param k_: as described above
    :param v_: as described above
    :return:
    """
    # Key is found
    if k_ in dict_:
        v_to_count = dict_[k_]
        inc_key_in_dict(v_to_count, v_)
    else:
        dict_[k_] = {v_: 1}


def sort_dict_by_value(dict_, reverse=False):
    """
    Sorts the input dictionary by value in the order given by the reverse flag.
    :param dict_: input dictionary.
    :param reverse: flag that indicates whether to sort in the reverse order.
    :return: dictionary sorted by value in the order denoted by the reverse flag. By default, the dictionary is sorted
             in ascending order of value.
    """
    return dict(sorted(dict_.items(), key=lambda key_value: key_value[1], reverse=reverse))
