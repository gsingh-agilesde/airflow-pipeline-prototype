import os

PARENT_DIR = os.path.dirname(os.path.realpath(__file__))
DATA_DIR = PARENT_DIR + "/data/"
CONFIGURATION_FILES = PARENT_DIR + "/configuration_files/"


CPT = "cpt"
ICD10 = "icd"
MODIFIER = "mod"


class OutputColumns:
    """
    Stores the column names for the output data frames or csv files that will be generated as part of data
    preprocessing.
    """
    NOTE_ID = "note_id"
    TEXT = "text"
    ICD_LABELS = "icd_labels"
    CPT_LABELS = "cpt_labels"