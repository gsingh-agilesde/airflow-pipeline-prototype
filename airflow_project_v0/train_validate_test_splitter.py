import pandas as pd
from itertools import chain

from sklearn.model_selection import train_test_split

from airflow_project_v0 import DATA_DIR, CPT, ICD10, OutputColumns
from airflow_project_v0.utils import to_pickle, df_group_by_attr1_list_attr2


# Column names used for temporary data frames created here.
__COUNT = "count"
__NUM_LABELS = "num_labels"

# Sizes of Train-Validate-Test split.
# These could be refactored to __init__.py in the future if some other modules need to use these.
# Presently, we keep this local and private.
__OVERALL_TEST_SIZE = 0.4
__VALIDATE_TEST_TEST_SIZE = 0.5
__MIN_COUNT = 5

# Indices of training, validation and testing data splits imposed by the return value of train_validate_test_split
# method below.
__TRAIN_INDEX = 0
__VALIDATE_INDEX = 1
__TEST_INDEX = 2





def train_validate_test_split(list_ids, shuffle=False):
    """
    Splits the existing data points into training, validation and testing sets for performing model training and
    evaluation. For efficiency, the splits are done only on a list of data identifiers and these are stored offline.
    :param list_ids: Takes a list of data identifiers as input.
    :param shuffle: Flag that indicates whether or not to  shuffle when creating the splits using sklearn standard
                    train_test_split method.
    :return: split of input list)ids into three lists -- train_ids, validate_ids and test_ids, one each for training,
             validation and testing respectively.
    """
    temp = train_test_split(list_ids, test_size=__OVERALL_TEST_SIZE, shuffle=shuffle)
    train_ids = temp[0]
    temp2 = train_test_split(temp[1], test_size=__VALIDATE_TEST_TEST_SIZE, shuffle=shuffle)
    validate_ids = temp2[0]
    test_ids = temp2[1]
    return train_ids, validate_ids, test_ids


def get_note_ids(df_1, df_2, index_):
    """
    Extracts note_ids for the particular phase : (training, validation, testing) based on the input index.
    :param df_1: The first data frame denoting the results of performing the split using code_group frequency
                 distribution.
    :param df_2: The second data frame denoting the results of performing the split using code frequency distribution.
    :param index_: Index denoting the particular phase (training, validation, testing) and as imposed by the
                   train_validate_test_split method.
    :return: All note_ids belonging for the particular phase denoted by the index_ variable.
    """
    set_1 = frozenset(chain.from_iterable(df_1[OutputColumns.NOTE_ID].apply(lambda x: x[index_]).values.tolist()))
    set_2 = frozenset(chain.from_iterable(df_2[OutputColumns.NOTE_ID].apply(lambda x: x[index_]).values.tolist()))
    return set_1.union(set_2)


def create_train_validate_test_data_splits(specialty, code_set):
    """
    Based on the selected specialty, this script creates the train, validate and test data splits based on note ids.
    The script saves the note_id values to an output pickle file
    ############
    Requisites:
    ############
    1. Need to create the join between clinical note text and the labels using the join_notes_text_codes_feedback module
       before running this function.
    2. The data files: "*_notes_with_cpt_feedback.csv", "*_notes_with_icd_feedback.csv" must be created before running
       this function.
    ############
    Note:
    ############
    The * here represents the specialty and must be replaced with one of the three options: cardio, ortho or pcp.
    These options are defined under cnn_attention/data_model/data_constants class SpecialtyNames.

    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :return:
    """

    labels = OutputColumns.CPT_LABELS if code_set in CPT else OutputColumns.ICD_LABELS

    note_id_text_length_labels_df = pd.read_csv(DATA_DIR + specialty + "_notes_with_" + code_set + "_feedback.csv",
                                                dtype=object)
    note_id_text_length_labels_df[labels] = note_id_text_length_labels_df[labels].apply(
        lambda codes: frozenset([code for code in codes.split(";")]))

    cpt_labels_group_note_ids_df = df_group_by_attr1_list_attr2(note_id_text_length_labels_df, labels,
                                                                OutputColumns.NOTE_ID)
    cpt_labels_group_note_ids_df[__COUNT] = cpt_labels_group_note_ids_df[OutputColumns.NOTE_ID].apply(lambda x: len(x))
    cpt_labels_group_note_ids_df.sort_values(__COUNT, ascending=False, inplace=True)
    cpt_labels_group_note_ids_df.reset_index(inplace=True, drop=True)

    subset_df = cpt_labels_group_note_ids_df[cpt_labels_group_note_ids_df[__COUNT] >= __MIN_COUNT]
    subset_df[OutputColumns.NOTE_ID] = subset_df[OutputColumns.NOTE_ID].apply(lambda x: train_validate_test_split(x))

    temp_df = cpt_labels_group_note_ids_df[cpt_labels_group_note_ids_df[__COUNT] < __MIN_COUNT]

    temp_df[__NUM_LABELS] = temp_df[labels].apply(lambda x: len(x))
    subset_df_2 = df_group_by_attr1_list_attr2(temp_df, __NUM_LABELS, OutputColumns.NOTE_ID)

    subset_df_2[OutputColumns.NOTE_ID] = subset_df_2[OutputColumns.NOTE_ID].apply(
        lambda l: list(chain.from_iterable(l)))

    mask_train_only = subset_df_2[OutputColumns.NOTE_ID].apply(lambda x: len(x) < __MIN_COUNT)
    note_ids_train_only = set(chain.from_iterable(subset_df_2[mask_train_only][OutputColumns.NOTE_ID].values.tolist()))

    subset_df_2 = subset_df_2[~mask_train_only]
    subset_df_2[OutputColumns.NOTE_ID] = subset_df_2[OutputColumns.NOTE_ID].apply(
        lambda x: train_validate_test_split(x))

    train_note_ids = get_note_ids(subset_df, subset_df_2, __TRAIN_INDEX)
    train_note_ids = train_note_ids.union(note_ids_train_only)
    validate_note_ids = get_note_ids(subset_df, subset_df_2, __VALIDATE_INDEX)
    test_note_ids = get_note_ids(subset_df, subset_df_2, __TEST_INDEX)

    to_pickle(DATA_DIR + specialty + "_" + code_set + "_train_note_ids.pickle", train_note_ids)
    to_pickle(DATA_DIR + specialty + "_" + code_set + "_validate_note_ids.pickle", validate_note_ids)
    to_pickle(DATA_DIR + specialty + "_" + code_set + "_test_note_ids.pickle", test_note_ids)

    # Re-read the original csv file and split the entire csv into train, validate, test csv files.
    note_id_text_length_labels_df = pd.read_csv(DATA_DIR + specialty + "_notes_with_" + code_set + "_feedback.csv")
    train_df = note_id_text_length_labels_df[note_id_text_length_labels_df[OutputColumns.NOTE_ID].isin(train_note_ids)]
    train_df.to_csv(DATA_DIR + specialty + "_" + code_set + "_train.csv", index=False)
    validate_df = note_id_text_length_labels_df[
        note_id_text_length_labels_df[OutputColumns.NOTE_ID].isin(validate_note_ids)]
    validate_df.to_csv(DATA_DIR + specialty + "_" + code_set + "_validate.csv", index=False)
    test_df = note_id_text_length_labels_df[note_id_text_length_labels_df[OutputColumns.NOTE_ID].isin(test_note_ids)]
    test_df.to_csv(DATA_DIR + specialty + "_" + code_set + "_test.csv", index=False)


if __name__ == '__main__':
    # read SpecialtyNames from configuration file

    create_train_validate_test_data_splits("mammography", CPT)
    create_train_validate_test_data_splits("mammography", ICD10)
