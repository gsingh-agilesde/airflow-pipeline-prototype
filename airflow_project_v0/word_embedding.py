import gensim.models.word2vec as w2v
import pandas as pd

from utils.file_io_utils import to_pickle
from airflow_project_v0 import DATA_DIR, OutputColumns


def generate_notes(specialty, code_set, code_descriptions=False):
    """
    Computes the iterator for the data
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :param code_descriptions: True means that we append code descriptions as part of generated notes. False means we do
                              not.
    :return: Yields list of words in a text such that it can be used for downstream embedding model.
    """

    # Use notes only from the selected specialty
    note_id_text_length_labels_df = pd.read_csv(DATA_DIR + specialty + "_notes_with_" + code_set + "_feedback.csv")

    if code_descriptions:
        from data_processing_global import cpt_level_1_code_description_df, cpt_level_2_code_description_df, \
            __DESCRIPTOR
        from cnn_attention.utils import process_note

        cpt_level_1_code_description_df.rename(columns={__DESCRIPTOR: OutputColumns.TEXT}, inplace=True)
        cpt_level_1_code_description_df[OutputColumns.TEXT] = \
            cpt_level_1_code_description_df[OutputColumns.TEXT].apply(lambda x: process_note(x)[0])

        cpt_level_2_code_description_df.rename(columns={__DESCRIPTOR: OutputColumns.TEXT}, inplace=True)
        cpt_level_2_code_description_df[OutputColumns.TEXT] = \
            cpt_level_2_code_description_df[OutputColumns.TEXT].apply(lambda x: process_note(x)[0])

        notes_df = pd.concat([pd.DataFrame(note_id_text_length_labels_df[OutputColumns.TEXT]),
                              pd.DataFrame(cpt_level_1_code_description_df[OutputColumns.TEXT]),
                              pd.DataFrame(cpt_level_2_code_description_df[OutputColumns.TEXT])])
    else:
        notes_df = note_id_text_length_labels_df

    for index, row in notes_df.iterrows():
        yield row[OutputColumns.TEXT].split()


def generate_wv_embeddings(specialty, code_set, embedding_size, min_count, workers, n_iter, code_descriptions=False):
    """
    Computes the embeddings for the input specialty and code_set. The embeddings are stored in the original word2vec,
    i.e., .w2v format as well as .embed format as required for the data_loader.py. Finally, the set of words in the
    vocabulary is returned.
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :param embedding_size: The number of dimensions for embeddings.
    :param min_count: Ignores all words with total frequency lower than this value.
    :param workers: Use these many worker threads to train the model (=faster training with multicore machines).
    :param n_iter: Number of iterations (epochs) over the corpus.
    :param code_descriptions: True means that we append code descriptions as part of generated notes. False means we do
                              not.
    :return: The set of words in the vocabulary of the trained embedding model.
    """
    notes = generate_notes(specialty, code_set, code_descriptions=code_descriptions)
    model = w2v.Word2Vec(size=embedding_size, min_count=min_count, workers=workers, iter=n_iter)
    model.build_vocab(notes)
    print("Training word embeddings for {}, {}".format(specialty, code_set))
    model.train(notes, total_examples=model.corpus_count, epochs=model.iter)

    # Save embeddings in the word2vec model format
    output_file_path = DATA_DIR + specialty + "_" + code_set + "_embeddings.w2v"
    model.save(output_file_path)

    # Vocabulary as a set of words.
    vocab = set(model.wv.vocab.keys())

    # word2vec component from gensim model.
    wv = model.wv

    # Save embeddings in the processed_full.embed format for dataloader.
    output_file_path_processed_embed = DATA_DIR + specialty + "_" + code_set + "_processed_full.embed"
    with open(output_file_path_processed_embed, 'w', encoding="UTF-8") as f:
        for word in vocab:
            line = [word]
            line.extend([str(val) for val in wv.word_vec(word)])
            f.write(" ".join(line) + "\n")

    return vocab


if __name__ == '__main__':
    from airflow_project_v0 import CPT, ICD10
    SPECIALTY = "mammography"

    vocab_cpt = generate_wv_embeddings(SPECIALTY, CPT, 100, 0, 4, 5)
    to_pickle(DATA_DIR + "{}_{}_vocab.pickle".format(SPECIALTY, CPT), vocab_cpt)

    vocab_icd = generate_wv_embeddings(SPECIALTY, ICD10, 100, 0, 4, 5)
    to_pickle(DATA_DIR + "{}_{}_vocab.pickle".format(SPECIALTY, ICD10), vocab_icd)

