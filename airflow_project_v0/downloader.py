import io
import json
import boto3

import pandas as pd

from airflow_project_v0 import CPT, ICD10, MODIFIER, DATA_DIR
from airflow_project_v0.utils import process_note

# get_from_config == Prefix, bucket


class DownloaderS3:

    def __init__(self, bucket_name, specialty_name, prefix_request, prefix_feedback):

        s3 = boto3.resource('s3')
        bucket = s3.Bucket(bucket_name)
        self.specialty_name = specialty_name
        self.bucket_request = bucket.objects.filter(Prefix=prefix_request)
        self.bucket_feedback = bucket.objects.filter(Prefix=prefix_feedback)

    @staticmethod
    def __process_object_summary(object_summary):
        data_in_bytes = object_summary.get()['Body'].read()  # data in the form of bytes array.
        decoded_data = data_in_bytes.decode('utf-8')  # Decode it in 'utf-8' format
        stringio_data = io.StringIO(decoded_data)  # IO module for creating a StringIO object.
        data = stringio_data.readlines()  # Now just read the StringIO obj line by line.
        return list(map(json.loads, data))


    @staticmethod
    def __process_feedback(data_files):
        cpt_code_dict, icd_code_dict, mod_code_dict = dict(), dict(), dict()

        for data in data_files:
            note_id = data.get("id")
            cpt_codes, icd_codes, mod_codes = list(), list(), list()

            for code_dict in data.get('finalized').get('codes'):
                code = str(code_dict.get("codeId")).replace(".", "")
                if code_dict.get("codeSet") == "CPT":
                    cpt_codes.append(code)
                elif code_dict.get("codeSet") == "ICD10":
                    icd_codes.append(code)
                else:
                    mod_codes.append(code)
            cpt_code_dict[note_id] = ";".join(set(cpt_codes))
            icd_code_dict[note_id] = ";".join(set(icd_codes))
            mod_code_dict[note_id] = ";".join(set(mod_codes))
        return {CPT: cpt_code_dict, ICD10: icd_code_dict, MODIFIER: mod_code_dict}


    @staticmethod
    def __process_request(data_files):
        note_text_dict = dict()

        for data in data_files:
            note_id = data.get("id")
            notes = data.get("notes")

            # validation
            assert len(notes) == 1, "Note length is more than 1 for {}".format(note_id)
            note_text_dict[note_id] = process_note(text=notes[0].replace("\\n", "\n").replace("\\r", "\r"))
        return note_text_dict

    @staticmethod
    def __get_data(bucket_prefix, processor):

        json_files_complete = list()
        for object_summary in bucket_prefix:
            if object_summary.key.endswith(".json"):
                json_files = DownloaderS3.__process_object_summary(object_summary=object_summary)
                json_files_complete += json_files
        return processor(data_files=json_files_complete)


    def get_data(self):
        result_cpt = list()
        result_icd = list()

        text_dict = \
            DownloaderS3.__get_data(bucket_prefix=self.bucket_request, processor=DownloaderS3.__process_request)
        feedback_dict = \
            DownloaderS3.__get_data(bucket_prefix=self.bucket_feedback, processor=DownloaderS3.__process_feedback)

        for codeset in (CPT, ICD10):
            for note_id, feedback in feedback_dict.get(codeset).items():
                text = text_dict.get(note_id)
                if codeset == CPT:
                    result_cpt.append((note_id, text, feedback))
                elif codeset == ICD10:
                    result_icd.append((note_id, text, feedback))

        df_cpt = pd.DataFrame(result_cpt, columns=["note_id", "text", "cpt_labels"])
        df_icd = pd.DataFrame(result_icd, columns=["note_id", "text", "icd_labels"])
        df_cpt.to_csv(DATA_DIR + "{}_notes_with_CPT_feedback.csv".format(self.specialty_name))
        df_icd.to_csv(DATA_DIR + "{}_notes_with_ICD10_feedback.csv".format(self.specialty_name))
        return None


if __name__ == "__main__":
    downloader = DownloaderS3(bucket_name='aideo-tech-autocoding-v1', specialty_name="mammography",
                              prefix_feedback="feedbacks/40/7/5/",
                              prefix_request="requests/40/7/5/")
    downloader.get_data()

