import pandas as pd
import _pickle as pickle

from nltk.tokenize import RegexpTokenizer


__TOKENIZER = RegexpTokenizer(r'\w+') # NLTK tokenizer to keep only alphanumeric tokens.



def to_pickle(file_path, data):
    """
    Store the input data to a pickle file at the specified file_path.
    :param file_path: path to output pickle file.
    :param data: data to be stored in the pickle file.
    :return: None
    """
    with open(file_path, "wb") as f:
        pickle.dump(data, f)


def df_group_by_attr1_list_attr2(df, attr1, attr2):
    """
    For the input data frame group by the attribute 1 (attr1) and collect values in attribute 2 (attr2). Both attributes
    hold the values of the respective column names. To avoid making attr1 as the index, we do reset_index() after
    converting to pandas data frame.
    :param df: pandas data frame
    :param attr1: attribute 1 for given pandas data frame df (this denotes the name of a column in df)
    :param attr2: attribute 2 for given pandas data frame df (this denotes the name of a column in df)
    :return: pandas data frame grouped by attribute 1 such that column 1 is attribute 1 and column 2 is a list of values
             of attribute 2.
    """
    return pd.DataFrame(df.groupby(attr1)[attr2].apply(list)).reset_index()


def process_note(text):
    """
    Process input text using above regular expression to capture alphanumeric tokens only. Additionally, we remove
    tokens that are solely numeric.
    :param text: Input clinical note text.
    :return: preprocessed note and mapping of word indices in processed note to their corresponding indices in the
             original note.
    """
    tokens = [t.lower() for t in __TOKENIZER.tokenize(text) if not t.isnumeric()]
    return ' '.join(tokens).replace("_", " ")