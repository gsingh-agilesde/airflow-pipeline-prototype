
import boto3
from botocore.exceptions import ClientError

from airflow_project_v0 import DATA_DIR


# get_from_config == Prefix, bucket


class FileName:
    SPECIALTY_CODESET_TEST = "{}_{}_test.csv"
    SPECIALTY_CODESET_TRAIN = "{}_{}_train.csv"
    SPECIALTY_CODESET_VOCAB = "{}_{}_vocab.pickle"
    SPECIALTY_CODESET_VALIDATE = "{}_{}_validate.csv"
    SPECIALTY_CODESET_EMBEDDING = "{}_{}_processed_full.embed"
    SPECIALTY_CODESET_TEST_NOTES = "{}_{}_test_note_ids.pickle"
    SPECIALTY_CODESET_FEEDBACK = "{}_notes_with_{}_feedback.csv"
    SPECIALTY_CODESET_TRAIN_NOTES = "{}_{}_train_note_ids.pickle"
    SPECIALTY_CODESET_VALIDATE_NOTES = "{}_{}_validate_note_ids.pickle"



class UploaderS3:

    def __init__(self, bucket_name, specialty_name, prefix):

        self.s3_client = boto3.client('s3')

        self.bucket_name = bucket_name
        self.prefix = prefix
        self.specialty_name = specialty_name


    def put_data(self, file_path, override=False):

        # todo: Check if the folder exits before uploading new files,
        #  put and override parameter to pass in teh method call

        proceed = False
        file_name = file_path.split("/")[-1]

        try:
            self.s3_client.get_object(Bucket=self.bucket_name, Key=self.prefix + file_name)
            print("File: {}{} already exists! Override: {}".format(self.bucket_name, self.prefix + file_name, override))
            if override:
                proceed = True

        except ClientError as ex:
            if ex.response['Error']['Code'] == 'NoSuchKey':
                proceed = True
            else:
                print("{}{} has following issue:\n{}\n".format(self.bucket_name, self.prefix, ex.args))

        if proceed:
            try:
                self.s3_client.upload_file(Filename=file_path, Bucket=self.bucket_name, Key=self.prefix + file_name)
                print("File: {}{} uploaded!".format(self.bucket_name, self.prefix + file_name))

            except ClientError as e:
                print("{} couldn't be uploaded due to: \n{}\n".format(file_path, e.args))
                return False
            return True
        else:
            return False


if __name__ == "__main__":

    from airflow_project_v0 import DATA_DIR

    uploader = UploaderS3(bucket_name="sagemaker-us-east-2-046610044696", specialty_name="xray",
                          prefix="sagemaker/pytorch-cnn-attention-{}-v2/".format("xray".replace("_", "-")))

    uploader.put_data(file_path=DATA_DIR + FileName.SPECIALTY_CODESET_TEST_NOTES.format("xray", "CPT"))

