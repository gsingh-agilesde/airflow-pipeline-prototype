##############    Proof Of Concept    ##############

This project is to help with deployment of airflow for proof of concept towards automating the whole pipeline.

Following tasks will be automated in this process:

1. Downloading the Feedback, Request files from relevant S3 bucket for a dataset
2. Creating the intermidiate files required for CNN attention training
3. Uploading the intermideiate files (18 per dataset) to relevant S3 bucket




The Sequence for the tasks are as following (currently):

Step 1: downloader
Step 2: train_validate_test_splitter
Step 3: word_embedding
Step 4: uploader

