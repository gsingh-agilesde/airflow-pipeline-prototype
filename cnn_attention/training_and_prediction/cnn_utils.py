from nltk.corpus import stopwords
import numpy as np
import torch
import json

from cnn_attention import CUDA

from cnn_attention.training_and_prediction.model import pick_model

__OPTIM_EXP_AVG = 'exp_avg'
__OPTIM_EXP_AVG_SQ = 'exp_avg_sq'
__CPU_DEVICE = "cpu"
__STOP_WORDS = set(stopwords.words('english'))


def load_model(model_dir, kernel_size, conv_out_size, model_name, cuda_device_number, dropout, data_iterator,
               label_vocab_itos=None, code_to_desc=None):
    """
    Initiates the model. If {model_dir} is provided then loads the pre-trained model from that directory
    :param model_dir: if loading a saved pre-trained data, then provide the path to saved model directory, else
                          None
    :param kernel_size: convolutional filter size
    :param conv_out_size: length of the output of the conv1D layer
    :param model_name: model to be trained. Currently, only cnn_attention.CNN is supported
    :param cuda_device_number: the cuda device number on which the model should be run.
    :param dropout: The dropout rate, presently only used for embedding layer.
    :param data_iterator: the data iterator object for input data.
    :param label_vocab_itos: index to string for LABEL field. (Used only in Multi-view model.)
    :param code_to_desc: dictionary with key as as code and value as preprocessed description. (Used only in Multi-view
                         model.)
    :return: device and model
    """
    # Device configuration
    device = torch.device(
        CUDA + cuda_device_number if torch.cuda.is_available() else __CPU_DEVICE)
    embedding_vectors = data_iterator.data_fields.TEXT.vocab.vectors
    model = pick_model(model_name, data_iterator.num_labels, kernel_size, conv_out_size, embedding_vectors,
                       model_path=model_dir, dropout=dropout, label_vocab_itos=label_vocab_itos,
                       code_to_desc=code_to_desc).to(device)
    return device, model


def get_code_scores(num_of_codes, output_scores, data_iterator):
    """
    This method selects the top {num_of_codes} based on the prediction scores.
    :param num_of_codes: top number of codes based on the prediction score.
    :param output_scores: output score array containing the scores of all the code labels.
    :param data_iterator: the data iterator object for input data.
    :return: returns the dictionary containing the scores of top {num_of_codes} codes with highest scores
    """
    sorted_out_idx = np.argsort(output_scores)
    out_score = dict()
    for idx, code_idx in enumerate(sorted_out_idx[-num_of_codes:]):
        val = output_scores[code_idx]
        out_score[data_iterator.data_fields.LABEL.vocab.itos[code_idx]] = val
    return out_score


def get_code_to_attention(data_iterator, kernel_size, code_idxs, top_n_attentions, attn_weights, note_data,
                          is_mention=False):
    """
    1. Find the attention weight for the code by querying {attn_weights}.
    2. Get the indices for the top n weighted attentions.
    3. Get the word id from the note_data for each of the top n indices and its next three indices from note_data
    4. Get the corresponding word from the TEXT vocab for each of the word ids.

    :param data_iterator: the data iterator object for input data.
    :param kernel_size: convolutional filter size
    :param code_idxs: indices to code labels
    :param top_n_attentions: The size of the attention for each label. The total size of the attention will be
                                top_n_attention*kernel_size of the convolution layer.
    :param attn_weights: attention weights for a single clinical note.
    :param note_data: text of single clinical note.
    :param is_mention: denotes if we want to run the function in the mentions mode.
                       If the is_mention is set to True, we obtain attention weights dictionary
                       with key as a word index in note and value as the corresponding attention weight.
                       Else we obtain attention weights dictionary with key as a word (not word index)
                       in the note and value as the corresponding attention weight.
    :return : code to the top n attention words with their attention weights dictionary
    """
    code_to_attention = dict()
    for code_idx in code_idxs:
        code = data_iterator.data_fields.LABEL.vocab.itos[code_idx]
        attn_for_code = attn_weights[code_idx]
        attn_for_code_sorted_indices = np.argsort(attn_for_code)[::-1]
        word_weight = dict()
        for word_idx in attn_for_code_sorted_indices:
            if len(word_weight) >= top_n_attentions * kernel_size:
                break
            for itr in range(word_idx, word_idx + kernel_size):
                if itr < attn_for_code.size - 1:
                    word_id = note_data[itr].item()
                    word = data_iterator.data_fields.TEXT.vocab.itos[word_id]
                    if word not in __STOP_WORDS and word != "<pad>":
                        word_id = note_data[itr].item()
                        word = data_iterator.data_fields.TEXT.vocab.itos[word_id]
                        if word not in word_weight.keys():
                            if is_mention:
                                word_weight[itr] = attn_for_code[word_idx]
                            else:
                                word_weight[word] = attn_for_code[word_idx]
        code_to_attention[code] = word_weight
    return code_to_attention


def parameter_reader(model_directory):
    """
    Read the param.json file included in the model folder specifying the various parameters associated with the model.
    :param model_directory: Absolute path to the model folder
    :return: a dictionary of parameter values
    """
    with open(model_directory + "/params.json") as f:
        data = json.load(f)

    __specialty = data.get("specialty")
    __code_set = data.get("code_set")
    __kernel_size = data.get("kernel_size")
    __conv_out_size = data.get("conv_out_size")
    __model_name = data.get("model_name")
    __cuda_device_number = data.get("cuda_device_number")
    __dropout = data.get("dropout")

    return {"specialty": __specialty, "code_set": __code_set, "kernel_size" : __kernel_size,
            "conv_out_size": __conv_out_size, "model_name": __model_name, "cuda_device_number": __cuda_device_number,
            "dropout": __dropout}
