from math import floor
from itertools import chain

import torch
import torch.nn.functional as f
from torch import nn
from torch.nn.init import xavier_uniform_ as xavier_uniform

from cnn_attention import CNN, MULTI_VIEW_CNN, SAVED_MODEL_NAME


class BaseModel(nn.Module):

    def __init__(self, num_labels, embedding_vectors, embed_size, dropout):
        super(BaseModel, self).__init__()
        torch.manual_seed(1337)
        self.num_labels = num_labels
        self.embed_size = embed_size
        self.embed_drop = nn.Dropout(p=dropout)

        # make embedding layer
        self.embed = nn.Embedding(embedding_vectors.size()[0], embed_size, padding_idx=1)
        self.embed.weight.data.copy_(embedding_vectors)
        # self.embed.weight.requires_grad = False


    @staticmethod
    def _get_loss(y_hat, target):
        # calculate the BCE
        loss = f.binary_cross_entropy(y_hat, target)
        return loss


class ConvAttnPool(BaseModel):
    """
    Mullenbach et al., Explainable Prediction of Medical Codes from Clinical Text, Association for Computational
    Linguistics, N18-1100, 1101-1111, 2018.
    """


    def __init__(self, num_labels, kernel_size, conv_out_size, embedding_vectors, embed_size, dropout):
        super(ConvAttnPool, self).__init__(num_labels, embedding_vectors, embed_size, dropout)

        # initialize conv layer as in 2.1
        self.conv = nn.Conv1d(self.embed_size, conv_out_size, kernel_size=kernel_size, padding=floor(kernel_size / 2))
        xavier_uniform(self.conv.weight)

        # context vectors for computing attention as in 2.2
        self.U = nn.Linear(conv_out_size, num_labels)
        xavier_uniform(self.U.weight)

        # final layer: create a matrix to use for the L binary classifiers as in 2.3
        self.final = nn.Linear(conv_out_size, num_labels)
        # TODO: make this into a hyper-parmeter
        xavier_uniform(self.final.weight)


    def forward(self, x, target, desc_data=None, get_attention=True):
        yhat, alpha = self.predict(x)
        loss = self._get_loss(yhat, target)
        return yhat, loss, alpha


    def predict(self, x):
        """
        This method is used to make the predictions for the test data.
        :param x: the data point for which the prediction is to be made
        :return: prediction score and word weights for attention.
        """
        # get embeddings and apply dropout
        x = self.embed(x)
        x = self.embed_drop(x)
        x = x.transpose(1, 2)

        # apply convolution and nonlinearity (tanh)
        H = torch.tanh(self.conv(x).transpose(1, 2))
        # apply attention
        alpha = f.softmax(self.U.weight.matmul(H.transpose(1, 2)), dim=2)
        # document representations are weighted sums using the attention. Can compute all at once as a matmul
        m = alpha.matmul(H)
        # final layer classification
        y = self.final.weight.mul(m).sum(dim=2).add(self.final.bias)

        # final sigmoid to get predictions
        yhat = torch.sigmoid(y)
        return yhat, alpha


class MultiviewConvAttnPool(BaseModel):
    """
    N. Sadoughi et al., Medical code prediction with multi-view convolution and
    description-regularized label-dependent attention, arXiv: 1811.01468, (2018).

    "Presently" (at the time of this writing), the paper has not been published.
    """


    def __init__(self, num_labels, kernel_size, conv_out_size, embedding_vectors, embed_size, dropout,
                 label_vocab_itos=None, code_to_desc=None, is_desc_reg=False, reg_lambda=0.0001):
        """
        :param num_labels: Number of output labels.
        :param kernel_size: Biggest kernel size. Other 3 kernels are computed with offsets as -2, -4 and -6.
        :param conv_out_size: Size of each convolutional output.
        :param embedding_vectors: Input embedding to initialize the embedding matrix. This is done in the super call.
        :param embed_size: Size of embedding dimension.
        :param dropout: Amount of dropout. Technically, Multiview paper does not specify a dropout in any layer.
        :param label_vocab_itos: index to string for LABEL field.
        :param code_to_desc: dictionary with key as as code and value as preprocessed description.
        :param is_desc_reg: if True indicates that we want to include description regularization.
        :param reg_lambda: hyperparameter that weights amount of description regularization.
        """

        super(MultiviewConvAttnPool, self).__init__(num_labels, embedding_vectors, embed_size, dropout)
        if kernel_size % 2 == 0:
            self.add_factor = 1
        else:
            self.add_factor = 0
        self.conv_out_size = conv_out_size
        self.label_vocab_itos = label_vocab_itos
        self.code_to_desc = code_to_desc
        self.is_desc_reg = is_desc_reg
        self.reg_lambda = reg_lambda

        ################################################################################
        ################################# MULTIVIEW CNN ################################
        ################################################################################
        # Define 4 convolution layers for multiview cnn as in section 4.2 of paper.

        self.conv = []
        self.conv.append(nn.Conv1d(in_channels=self.embed_size, out_channels=conv_out_size, kernel_size=kernel_size,
                                   padding=floor(kernel_size / 2)))
        xavier_uniform(self.conv[-1].weight)
        self.conv.append(nn.Conv1d(in_channels=self.embed_size, out_channels=conv_out_size, kernel_size=kernel_size - 2,
                                   padding=floor((kernel_size - 2) / 2)))
        xavier_uniform(self.conv[-1].weight)
        self.conv.append(nn.Conv1d(in_channels=self.embed_size, out_channels=conv_out_size, kernel_size=kernel_size - 4,
                                   padding=floor((kernel_size - 4) / 2)))
        xavier_uniform(self.conv[-1].weight)
        self.conv.append(nn.Conv1d(in_channels=self.embed_size, out_channels=conv_out_size, kernel_size=kernel_size - 6,
                                   padding=floor((kernel_size - 6) / 2)))
        xavier_uniform(self.conv[-1].weight)

        # context vectors for computing attention as in section 4.3 of the paper.
        self.V = nn.Linear(conv_out_size, num_labels)
        xavier_uniform(self.V.weight)

        # Parameters for length-embedding function used in section 4.4, eq. (3) of the paper.
        self.K = nn.Linear(1, num_labels)
        xavier_uniform(self.K.weight)

        # final layer: create a matrix to use for the L binary classifiers used in 4.4, eq. (4) of the paper.
        self.U = nn.Linear(conv_out_size, num_labels)
        xavier_uniform(self.U.weight)
        ################################################################################
        ################################################################################
        ################################################################################

        # TODO: Neural network layers for secondary neural network for description
        #      regularization.
        ################################################################################
        ############################### DESC REGULARIZATION ############################
        ################################################################################
        self.conv_dr = nn.Conv1d(in_channels=self.embed_size, out_channels=conv_out_size, kernel_size=kernel_size,
                                 padding=floor(kernel_size / 2))
        self.max_pool_dr = nn.MaxPool1d(kernel_size=kernel_size - 6, stride=1)


    def forward(self, x, target):
        yhat, alpha = self.predict(x)
        temp_loss = self._get_loss(yhat, target)
        # TODO: Description regularization.
        if self.is_desc_reg and self.label_vocab_itos:
            # loss = temp_loss + ...
            pass
        else:
            loss = temp_loss

        return yhat, loss, alpha


    # TODO: Secondary neural network for desscription regularization.
    def f_of_D(self, target):
        # 1. Based on target, extract the code labels.
        # 2. For all code labels j={1,...,L} extract the preprocessed long descriptions, D_j.
        # 3. Use the same self.embed layer to obtain embeddings for words in long descriptions.
        for val in target:
            for code_ind in chain.from_iterable(val.nonzero().tolist()):
                code = self.label_vocab_itos[code_ind]
                code_desc = self.code_to_desc[code]

        # TODO:
        # Embedding layer, same embedding layer as the one used in the primary multi-view cnn network.
        # Convolutional layer with kernel size as largest kernel size from the MCNN network and with same no. of filters
        # as that of the MCNN network.
        # Spatial Max pooling layer.
        # Dense Layer
        # Sigmoid Layer.


    def predict(self, x):
        # get embeddings
        x_length = x.shape[1] - (x == 1).sum(dim=1)
        x = self.embed(x)
        x = x.transpose(1, 2)

        # -- Multiview CNN defined per section 4.2, Eq. (1)
        chat = torch.zeros((len(self.conv), x.shape[0], x.shape[2] + self.add_factor, self.conv_out_size))
        for i in range(len(self.conv)):
            chat[i, :, :, :] = self.conv[i](x).transpose(1, 2)
        # ------ View pooling.
        C = torch.tanh(torch.max(chat, dim=0)[0])

        # Self-attention computed per section 4.3, Eq. (2)
        # -----attention weights
        alpha = C.matmul(self.V.weight.t())
        # ----- attention weighted output.
        P = C.transpose(1, 2).matmul(alpha)

        # Length embedding function defined per section 4.4, Eq. (3).
        T = self.K(x_length.float().reshape(-1, 1))
        # Final layer defined per section 4.4, Eq. (4)
        y = self.U.weight.mul(P.transpose(1, 2)).sum(dim=2).add(self.U.bias).add(T)
        yhat = torch.sigmoid(y)

        return yhat, alpha


def pick_model(model_name, num_labels, kernel_size, conv_out_size, embedding_vectors, embed_size=100, dropout=0.5,
               model_path=None, label_vocab_itos=None, code_to_desc=None):
    """
    This method selects and initiates a model according to the params of the function.

    NOTE: When a pre-trained model is loaded using the param {model_path},
    the state dictionaries(parameters) of the pre-trained model and model skeleton initiated here should be the same.

    :param model_name: The name of the model to be initialized.
    :param num_labels: number of labels in the dataset
    :param kernel_size: convolutional filter size
    :param conv_out_size: length of the output of the conv1D layer
    :param embedding_vectors: embedding vectors
    :param embed_size: dimension of each embedding vector
    :param dropout: dropout at the embedding layer
    :param model_path: If pre-trained model needs to be loaded then provide the path to the saved model here.
    :param label_vocab_itos: index to string for LABEL field. (Used only in Multi-view model.)
    :param code_to_desc: dictionary with key as as code and value as preprocessed description. (Used only in Multi-view
                         model.)
    :return: initialized model with the given param.
    """
    if model_name == CNN:
        model = ConvAttnPool(num_labels, kernel_size, conv_out_size, embedding_vectors, embed_size, dropout)
    elif model_name == MULTI_VIEW_CNN:
        model = MultiviewConvAttnPool(num_labels, kernel_size, conv_out_size, embedding_vectors, embed_size, dropout,
                                      label_vocab_itos=label_vocab_itos, code_to_desc=code_to_desc)
    else:
        raise ValueError("{} model is not defined. Choose a valid model.".format(str(model_name)))
    if model_path:
        sd = torch.load(model_path + SAVED_MODEL_NAME)
        model.load_state_dict(sd)
    return model
