import os
import time
import uuid

from nltk.corpus import stopwords
import numpy as np
import torch
from tqdm import tqdm

from utils.file_io_utils import to_pickle, to_json

from cnn_attention import CNN, MODEL_DIR, SAVED_MODEL_NAME

from cnn_attention.training_and_prediction.cnn_utils import load_model, get_code_scores, get_code_to_attention

from cnn_attention.data_model.data_constants import SpecialtyNames


class ModelTraining:
    __OPTIM_EXP_AVG = 'exp_avg'
    __OPTIM_EXP_AVG_SQ = 'exp_avg_sq'
    __CPU_DEVICE = "cpu"
    __STOP_WORDS = set(stopwords.words('english'))

    """
    Files required for training a model or test batch of notes with a pre-trained model : 
        1. {specialty}_notes_with_{code_set}_feedback.csv
        2. {specialty}_{code_set}_train.csv
        3. {specialty}_{code_set}_validate.csv
        4. {specialty}_{code_set}_test.csv
        5. {specialty}_{code_set}_vocab.pickle
        6. {specialty}_{code_set}_processed_full.embed
        7. {specialty}_{code_set}_train_note_ids.pickle
        8. {specialty}_{code_set}_validate_note_ids.pickle
        9. {specialty}_{code_set}_test_note_ids.pickle 
    Files required for loading a pre-trained model and getting the predictions for a SINGLE note : 
        1. {specialty}_{code_set}_vocab.pickle
        2. {specialty}_{code_set}_processed_full.embed   
        6. {code_set}_codes_in_{specialty}_specialty.pickle  
    """


    def __init__(self, model_dir, specialty, code_set,
                 kernel_size, conv_out_size, model_name, cuda_device_number,
                 epochs=100, batch_size=32, weight_decay=0, learning_rate=0.00025, early_stopping=False,
                 epsilon=0, patience=0, dropout=0.5, data_iterator=None):
        """
        This class initializes and loads the data and model for training and testing.

        :param model_dir: if loading a saved pre-trained data, then provide the path to saved model directory, else
                          None
        :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class
                          SpecialtyNames.
        :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
        :param kernel_size: convolutional filter size
        :param conv_out_size: length of the output of the conv1D layer
        :param model_name: model to be trained. Currently, only cnn_attention.CNN is supported
        :param cuda_device_number: the cuda device number on which the model should be run.

        :param epochs: number of epochs to train the model for.
        :param batch_size: batch size
        :param weight_decay: weight decay
        :param learning_rate: learning rate
        :param early_stopping: True if early stopping is desired.
        :param epsilon: The amount by which the loss should have changed.
        :param patience: The number of epochs for which the loss has not changed.
        :param dropout: The dropout rate, presently only used for embedding layer.
        :param data_iterator: the data iterator object for input data.
        """
        self.model_dir = model_dir
        self.specialty = specialty
        self.code_set = code_set
        self.kernel_size = kernel_size
        self.conv_out_size = conv_out_size
        self.model_name = model_name
        self.cuda_device_number = cuda_device_number
        self.epochs = epochs
        self.batch_size = batch_size
        self.weight_decay = weight_decay
        self.learning_rate = learning_rate
        self.early_stopping = early_stopping
        self.patience = patience
        self.epsilon = epsilon
        self.dropout = dropout
        self.data_iterator = data_iterator

        self.device, self.model = load_model(self.model_dir, self.kernel_size, self.conv_out_size, self.model_name,
                                             self.cuda_device_number, self.dropout, self.data_iterator)
        if not self.model_dir:
            self.optimizer = torch.optim.Adam(self.model.parameters(),
                                              weight_decay=self.weight_decay,
                                              lr=self.learning_rate)


    def train_model(self, train_batches_gen, validate_batches_gen):
        """
        Trains the model and calculates the train loss, validation loss .
        Saves the model at the {model_dir} directory.
        :param train_batches_gen: data_processing.data_loader.BatchGenerator object for training dataset
        :param validate_batches_gen: data_processing.data_loader.BatchGenerator object for validation dataset
        :return: loss_each_epoch_train, loss_each_epoch_val, model_dir and val_note_id_to_scores
        """
        loss_each_epoch_train = []
        loss_each_epoch_val = []
        val_note_id_to_score = None
        for epoch in range(1, self.epochs + 1):
            print("Epoch: {}".format(str(epoch)))
            train_loss = self.__train(train_batches_gen)
            loss_each_epoch_train.append(train_loss)

            val_loss, temp_val_note_id_to_score = self.__test(validate_batches_gen)
            loss_each_epoch_val.append(val_loss)
            val_note_id_to_score = temp_val_note_id_to_score
            if self.early_stopping and self.early_stopping_criteria(loss_each_epoch_val):
                # stop training, do tests on test and train sets, and then stop the script
                print("loss hasn't improved in {} epochs, early stopping.".format(self.patience))
                break
        # Use a random id using python library uuid to avoid collision of folder names. This happens mainly when
        # performing micro-modeling for organization-based models
        model_dir_ = MODEL_DIR + self.model_name + "_{}".format(self.specialty) + "_{}".format(
            self.code_set) + time.strftime('_%b_%d_%H_%M_%S', time.localtime()) + "_ID_" + str(uuid.uuid4())
        os.mkdir(model_dir_)
        self.model_dir = model_dir_
        self.save_model_params()
        self.save_model()
        print("Model trained and saved at location : {}".format(self.model_dir))
        return loss_each_epoch_train, loss_each_epoch_val, self.model_dir, val_note_id_to_score


    def __train(self, train_batches_gen):
        """
        trains the model for 1 epoch.
        :param train_batches_gen: data_processing.data_loader.BatchGenerator object for training dataset
        :return: train and validation loss for the epoch
        """
        losses = []
        self.model.train()
        prev_device = self.device
        for note_ids, data, target in tqdm(train_batches_gen):
            data = data.to(self.device)
            target = target.to(self.device)
            self.optimizer.zero_grad()
            try:
                output, loss, _ = self.model(data, target)
                del output, _
                torch.cuda.empty_cache()
                loss.backward()
                temp_loss = loss
            except RuntimeError:

                # Leaving this commented code here that can be used for checking the memory usage while running epochs
                # result = subprocess.check_output(
                #     [
                #         'nvidia-smi', '--query-gpu=memory.free',
                #         '--format=csv,nounits,noheader'
                #     ], encoding='utf-8')
                # print("memory in free before empty cache: " + result.strip().split('\n')[0])

                # PUT STUFF FROM GPU TO CPU: model, optimizer, data, target
                print("Offloading computation to {}".format(str(ModelTraining.__CPU_DEVICE)))
                self.model = self.model.to(ModelTraining.__CPU_DEVICE)
                for d in list(self.optimizer.state.values()):
                    d[ModelTraining.__OPTIM_EXP_AVG] = d[ModelTraining.__OPTIM_EXP_AVG].to(
                        ModelTraining.__CPU_DEVICE)
                    d[ModelTraining.__OPTIM_EXP_AVG_SQ] = d[ModelTraining.__OPTIM_EXP_AVG_SQ].to(
                        ModelTraining.__CPU_DEVICE)
                data = data.to(ModelTraining.__CPU_DEVICE)
                target = target.to(ModelTraining.__CPU_DEVICE)

                # Compute forward pass again for this batch; this time on CPU.
                self.optimizer.zero_grad()
                output, loss2, _ = self.model(data, target)
                del output, _
                loss2.backward()
                temp_loss = loss2
                self.device = ModelTraining.__CPU_DEVICE

            losses.append(temp_loss.item())
            del temp_loss
            del data, target
            self.optimizer.step()
        if self.device != prev_device:
            self.device = prev_device
            # PUT STUFF BACK TO GPU FROM CPU: Model and optimizer only. Other variables MUST be deleted.
            print("Moving model back to {}".format(str(self.device)))
            self.model = self.model.to(self.device)
            for d in list(self.optimizer.state.values()):
                d[ModelTraining.__OPTIM_EXP_AVG] = d[ModelTraining.__OPTIM_EXP_AVG].to(self.device)
                d[ModelTraining.__OPTIM_EXP_AVG_SQ] = d[ModelTraining.__OPTIM_EXP_AVG_SQ].to(self.device)
        torch.cuda.empty_cache()
        return np.mean(losses, dtype=np.float64)


    def early_stopping_criteria(self, loss_each_epoch):
        """
        This method checks if the loss has changed by more than {epsilon} for {patience} number of epochs.
        :param loss_each_epoch: list containing loss for each epoch.
        :return: True if the loss has not changed and False otherwise
        """
        loss_diff = 0
        curr_loss = loss_each_epoch[-1]
        z = 0
        for loss in loss_each_epoch[-2::-1]:
            if (loss - curr_loss) < self.epsilon:
                loss_diff += 1
            z += 1
            if z == self.patience:
                break
        print("early stopping, loss_diff: {}".format(loss_diff))
        if loss_diff == self.patience:
            return True
        else:
            return False


    def test_model(self, test_batches_gen, save_attention=False, top_k_codes=8, top_n_attentions=4,
                   top_n_code_scores=100, is_top_n=False):
        """
        Predict and saves the scores of the predictions on the test data set.
        :param test_batches_gen: data_processing.data_loader.BatchGenerator object for held out test dataset
        :param save_attention: boolean for saving the attention.
        :param top_k_codes: The number of codes for which the attention should be saved.
        :param top_n_attentions: The size of the attention for each label. The total size of the attention will be
                                top_n_attention*kernel_size of the convolution layer.
        :param top_n_code_scores: the number of codes for which the scores should be saved.
        :param is_top_n: True, if the method of code prediction is top n. False, if the method of code prediction is
                         thresholding.
        :return: loss incurred by the model on the test set and a nested dictionary with key as note id and value as
                 another dictionary with key as code label and value as score.
        """
        if save_attention:
            loss_test, test_note_id_to_score = self.__test(test_batches_gen, save_attention=save_attention,
                                                           top_n_attentions=top_n_attentions, top_k_codes=top_k_codes,
                                                           save_output=True, top_n_code_scores=top_n_code_scores,
                                                           is_top_n=is_top_n)
        else:
            loss_test, test_note_id_to_score = self.__test(test_batches_gen, save_output=True, is_top_n=is_top_n)
        return loss_test, test_note_id_to_score


    def test_model_for_attention_weights(self, test_batches_gen, attention_select_codes):
        """
        Predicts and saves the scores of the predictions on the test data set with a focus on saving attention weights
        for select set of codes. Additionally, we use the thresholding method as opposed to the top n method for
        predicting a code.
        :param test_batches_gen: data_processing.data_loader.BatchGenerator object for held out test dataset
        :param attention_select_codes: codes for which we wish to obtain results for attention weights.
        :return: loss incurred by the model on the data set
        """
        return self.__test(test_batches_gen, save_attention=True, save_output=True, top_n_attentions=4,
                           is_top_n=False, attention_select_codes=attention_select_codes, prediction_threshold=0.5)


    def __test(self, data_gen, save_attention=False, save_output=False, top_k_codes=8, top_n_attentions=4,
               top_n_code_scores=100, is_top_n=False, attention_select_codes=(), prediction_threshold=0.5):
        """
        Predicts the probability of the labels for the {data_gen} data set.
        :param data_gen: data generator for which the loss is to be calculated
        :param save_attention: True, if the output attention should be saved to the {model_dir}
        :param save_output: True, if the output predictions should be saved to the {model_dir}
        :param top_k_codes: The number of codes for which the attention should be saved.
        :param top_n_attentions: The size of the attention for each label. The total size of the attention will be
                                top_n_attention*kernel_size of the convolution layer.
        :param top_n_code_scores: the number of codes for which the scores should be saved.
        :param is_top_n: True, if the method of code prediction is top n. False, if the method of code prediction is
                         thresholding.
        :param attention_select_codes: codes for which we wish to obtain results for attention weights.
        :param prediction_threshold: threshold value for the thresholding method of code prediction.
        :return: loss incurred by the model on the data set
        """
        losses = []
        self.model.eval()
        note_id_to_score = dict()
        note_id_to_attn_weight_tp = dict()
        note_id_to_attn_weight_fp = dict()
        note_id_to_attn_weight_fn = dict()
        prev_device = self.device
        for note_ids, data, target in tqdm(data_gen):
            with torch.no_grad():
                data = data.to(self.device)
                target = target.to(self.device)
                self.model.zero_grad()
                try:
                    output, loss, alpha = self.model(data, target)
                except RuntimeError:
                    # PUT STUFF FROM GPU TO CPU: model, data, target
                    print("Offloading computation to {}".format(str(ModelTraining.__CPU_DEVICE)))
                    self.model = self.model.to(ModelTraining.__CPU_DEVICE)
                    data = data.to(ModelTraining.__CPU_DEVICE)
                    target = target.to(ModelTraining.__CPU_DEVICE)

                    self.model.zero_grad()
                    output, loss, alpha = self.model(data, target)
                    self.device = ModelTraining.__CPU_DEVICE

                losses.append(loss.item())

                output = output.cpu().numpy()
                alpha = alpha.cpu().numpy()
                for i, note_id in enumerate(note_ids):
                    note_id_val = self.data_iterator.data_fields.ID.vocab.itos[note_id.item()]
                    out = output[i]
                    sorted_out_idx = np.argsort(out)
                    # If the way we compute true positive, false positive, etc. is based on "top n" statistics
                    # In this case, we assume that recall@n is the relevant metric and hence we only need to store
                    # top n code scores to file.
                    if is_top_n:
                        note_id_to_score[note_id_val] = get_code_scores(top_n_code_scores, out, self.data_iterator)
                    # This else statement assumes that we default to thresholding to compute true positive, false
                    # positive, etc. In this case, we assume that we care about both recall and precision for each
                    # code label individually. Hence we need to store code scores for all codes.
                    else:
                        note_id_to_score[note_id_val] = self.get_code_scores_all(out)
                    if save_attention:
                        true_label_idx = set([i for i, j in enumerate(target[i]) if j == 1])
                        attn_weights = alpha[i]
                        if is_top_n or attention_select_codes == ():
                            predicted_code_idxs = set(sorted_out_idx[-top_k_codes:])
                        else:
                            predicted_code_idxs = self.get_predicted_code_idxs(attention_select_codes, out,
                                                                               thresh=prediction_threshold)

                        code_idxs_tp = true_label_idx.intersection(predicted_code_idxs)
                        code_to_attention_tp = get_code_to_attention(self.data_iterator, self.kernel_size, code_idxs_tp,
                                                                     top_n_attentions, attn_weights, data[i],
                                                                     is_mention=True)
                        note_id_to_attn_weight_tp[note_id_val] = code_to_attention_tp

                        code_idxs_fp = predicted_code_idxs - true_label_idx
                        code_to_attention_fp = get_code_to_attention(self.data_iterator, self.kernel_size, code_idxs_fp,
                                                                     top_n_attentions, attn_weights, data[i],
                                                                     is_mention=True)
                        note_id_to_attn_weight_fp[note_id_val] = code_to_attention_fp

                        code_idxs_fn = true_label_idx - predicted_code_idxs
                        code_to_attention_fn = get_code_to_attention(self.data_iterator, self.kernel_size, code_idxs_fn,
                                                                     top_n_attentions, attn_weights, data[i],
                                                                     is_mention=True)
                        note_id_to_attn_weight_fn[note_id_val] = code_to_attention_fn

                del data, target, output, alpha
        # PUT STUFF BACK TO GPU FROM CPU: model
        if self.device != prev_device:
            self.device = prev_device
            print("Moving model back to {}".format(str(self.device)))
            self.model = self.model.to(self.device)

        if save_output:
            to_pickle(self.model_dir + "/test_scores.pickle", note_id_to_score)
            if save_attention:
                to_pickle(self.model_dir + "/test_attention_weights_tp.pickle", note_id_to_attn_weight_tp)
                to_pickle(self.model_dir + "/test_attention_weights_fp.pickle", note_id_to_attn_weight_fp)
                to_pickle(self.model_dir + "/test_attention_weights_fn.pickle", note_id_to_attn_weight_fn)

        torch.cuda.empty_cache()
        return np.mean(losses, dtype=np.float64), note_id_to_score


    def get_code_scores_all(self, output_scores):
        """
        This method selects code scores for all codes.
        :param output_scores: output score array containing the scores of all the code labels.
        :return: returns the dictionary containing the scores of all codes.
        """
        out_score = dict()
        for i in range(len(output_scores)):
            out_score[self.data_iterator.data_fields.LABEL.vocab.itos[i]] = output_scores[i]
        return out_score


    def get_predicted_code_idxs(self, code_labels, output_scores, thresh=0.5):
        """
        This method outputs the scores of the input code_labels in the out_score dict.
        :param code_labels: set of code labels for whom we wish to obtain predictions.
        :param output_scores: output score array containing the scores of all the code labels.
        :param thresh: threshold using for prediction of a code.
        :return: predicted_code_idxs, a subset of code indices corresponding to input code_labels such that the
                 only the predicted code_label indices are included.
        """
        predicted_code_idxs = set()
        for code in code_labels:
            code_idx = self.data_iterator.data_fields.LABEL.vocab.stoi[code]
            if output_scores[code_idx] >= thresh:
                predicted_code_idxs.add(code_idx)
        return predicted_code_idxs


    def save_model_params(self):
        """
        Saves the necessary model and class params to the json file at the {model_dir} directory
        """
        param_dict = self.__dict__.copy()
        param_dict.pop("data_iterator")
        # param_dict.pop("train_batches_gen")
        # param_dict.pop("test_batches_gen")
        # param_dict.pop("validate_batches_gen")
        param_dict.pop("model")
        param_dict.pop("optimizer")
        param_dict.pop("device")
        to_json(self.model_dir + "/params.json", param_dict)


    def save_model(self):
        """
        Saves the trained model at the {model_dir} directory
        """
        sd = self.model.cpu().state_dict()
        torch.save(sd, self.model_dir + SAVED_MODEL_NAME)
        self.model.to(self.device)


if __name__ == '__main__':
    from cnn_attention import CPT_CODE_SET, ICD10_CODE_SET
    from cnn_attention.data_processing.data_loader import DataLoader
    from matplotlib import pyplot as py
    from cnn_attention.data_model.data_constants import OutputColumns

    n_epochs = 1
    batch_size_ = 32
    learning_rate_ = 0.25 * 1e-3

    """
    training and testing the model 
    """
    specialty_ = SpecialtyNames.MRI
    code_set_ = ICD10_CODE_SET
    data_loader = DataLoader(specialty_, code_set_, rest_api=False)
    train_data, validate_data, test_data = data_loader.load_datasets()
    train_batches_gen_, validate_batches_gen_ = data_loader.get_train_validate_batch_generators(batch_size_,
                                                                                                train_data,
                                                                                                validate_data)
    test_batches_gen_ = data_loader.get_test_batch_generator(test_data, OutputColumns.NOTE_ID, OutputColumns.TEXT,
                                                             y_field=data_loader.label_col, is_train=True,
                                                             batch_size=batch_size_)
    model_training = ModelTraining(None, specialty_, code_set_, 4, 50, CNN, "0", epochs=n_epochs,
                                   batch_size=batch_size_, weight_decay=0, learning_rate=learning_rate_,
                                   early_stopping=False, epsilon=0, patience=0, data_iterator=data_loader.data_iterator)

    loss_each_epoch_train_, loss_each_epoch_val_, model_directory_, val_note_id_to_score_ = model_training.train_model(
        train_batches_gen_,
        validate_batches_gen_)
    loss_test_, test_note_id_to_score_ = model_training.test_model(test_batches_gen_, is_top_n=True)
    print("loss for test data : {}".format(str(loss_test_)))

    py.figure()
    h1 = py.plot(range(0, n_epochs), loss_each_epoch_train_)
    h2 = py.plot(range(0, n_epochs), loss_each_epoch_val_)
    py.savefig(model_directory_ + "/loss_vs_time.png")
