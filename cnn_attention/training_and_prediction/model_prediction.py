import numpy as np
import torch
from tqdm import tqdm

from cnn_attention.data_processing.data_loader import DataLoader

from cnn_attention.training_and_prediction import cnn_utils


class ModelPrediction:

    def __init__(self, model_dir):
        self.model_dir = model_dir
        _params = cnn_utils.parameter_reader(model_directory=model_dir)
        _specialty = _params.get('specialty')
        _code_set = _params.get('code_set')

        self.kernel_size = _params.get('kernel_size')
        self.conv_out_size = _params.get('conv_out_size')
        self.model_name = _params.get('model_name')
        self.cuda_device_number = _params.get('cuda_device_number')

        dropout = _params.get('dropout')
        if not dropout:
            self.dropout = 0.5
        else:
            self.dropout = dropout

        self.data_loader = DataLoader(specialty=_specialty, code_set=_code_set, rest_api=True)
        self.data_iterator = self.data_loader.data_iterator
        self.device, self.model = cnn_utils.load_model(model_dir, self.kernel_size, self.conv_out_size, self.model_name,
                                                       self.cuda_device_number, self.dropout, self.data_iterator)


    def get_prediction_for_notes(self, clinical_notes, note_id_field="noteId", content_field="content", top_k_codes=8,
                                 top_n_attentions=4, is_mention=True):
        """
        This function accepts a list of dictionaries of noteId to pre-processed clinical note string
        and returns the prediction score for the {top_k_codes} and attention_weights for the {top_n_attentions}
        :param clinical_notes: list of dictionaries of noteId to pre-processed clinical note string
        :param note_id_field: name of the note_id field
        :param content_field: name of the content field
        :param top_k_codes: The number of codes for which the prediction scores must be returned.
        :param top_n_attentions: The size of the attention for each label. The total size of the attention will be
                                top_n_attention*kernel_size of the convolution layer.
        :param is_mention: denotes if we want to run the function in the mentions mode.
        :return: code to prediction score dictionary and code to word attention weight dictionary.
        """

        data_gen = self.data_loader.get_test_batch_generator_from_list_dict(clinical_notes, note_id_field,
                                                                            content_field)
        self.model.eval()
        note_id_to_code_score = dict()
        note_id_to_code_attn = dict()
        for note_ids, data, target in tqdm(data_gen):
            output, alpha = self.get_prediction(data)
            output = output.numpy()
            alpha = alpha.numpy()
            for i, note_id in enumerate(note_ids):
                note_id_val = self.data_iterator.data_fields.ID.vocab.itos[note_id.item()]
                out = output[i]
                sorted_out_idx = np.argsort(out)
                note_id_to_code_score[note_id_val] = cnn_utils.get_code_scores(top_k_codes, out, self.data_iterator)
                code_to_attention = cnn_utils.get_code_to_attention(self.data_iterator, self.kernel_size,
                                                                    set(sorted_out_idx[-top_k_codes:]), top_n_attentions,
                                                                    alpha[i],
                                                                    data.numpy()[i],
                                                                    is_mention=is_mention)
                note_id_to_code_attn[note_id_val] = code_to_attention
        return note_id_to_code_score, note_id_to_code_attn


    def get_prediction(self, data):
        """
        This function accepts the input batch of data and returns the output scores for each code label and attention
        weights for each code label and each input text.
        :param data: input batch of data.
        :return: output scores for each code label and attention weights for each code label and each input text.
        """
        prev_device = self.device
        with torch.no_grad():
            data = data.to(self.device)
            self.model.zero_grad()
            try:
                output, alpha = self.model.predict(data)
            except RuntimeError:
                # PUT STUFF FROM GPU TO CPU: model, data, target
                print("Offloading computation to {}".format(str(cnn_utils.__CPU_DEVICE)))
                self.model = self.model.to(cnn_utils.__CPU_DEVICE)
                data = data.to(cnn_utils.__CPU_DEVICE)

                self.model.zero_grad()
                output, alpha = self.model.process_individual_notes(data)
                self.device = cnn_utils.__CPU_DEVICE
                if self.device != prev_device:
                    self.device = prev_device
                    print("Moving model back to {}".format(str(self.device)))
                    self.model = self.model.to(self.device)
        return output, alpha


if __name__ == '__main__':
    """
    getting prediction for a batch of notes in the form of list of dictionaries from a pre-trained model
    """
    from utils import file_io_utils
    from cnn_attention import DATA_DIR, MODEL_DIR

    note = file_io_utils.load_json(DATA_DIR + "batch_request.json")

    model_dir__ = MODEL_DIR + "conv_attn_ct_scan_ICD10_Oct_21_13_23_36"
    # model_dir__ = MODEL_DIR + "conv_attn_los_alamitos_ICD10_Jan_26_04_01_22"
    model_prediction = ModelPrediction(model_dir=model_dir__)

    note_id_to_code_score_, note_id_to_code_attn_ = model_prediction.get_prediction_for_notes(note)

    print(note_id_to_code_score_)
    print(note_id_to_code_attn_)

