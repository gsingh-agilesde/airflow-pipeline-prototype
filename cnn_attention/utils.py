from utils.dict_utils import inc_key_in_dict
from nltk.tokenize import RegexpTokenizer

# NLTK tokenizer to keep only alphanumeric tokens.
__TOKENIZER = RegexpTokenizer(r'\w+')


def to_str(bytes_or_str):
    if isinstance(bytes_or_str, bytes):
        value = bytes_or_str.decode('utf-8')
    else:
        value = bytes_or_str
    return value

def process_note(text):
    """
    Process input text using above regular expression to capture alphanumeric tokens only. Additionally, we remove
    tokens that are solely numeric.
    :param text: Input clinical note text.
    :return: preprocessed note and mapping of word indices in processed note to their corresponding indices in the
             original note.
    """
    text = to_str(bytes_or_str=text)
    ind_to_ind = dict()
    j = 0
    res = [t.lower() for t in text.split()]
    for i, token in enumerate(res):
        for t in __TOKENIZER.tokenize(token):
            if not t.isnumeric():
                ind_to_ind[j] = i
                j += 1
    tokens = [t.lower() for t in __TOKENIZER.tokenize(text) if not t.isnumeric()]
    return ' '.join(tokens), ind_to_ind


def extract_note_id(file_name):
    """
    Extracts note id from the file name of the note.

    E.g. for file_name="Cardio_5_2018-04-20_10418_73027266.txt", we do the following:
    1. file_name.split(".")                             --> ['Cardio_5_2018-04-20_10418_73027266', 'txt']
    2. file_name.split(".")[0]                          --> 'Cardio_5_2018-04-20_10418_73027266'
    3. file_name.split(".")[0].split("_")               --> ['Cardio', '5', '2018-04-20', '10418', '73027266']
    4. file_name.split(".")[0].split("_")[1:]           --> ['5', '2018-04-20', '10418', '73027266']
    5. "_".join(file_name.split(".")[0].split("_")[1:]) --> '5_2018-04-20_10418_73027266'

    :param file_name: Input file name string.
    :return: The note identifier string.
    """
    return '_'.join(file_name.split(".")[0].split("_")[1:])


def extract_note_type(file_name):
    """
    Extracts note type from the file name of the note.

    E.g. for file_name="Cardio_5_2018-04-20_10418_73027266.txt", we do the following:
    1. file_name.split("_")    --> ['Cardio', '5', '2018-04-20', '10418', '73027266.txt']
    2. file_name.split("_")[0] --> 'Cardio'

    :param file_name: Input file name string.
    :return: The note type string.
    """
    return file_name.split("_")[0]


def get_code_frequency(set_codes_generator):
    """
    Computes the frequency for each code for all codes in the set_codes_generator object.
    :param set_codes_generator: input codes generator object that generators a set of codes.
    :return: dictionary with key as code and value as the frequency.
    """
    code_to_frequency = dict()
    for codes in set_codes_generator:
        for code in codes:
            inc_key_in_dict(dict_=code_to_frequency, k_=code)
    return code_to_frequency


def get_set_codes_frequency(set_codes_generator):
    """
    Computes the frequency for each set of codes for all sets of codes in the set_codes_generator object.
    :param set_codes_generator: input codes generator object that generators a set of codes.
    :return: dictionary with key as a constant (frozen) set of codes and value as the frequency of that set.
    """
    set_codes_to_frequency = dict()
    for codes in set_codes_generator:
        inc_key_in_dict(dict_=set_codes_to_frequency, k_=frozenset(codes))
    return set_codes_to_frequency
