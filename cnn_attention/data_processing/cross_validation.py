from collections import defaultdict
from itertools import chain
from sklearn.model_selection import KFold


class StratifiedKFold:

    def __init__(self, n_splits=5):
        """
        :param n_splits: int value that indicates the number of folds in cross validation.
                         This must be at least 2.
        """
        if n_splits < 2:
            raise ValueError("n_splits must be at least equal to 2.")
        self.n_splits = n_splits


    def split(self, y=None):
        """
        :param y: numpy array-like such that every element of the array is hashable.
                  This represents the labels array that contains data from both training and validation datasets. Since
                  stratification is required to be done by label, we do not consider the features, X array and only
                  provide splits of indices on labels array y. The same indices must be used to obtain corresponding
                  features X.
        :return: yields the requisite indices for training and validation datasets
        """
        # 0. For every unique code label compute the indices corresponding to the examples with the given code label in
        #    a dictionary with key=code label and value=list of indices. This is why we need all code labels to be
        #    hashable.
        unique_y_to_indices = defaultdict(list)
        for i, label in enumerate(y):
            unique_y_to_indices[label].append(i)

        # 1. For every unique code label check if number of examples are less than self.n_splits.
        #    If number of samples are less than self.n_splits, all the examples corresponding to the code label must be
        #    in the training dataset for all folds.
        common_train_indices = []
        labels = list(unique_y_to_indices.keys())
        for label in labels:
            if len(unique_y_to_indices[label]) < self.n_splits:
                common_train_indices.append(unique_y_to_indices[label])
                del unique_y_to_indices[label]

        common_train_indices = list(chain.from_iterable(common_train_indices))

        # 2. For every unique code label perform the usual kfold splits from sklearn to obtain one generator object per
        #    code.
        kf_generators = dict()
        for label in unique_y_to_indices:
            kf_generators[label] = KFold(n_splits=self.n_splits).split(unique_y_to_indices[label])

        # 3. Finally for all generator objects, make one iteration click and collect all indices for train and validate
        #    separately and perform the yield.
        #    Note: Each code label's generator object may exhaust at different times. Need to handle this issue.
        for i in range(self.n_splits):
            train_indices, validate_indices = [], []
            for label in unique_y_to_indices:
                train_, validate_ = next(kf_generators[label])
                train_ind = [unique_y_to_indices[label][i] for i in train_]
                validate_ind = [unique_y_to_indices[label][i] for i in validate_]
                train_indices.append(train_ind)
                validate_indices.append(validate_ind)
            train_indices.append(common_train_indices)
            train_indices = list(chain.from_iterable(train_indices))
            validate_indices = list(chain.from_iterable(validate_indices))
            yield train_indices, validate_indices
