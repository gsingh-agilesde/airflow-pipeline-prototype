import pandas as pd

from cnn_attention.data_processing.train_validate_test_split import create_train_validate_test_data_splits
from cnn_attention.data_processing.word_embeddings import generate_wv_embeddings
from utils.file_io_utils import to_pickle

from cnn_attention.data_model.data_constants import OutputColumns, SpecialtyNames
from cnn_attention import DATA_DIR, EM_NEW, EM_EST, CPT_CODE_SET

EM_EST_SET = {"99211", "99212", "99213", "99214", "99215"}
EM_NEW_SET = {"99201", "99202", "99203", "99204", "99205"}
CODE_SET_DICT = {EM_NEW: EM_NEW_SET, EM_EST: EM_EST_SET}
labels = OutputColumns.CPT_LABELS
for code_set in [EM_EST, EM_NEW]:
    for specialty in [SpecialtyNames.ALL, SpecialtyNames.CARDIO, SpecialtyNames.ORTHO, SpecialtyNames.PCP]:
        note_id_text_length_labels_df = pd.read_csv(
            DATA_DIR + specialty + "_notes_with_" + CPT_CODE_SET + "_feedback.csv")
        note_id_text_length_labels_df[labels] = note_id_text_length_labels_df[labels].apply(
            lambda codes: frozenset([code for code in codes.split(";")]))
        note_id_text_length_labels_df["is_em_code"] = note_id_text_length_labels_df['cpt_labels'].apply(
            lambda x: len(x) == 1 and len(x.intersection(CODE_SET_DICT[code_set])) == 1)
        note_id_text_length_labels_df = note_id_text_length_labels_df[
            note_id_text_length_labels_df["is_em_code"] == True]

        all_note_ids = set(note_id_text_length_labels_df[OutputColumns.NOTE_ID].tolist())
        note_id_text_length_labels_df = pd.read_csv(
            DATA_DIR + specialty + "_notes_with_" + CPT_CODE_SET + "_feedback.csv")
        df = note_id_text_length_labels_df[
            note_id_text_length_labels_df[OutputColumns.NOTE_ID].isin(all_note_ids)]
        df.to_csv(DATA_DIR + specialty + "_notes_with_" + code_set + "_feedback.csv",
                  index=False)
        create_train_validate_test_data_splits(specialty, code_set)
        vocab = generate_wv_embeddings(specialty, code_set, 100, 0, 4, 5)
        to_pickle(DATA_DIR + "{}_{}_vocab.pickle".format(specialty, code_set), vocab)
        print("support for {} and {} : {}".format(specialty, code_set, str(len(all_note_ids))))
