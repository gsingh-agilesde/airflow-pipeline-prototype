import os
from cnn_attention import CPT_CODE_SET, MODEL_DIR
from cnn_attention.data_model.data_constants import SpecialtyNames
from cnn_attention.data_processing.analysis.code_metric_frequency_analysis import compute_code_metric_frequency

specialty = SpecialtyNames.ORTHO_NO_EM
code_set = CPT_CODE_SET

if __name__ == '__main__':

    # Assume that the micro models for all organizations are stored under MODEL_DIR/org_based_models/ folder.
    model_subpath = "/org_based_models/"
    base_path = MODEL_DIR + model_subpath
    paths = os.listdir(base_path)
    org_to_model_names = dict()

    for i, folder in enumerate(paths):
        # Example folder -- conv_attn_ortho_no_em_13532_CPT_Jul_11_06_02_30_ID_74f16630-12ab-453b-8769-b98e57420230
        # The ID component at the very end was added in to avoid ambiguity in model names using uuid.

        # Extract the 13532 number from above model name.
        org = folder.split("_")[5]
        # If key 13532 is present in org_to_model_names dictionary, then its value must be a list. In this case, we
        # simply append the "folder" variable to that list.
        # If key 13532 is not present in the org_to_model_names dictionary, then we set the key in the dictionary with
        # a default value of empty list, i.e., [], and we then append the "folder" variable in the usual way.
        org_to_model_names.setdefault(folder.split("_")[5], []).append(folder)

    for org_id in org_to_model_names:
        print("\nFor org id={}\n".format(org_id))
        new_specialty = specialty + "_" + org_id
        model_names = org_to_model_names[org_id]
        for model_name in model_names:
            compute_code_metric_frequency(new_specialty, code_set, base_path + model_name + "/test_scores.pickle",
                                          model_name)
