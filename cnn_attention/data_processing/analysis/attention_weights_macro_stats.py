import pandas as pd

from utils.file_io_utils import from_pickle, from_text

from cnn_attention import CNN, DATA_DIR, CPT_CODE_SET, MODEL_DIR

from cnn_attention.utils import process_note
from cnn_attention.data_model.data_constants import SpecialtyNames, OutputColumns
from cnn_attention.training_and_prediction.model_training import ModelTraining

__THRESH = 0.1
__THRESH_CODE = 0.5

__SPECIALTY_NOTE_PREFIX = "Ortho_"
__SPECIALTY_NOTE_PATH = DATA_DIR + SpecialtyNames.ORTHO + "_notes_edited/"

# We assume that the following folder is created under data_processing/analysis/
__OUTPUT_NOTE_PATH = "attention_weight_output_rtf/"

__RTF_BEGIN = """{\\rtf1"""
__RTF_END = """}"""

n_epochs = 120
early_stopping = True
batch_size_ = 32
learning_rate_ = 0.25 * 1e-3


class ClassificationType:
    TP = "TP"
    FP = "FP"
    FN = "FN"


def get_code_to_note_id_to_wordset(codes_of_interest, attention_weights, ground_truth_labels_file_path,
                                   classification_type=ClassificationType.TP,
                                   top_k=8):
    """
    Helper function to obtain a nested dictionary storing results of input classification_type for all codes of
    interest.
    :param codes_of_interest: codes for whom we wish to obtain the results.
    :param attention_weights: stored attention weights for input classification type for all codes and notes.
    :param ground_truth_labels_file_path: File path to the ground truth labels for the test dataset.
    :param classification_type: An enum-like object that denotes whether we wish to obtain results for true positive
    (TP), false positive (FP) or false negative (FN).
    :param top_k: number of attention weights to store out of 16 stored weights. This default of 16 can be changed in
                  the model_training.py file or an option for the same may be exposed via the
                  model_training.ModelTraining.test_model_for_attention_weights method.
    :return: dictionary with key as code label and value as another dictionary with its key as note_id and value as a
             list of top_k attention words for the chosen code label and note_id.
    """
    code_to_note_id_to_wordset = dict()

    for code in codes_of_interest:
        test_df = pd.read_csv(ground_truth_labels_file_path)
        test_df[OutputColumns.CPT_LABELS] = test_df[OutputColumns.CPT_LABELS].apply(
            lambda x: [v for v in x.split(";") if v == code])

        mask = test_df[OutputColumns.CPT_LABELS].apply(lambda x: len(x) != 0)
        res = test_df[mask][OutputColumns.NOTE_ID]

        if classification_type == ClassificationType.TP:
            note_ids = res.values.tolist()
        else:
            note_ids = list(attention_weights.keys())

        note_id_to_wordset = dict()
        for note_id in note_ids:
            if note_id in attention_weights:
                if code in attention_weights[note_id]:
                    note_path = __SPECIALTY_NOTE_PATH + __SPECIALTY_NOTE_PREFIX + note_id + ".txt"
                    original_note_text = " ".join(from_text(note_path))
                    processed_text, mapping = process_note(original_note_text)
                    note_id_to_wordset[note_id] = []
                    rtf_text = original_note_text.split()
                    rtf_file = open(
                        __OUTPUT_NOTE_PATH + code + "_" + classification_type + "+" + __SPECIALTY_NOTE_PREFIX + note_id
                        + ".rtf", "w")
                    sorted_16_attention_weights = sorted(attention_weights[note_id][code].items(), key=lambda kv: kv[1],
                                                         reverse=True)
                    for i in range(top_k):
                        word, weight = sorted_16_attention_weights[i]
                        # If more information such as mapping of word index to word for both original and preprocessed
                        # note is needed, use the following.
                        # note_id_to_wordset[note_id].append([{word: processed_text.split()[word]},
                        #                                     {mapping[word]: original_note_text.split()[mapping[word]]}])
                        # Else use the following.
                        note_id_to_wordset[note_id].append(processed_text.split()[word])
                        rtf_text[mapping[word]] = "\\b " + rtf_text[mapping[word]] + " \\b0"
                    code_to_note_id_to_wordset[code] = note_id_to_wordset
                    rtf_file.write(__RTF_BEGIN + " ".join(rtf_text) + __RTF_END)
                    rtf_file.close()

    return code_to_note_id_to_wordset


def get_code_to_note_id_to_score(code_to_note_id_to_wordset, note_id_to_code_to_score):
    """
    Helper function to obtain a nested dictionary storing results of code probability score for all codes in the input
    code_to_note_id_to_wordset dictionary.
    :param code_to_note_id_to_wordset: dictionary with key as code label and value as another dictionary with its key
                                       as note_id and value as a list of attention words for the chosen code label and
                                       note_id.
    :param note_id_to_code_to_score: dictionary with key as note id and value as another dictionary with its key as
                                     code label and value as the probability score for the chosen code label and
                                     note_id.
    :return: dictionary with key as code label and value as another dictionary with its key as note_id and value as the
             probability score for the chosen code label and note_id.
    """
    code_to_note_id_to_score = dict()
    for code in code_to_note_id_to_wordset:
        code_to_note_id_to_score[code] = {}
        for note_id in code_to_note_id_to_wordset[code]:
            code_to_score = note_id_to_code_to_score[note_id]
            code_to_note_id_to_score[code][note_id] = code_to_score[code]
    return code_to_note_id_to_score


if __name__ == '__main__':
    code_set = CPT_CODE_SET
    specialty = SpecialtyNames.ORTHO_NO_EM
    model_name = "conv_attn_ortho_no_em_CPT_Jun_29_10_32"

    # No Error case.
    # codes_of_interest = {"69209", "J7324", "87804"}

    # Type I Error case, Type II Error case.
    codes_of_interest = {"82962", "81025"}

    model_training = ModelTraining(MODEL_DIR + model_name + "/", specialty, code_set, 4, 50, CNN, "0", epochs=n_epochs,
                                   batch_size=batch_size_, weight_decay=0, learning_rate=learning_rate_,
                                   early_stopping=early_stopping, epsilon=0, patience=5)

    loss_test_ = model_training.test_model_for_attention_weights(attention_select_codes=tuple(codes_of_interest))
    print("loss for test data : {}".format(str(loss_test_)))

    test_attention_weights_tp_file_path = MODEL_DIR + model_name + "/test_attention_weights_tp.pickle"
    test_attention_weights_fp_file_path = MODEL_DIR + model_name + "/test_attention_weights_fp.pickle"
    test_attention_weights_fn_file_path = MODEL_DIR + model_name + "/test_attention_weights_fn.pickle"

    test_score_file_path = MODEL_DIR + model_name + "/test_scores.pickle"

    attention_weights_tp = from_pickle(test_attention_weights_tp_file_path)
    attention_weights_fp = from_pickle(test_attention_weights_fp_file_path)
    attention_weights_fn = from_pickle(test_attention_weights_fn_file_path)

    ground_truth_labels_file_path = DATA_DIR + "{}_{}_test.csv".format(specialty, code_set)

    # Results: For attention weights for codes_of_interest.
    code_to_note_id_to_wordset_tp = get_code_to_note_id_to_wordset(codes_of_interest, attention_weights_tp,
                                                                   ground_truth_labels_file_path)
    code_to_note_id_to_wordset_fp = get_code_to_note_id_to_wordset(codes_of_interest, attention_weights_fp,
                                                                   ground_truth_labels_file_path,
                                                                   classification_type=ClassificationType.FP)
    code_to_note_id_to_wordset_fn = get_code_to_note_id_to_wordset(codes_of_interest, attention_weights_fn,
                                                                   ground_truth_labels_file_path,
                                                                   classification_type=ClassificationType.FN)

    # Results: For code scores for codes_of_interest.
    note_id_to_code_to_score = from_pickle(test_score_file_path)
    note_ids = list(note_id_to_code_to_score.keys())

    code_to_note_id_to_score_tp = get_code_to_note_id_to_score(code_to_note_id_to_wordset_tp, note_id_to_code_to_score)
    code_to_note_id_to_score_fp = get_code_to_note_id_to_score(code_to_note_id_to_wordset_fp, note_id_to_code_to_score)
    code_to_note_id_to_score_fn = get_code_to_note_id_to_score(code_to_note_id_to_wordset_fn, note_id_to_code_to_score)
