import matplotlib.pyplot as plt
import pandas as pd
from nltk.corpus import stopwords
from wordcloud import WordCloud, STOPWORDS

from cnn_attention import DATA_DIR, CPT_CODE_SET

from cnn_attention.data_model.data_constants import OutputColumns
from cnn_attention.data_model.data_constants import SpecialtyNames

# Use the following stop words and the ones added in the main script. These words wont be used in the word cloud.
stop_words = set(STOPWORDS).union(set(stopwords.words('english')))


def draw_word_cloud(specialty, code_set):
    """
    Plots the word cloud from the clinical note data present in the clinical notes stored in the data frame for the
    corresponding specialty and code_set
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :return:
    """

    # Use either CPT/ICD dataframe.
    # This does not matter since we really only care about the words in the clinical notes.
    temp_df = pd.read_csv(DATA_DIR + specialty + "_notes_with_" + code_set + "_feedback.csv")

    # Form the text for all clinical notes.
    text_ = ''
    for note in temp_df[OutputColumns.TEXT]:
        for word in note.split():
            word2 = word.replace('"', '')
            if word2 not in stop_words:
                text_ += word2 + ' '
    print("Generating text .... done.")
    # This draws word clouds using bigrams and not unigrams since they are typically more meaningful.
    wordcloud = WordCloud(width=1600, height=800).generate(text_)
    print("Generating word cloud object .... done.")

    # Plot the WordCloud image
    plt.figure(figsize=(20, 10), facecolor='k')
    plt.imshow(wordcloud)
    plt.axis("off")
    plt.tight_layout(pad=0)
    plt.savefig(specialty + "_" + code_set + "_wordcloud.png", facecolor='k', bbox_inches='tight')
    plt.show()


if __name__ == '__main__':
    # Add these words frequently appearing to de-identify the PHI in the clinical notes.
    stop_words.add('xxx')
    stop_words.add('rrr')
    stop_words.add('llllll')
    stop_words.add('ffffff')
    stop_words.add('dddddd')

    #######
    # For other stop words we use the first note from Cardio.
    # This contains most stop words involved in section names for most clinical notes for any specialty.
    # E.g., Review, of, systems, chief, complaint, etc.
    temp_df_2 = pd.read_csv(DATA_DIR + SpecialtyNames.CARDIO + "_notes_with_" + CPT_CODE_SET + "_feedback.csv")
    sample_note = temp_df_2.loc[0, OutputColumns.TEXT]
    for word in sample_note.split():
        word2 = word.replace('"', '')
        stop_words.add(word2)
    #######
    print("num stop words: {}".format(len(stop_words)))

    draw_word_cloud(SpecialtyNames.CARDIO, CPT_CODE_SET)
    draw_word_cloud(SpecialtyNames.ORTHO, CPT_CODE_SET)
    draw_word_cloud(SpecialtyNames.PCP, CPT_CODE_SET)
