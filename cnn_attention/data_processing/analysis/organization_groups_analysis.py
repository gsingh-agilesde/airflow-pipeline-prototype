import pandas as pd
from itertools import chain

from utils.pandas_utils import df_group_by_attr1_list_attr2

from cnn_attention import DATA_DIR, CPT_CODE_SET
from cnn_attention.data_model.data_constants import SpecialtyNames, OutputColumns

__NO_OF_NOTES = "No. of notes"
__RATIO = "Ratio (No. of Notes to No. of Codes)"
__PHYS_GROUP_ID = "Physician group ID"
__NO_OF_UNIQUE_CODE_LABELS = "No. of unique code labels"
__UNIQUE_CODE_LABELS = "Unique code labels"


def basic_analysis(specialty, code_set):
    """
    In the basic analysis, we group the input data by organization (or physician) id and compute the various aggregate
    statistics such as number of records, number of unique labels, unique code labels and the ratio of number of notes
    to number of unique codes per organization or physician group. The result is stored as a csv file on disk in the
    present working directory.
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :return: the result dataframe with aggregate statistics.
    """
    note_id_text_length_labels_df = pd.read_csv(DATA_DIR + specialty + "_notes_with_" + code_set + "_feedback.csv")
    note_id_text_length_labels_df[OutputColumns.CPT_LABELS] = note_id_text_length_labels_df[
        OutputColumns.CPT_LABELS].apply(lambda x: x.split(";"))
    note_id_text_length_labels_df[OutputColumns.NOTE_ID] = note_id_text_length_labels_df[OutputColumns.NOTE_ID].apply(
        lambda x: x.split("_")[2])
    result_df = df_group_by_attr1_list_attr2(note_id_text_length_labels_df, OutputColumns.NOTE_ID,
                                             OutputColumns.CPT_LABELS)
    result_df[__NO_OF_NOTES] = result_df[OutputColumns.CPT_LABELS].apply(
        lambda x: len(x))
    result_df[__UNIQUE_CODE_LABELS] = result_df[OutputColumns.CPT_LABELS].apply(lambda x: set(chain.from_iterable(x)))
    result_df[OutputColumns.CPT_LABELS] = result_df[OutputColumns.CPT_LABELS].apply(
        lambda x: len(set(chain.from_iterable(x))))
    result_df[__RATIO] = result_df[__NO_OF_NOTES] / result_df[OutputColumns.CPT_LABELS]

    result_df.rename(
        columns={OutputColumns.NOTE_ID: __PHYS_GROUP_ID, OutputColumns.CPT_LABELS: __NO_OF_UNIQUE_CODE_LABELS},
        inplace=True)

    result_df.sort_values(by=__RATIO, ascending=False, inplace=True)
    result_df.reset_index(inplace=True, drop=True)
    return result_df


if __name__ == '__main__':
    result_df = basic_analysis(SpecialtyNames.CARDIO_NO_EM, CPT_CODE_SET)
    result_df.to_csv(SpecialtyNames.CARDIO_NO_EM + "_" + CPT_CODE_SET + "_org_groups.csv")

    result_df = basic_analysis(SpecialtyNames.ORTHO_NO_EM, CPT_CODE_SET)
    result_df.to_csv(SpecialtyNames.ORTHO_NO_EM + "_" + CPT_CODE_SET + "_org_groups.csv")

    result_df = basic_analysis(SpecialtyNames.PCP_NO_EM, CPT_CODE_SET)
    result_df.to_csv(SpecialtyNames.PCP_NO_EM + "_" + CPT_CODE_SET + "_org_groups.csv")
