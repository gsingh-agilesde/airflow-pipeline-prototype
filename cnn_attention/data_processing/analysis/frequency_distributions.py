import pandas as pd

from utils.plotting_utils import Columns, PlotProperties, plot_top_k_frequency_distribution_for_df_property
from utils.dict_utils import sort_dict_by_value
from utils.pandas_utils import generate_values_in_column
from data_processing_global import set_em_codes

from cnn_attention import DATA_DIR, CPT_CODE_SET, ICD10_CODE_SET

from cnn_attention.data_model.data_constants import OutputColumns
from cnn_attention.utils import get_code_frequency, get_set_codes_frequency


def plot_code_frequency_distribution(specialty, code_set):
    """
    Plots the code frequency distributions in an horizontal plot style using utils/plotting_utils.
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :return:
    """
    # Read data into a data frame.
    note_id_text_length_labels_df = pd.read_csv(DATA_DIR + specialty + "_notes_with_" + code_set + "_feedback.csv")
    note_id_text_length_labels_df[OutputColumns.CPT_LABELS] = note_id_text_length_labels_df[
        OutputColumns.CPT_LABELS].apply(lambda x: x.split(";"))

    # Compute property_frequency_df for plotting_utils function. Here property is "code label".
    code_to_frequency = sort_dict_by_value(get_code_frequency(
        generate_values_in_column(note_id_text_length_labels_df, OutputColumns.CPT_LABELS)),
        reverse=True)
    code_frequency_df = pd.DataFrame(list(code_to_frequency.items()), columns=[Columns.PROPERTY, Columns.FREQUENCY])

    # Plot the distribution
    max_frequency = list(code_to_frequency.values())[0]
    plot_properties = PlotProperties(width=80, height=60, font_scale=6, xticks=range(0, max_frequency, 1000))
    k = 50
    fig = plot_top_k_frequency_distribution_for_df_property(code_frequency_df, plot_properties, k=k,
                                                            save_figure_file_path=None, show_figure=False)
    non_em_count = 0
    for y_tick_label in fig.axes[0].get_yticklabels():
        if y_tick_label._text not in set_em_codes:
            y_tick_label.set_color([0.7, 0, 0])
            non_em_count += 1
    fig.axes[0].set_title(specialty.title() + ": " + CPT_CODE_SET + " code distribution")
    fig.savefig(specialty + "_" + CPT_CODE_SET + "_code_distribution_top_" + str(k) + ".png")
    fig.show()


def plot_code_group_frequency_distribution(specialty, code_set):
    """
    Plots the code group frequency distribution in an horizontal plot style using utils/plotting_utils.
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :return:
    """
    # Read data into a data frame.
    note_id_text_length_labels_df = pd.read_csv(DATA_DIR + specialty + "_notes_with_" + code_set + "_feedback.csv")
    note_id_text_length_labels_df[OutputColumns.CPT_LABELS] = note_id_text_length_labels_df[
        OutputColumns.CPT_LABELS].apply(lambda x: x.split(";"))

    # Compute property_frequency_df for plotting_utils function. Here property is "set of code labels".
    set_codes_to_frequency = sort_dict_by_value(get_set_codes_frequency(
        generate_values_in_column(note_id_text_length_labels_df, OutputColumns.CPT_LABELS)),
        reverse=True)
    set_codes_to_frequency_df = pd.DataFrame(list(set_codes_to_frequency.items()),
                                             columns=[Columns.PROPERTY, Columns.FREQUENCY])
    set_codes_to_frequency_df[Columns.PROPERTY] = set_codes_to_frequency_df[Columns.PROPERTY].apply(
        lambda x: ",".join([code for code in x]))

    # Plot the distribution
    max_frequency = list(set_codes_to_frequency.values())[0]
    plot_properties = PlotProperties(width=160, height=60, font_scale=5.5, xticks=range(0, max_frequency, 1000))
    k = 50
    fig = plot_top_k_frequency_distribution_for_df_property(set_codes_to_frequency_df, plot_properties, k=k,
                                                            save_figure_file_path=None, show_figure=False)
    fig.axes[0].set_title(specialty.title() + ": CPT code groups distribution")
    fig.savefig(specialty + "_" + code_set + "_code_group_distribution_top_" + str(k) + ".png")
    fig.show()


def get_code_group_frequency_in_train_test_validate(code_set, specialty):
    """
    This method calculates the code group frequency across the train, validate, test data sets and saves it in a csv
    file in the {DATA_DIR/auto_coding_analysis/} folder.

    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    """
    train_data = pd.read_csv(DATA_DIR + "{}_{}_train.csv".format(specialty, code_set), dtype=object)
    validate_data = pd.read_csv(DATA_DIR + "{}_{}_validate.csv".format(specialty, code_set), dtype=object)
    test_data = pd.read_csv(DATA_DIR + "{}_{}_test.csv".format(specialty, code_set), dtype=object)

    train_codes = train_data.groupby(OutputColumns.CPT_LABELS).count().reset_index().sort_values(
        OutputColumns.NOTE_ID)
    validate_codes = validate_data.groupby(OutputColumns.CPT_LABELS).count().reset_index().sort_values(
        OutputColumns.NOTE_ID)
    test_codes = test_data.groupby(OutputColumns.CPT_LABELS).count().reset_index().sort_values(
        OutputColumns.NOTE_ID)
    df_1 = test_codes.merge(validate_codes, on=OutputColumns.CPT_LABELS, suffixes=("_test", "_validate"))
    df_2 = test_codes.merge(train_codes, on=OutputColumns.CPT_LABELS, suffixes=("_test", "_train"))
    code_freq_df = df_2.merge(df_1, on=[OutputColumns.CPT_LABELS, "note_id_test", "text_test"])[
        [OutputColumns.CPT_LABELS, "note_id_train", "note_id_validate", "note_id_test"]].sort_values(
        "note_id_train",
        ascending=False)
    code_freq_df.to_csv(
        DATA_DIR + "auto_coding_analysis/" + "{}_{}_code_group_freq_tvt.csv".format(specialty, code_set),
        index=False)


if __name__ == '__main__':
    from cnn_attention.data_model.data_constants import SpecialtyNames

    plot_code_frequency_distribution(SpecialtyNames.CARDIO, CPT_CODE_SET)
    plot_code_frequency_distribution(SpecialtyNames.ORTHO, CPT_CODE_SET)
    plot_code_frequency_distribution(SpecialtyNames.PCP, CPT_CODE_SET)

    plot_code_group_frequency_distribution(SpecialtyNames.CARDIO, CPT_CODE_SET)
    plot_code_group_frequency_distribution(SpecialtyNames.ORTHO, CPT_CODE_SET)
    plot_code_group_frequency_distribution(SpecialtyNames.PCP, CPT_CODE_SET)

    for code_set_ in [CPT_CODE_SET, ICD10_CODE_SET]:
        for specialty_ in [SpecialtyNames.CARDIO, SpecialtyNames.ORTHO, SpecialtyNames.PCP, SpecialtyNames.ALL]:
            get_code_group_frequency_in_train_test_validate(code_set_, specialty_)
