import pandas as pd
import time
import numpy as np

from scipy.stats import linregress

from utils.pandas_utils import generate_values_in_column

from sklearn.metrics import recall_score, precision_score, f1_score

from utils.file_io_utils import from_pickle
from utils.dict_utils import sort_dict_by_value
from cnn_attention.utils import get_code_frequency

from cnn_attention import DATA_DIR
from cnn_attention.data_model.data_constants import OutputColumns
from cnn_attention.evaluation.test_model_performance import get_labels_column_name_for

__CODE = "code"
__FREQUENCY = "frequency"
__TEST_FREQ = "test frequency"
__RECALL = "recall"
__PRECISION = "precision"
__F1 = "f1"

__THRESH = 0.5


def get_code_to_index(set_code_labels):
    """
    Given the set of unique code labels create a mapping between index and label. This is useful for creating the
    binary arrays y_true, y_pred and y_raw.
    :param set_code_labels: the set of code labels for the given dataset.
    :return:
    """
    code_to_index = dict()
    z = 0
    for code in set_code_labels:
        code_to_index[code] = z
        z += 1
    return code_to_index


def get_set_code_labels(specialty, code_set):
    """
    Given the specialty and code set compute the unique set of code labels.
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :return: set of unique code labels.
    """
    file_name = DATA_DIR + "{}_codes_in_{}_specialty.pickle".format(code_set, specialty)
    set_code_labels = from_pickle(file_name)
    set_code_labels.add('<unk>')
    set_code_labels.add('<pad>')
    return set_code_labels


def get_y_true_y_pred_y_raw_v2(note_ids, set_code_labels, note_id_to_list_labels, note_id_to_code_to_score,
                               code_to_index, thresh=0.5):
    """
    Given raw confidence scores and ground truth predictions for every code label and every note, compute the requisite
    result arrays mentioned in the return.
    :param note_ids: all the note ids for the given dataset.
    :param set_code_labels: the set of code labels for the given dataset.
    :param note_id_to_list_labels: dictionary with key as note id and value as a list of code labels in ground truth.
    :param note_id_to_code_to_score: dictionary with key as note id and value as a dictionary with key as code and
                                     value as predicted confidence score.
    :param code_to_index: dictionary with key as code label and value as an index the code label is mapped to
    :param thresh: threshold for defining whether a code label is considered as predicted or not predicted.
    :return: Three binary 2d array each with row denoting a clinical note id and column denoting a code label such that
             a value of 1 indicates that the code label is in the ground truth (y_true), or is a prediction by the
             learning algorithm (y_pred) or is a confidence score by the learning algorithm (y_raw).
    """
    y_true = np.zeros((len(note_ids), len(set_code_labels)), dtype=float)
    y_pred = np.zeros((len(note_ids), len(set_code_labels)), dtype=float)
    y_raw = np.zeros((len(note_ids), len(set_code_labels)), dtype=float)

    t0 = time.time()
    # Set y_true and y_pred
    for i, note_id in enumerate(note_ids):
        if (i + 1) % 1000 == 0:
            print("index = {}, time elapsed = {}".format(i + 1, time.time() - t0))
            t0 = time.time()

        if note_id in note_id_to_list_labels:

            code_to_score = note_id_to_code_to_score[note_id]
            ground_truth_labels = note_id_to_list_labels[note_id]

            for code in ground_truth_labels:
                y_true[i, code_to_index[code]] = 1

            for code in code_to_score:
                raw_score = code_to_score[code]
                y_pred[i, code_to_index[code]] = 1 if raw_score >= thresh else 0
                y_raw[i, code_to_index[code]] = raw_score
        else:
            raise ValueError("Note id value not in ground truth.")
    return y_true, y_pred, y_raw


def get_consolidated_metrics(code_to_recall, code_to_precision, code_to_f1, code_to_test_freq):
    """
    This function converts the input result dictionaries of various metrics into one consolidated pandas data frame.
    :param code_to_recall: dictionary with key as code label and value as recall score
    :param code_to_precision: dictionary with key as code label and value as precision score
    :param code_to_f1: dictionary with key as code label and value as f1 score
    :param code_to_test_freq: dictionary with key as code label and value as frequency of label in test/validation
                              dataset.
    :return: consolidated resulting pandas data frame
    """
    code_f1_df = pd.DataFrame(list(code_to_f1.items()), columns=[__CODE, __F1])
    code_recall_df = pd.DataFrame(list(code_to_recall.items()), columns=[__CODE, __RECALL])
    code_precision_df = pd.DataFrame(list(code_to_precision.items()), columns=[__CODE, __PRECISION])
    code_test_freq_df = pd.DataFrame(list(code_to_test_freq.items()), columns=[__CODE, __TEST_FREQ])
    result_df = pd.merge(code_f1_df, code_precision_df, on=__CODE)
    result_df = pd.merge(code_recall_df, result_df, on=__CODE)
    result_df = pd.merge(code_test_freq_df, result_df, on=__CODE)
    result_df.sort_values(by=__F1, inplace=True, ascending=False)
    return result_df


def compute_code_metric_frequency_v2(specialty, code_set, note_id_to_list_labels, note_id_to_code_to_score, thresh=0.5):
    """
    In version 2 of this function, we accept more flexible inputs namely the ground truth data in the form of
    note_id_to_list_labels and predictions in the form of note_id_to_code_to_score. Additionally, in the returned
    data frame we do not provide overall frequency of code label since it can be inferred from the included frequency
    of code labelin the test/validation dataset.
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :param note_id_to_list_labels: dictionary with key as note id and value as a list of code labels in ground truth.
    :param note_id_to_code_to_score: dictionary with key as note id and value as a dictionary with key as code and
                                     value as predicted confidence score.
    :param thresh: threshold for defining whether a code label is considered as predicted or not predicted.
    :return: result data frame with columns as code label, test frequency, recall, precision, f1.
    """
    set_code_labels = get_set_code_labels(specialty, code_set)
    code_to_index = get_code_to_index(set_code_labels)
    note_ids = note_id_to_code_to_score.keys()
    y_true, y_pred, y_raw = get_y_true_y_pred_y_raw_v2(note_ids, set_code_labels, note_id_to_list_labels,
                                                       note_id_to_code_to_score,
                                                       code_to_index, thresh)
    code_to_recall, code_to_precision, code_to_f1, code_to_test_freq = get_code_to_metrics_given_scores(set_code_labels,
                                                                                                        code_to_index,
                                                                                                        y_true, y_pred)

    result_df = get_consolidated_metrics(code_to_recall, code_to_precision, code_to_f1, code_to_test_freq)

    return result_df


def get_y_true_y_pred_y_raw(labels, test_df, result, note_ids, set_code_labels, code_to_index, thresh):
    """
    Given raw confidence scores and ground truth predictions for every code label and every note, compute the requisite
    result arrays mentioned in the return.
    :param labels: column name corresponding to the label type as CPT or ICD10 code label.
    :param test_df: dataframe storing the ground truth
    :param result: nested dictionary storing the result with key as note id and value as a dictionary with its key
                   being a code label and value being the label's confidence score.
    :param note_ids: all the note ids for the given dataset.
    :param set_code_labels: the set of code labels for the given dataset.
    :param code_to_index: dictionary with key as code label and value as an index the code label is mapped to
    :param thresh: threshold for defining whether a code label is considered as predicted or not predicted.
    :return: Three binary 2d array each with row denoting a clinical note id and column denoting a code label such that
             a value of 1 indicates that the code label is in the ground truth (y_true), or is a prediction by the
             learning algorithm (y_pred) or is a confidence score by the learning algorithm (y_raw).
    """
    # Initialize y_true and y_pred numpy arrays.
    y_true = np.zeros((len(note_ids), len(set_code_labels)), dtype=float)
    y_pred = np.zeros((len(note_ids), len(set_code_labels)), dtype=float)
    y_raw = np.zeros((len(note_ids), len(set_code_labels)), dtype=float)

    t0 = time.time()
    # Set y_true and y_pred
    for i, note_id in enumerate(note_ids):
        if (i + 1) % 1000 == 0:
            print("index = {}, time elapsed = {}".format(i + 1, time.time() - t0))
            t0 = time.time()

        code_to_score = result[note_id]

        slice_df = test_df[test_df[OutputColumns.NOTE_ID] == note_id]
        ground_truth_labels = slice_df[labels].values.tolist()

        for code in ground_truth_labels[0]:
            y_true[i, code_to_index[code]] = 1

        for code in code_to_score:
            raw_score = code_to_score[code]
            y_pred[i, code_to_index[code]] = 1 if raw_score >= thresh else 0
            y_raw[i, code_to_index[code]] = raw_score

    return y_true, y_pred, y_raw


def get_code_to_metrics_given_scores(set_code_labels, code_to_index, y_true, y_pred):
    """
    Given the ground truth and predictions for every code label and for every note, compute precision, recall, f1 and
    test frequency per code label.
    :param set_code_labels: the set of code labels for the given dataset.
    :param code_to_index: dictionary with key as code label and value as an index the code label is mapped to
    :param y_true: Binary 2d array with row denoting a clinical note id and column denoting a code label such that a
                   value of 1 indicates that the code label is in the ground truth for the corresponding note.
    :param y_pred: Binary 2d array with row denoting a clinical note id and column denoting a code label such that a
                   value of 1 indicates that the code label is predicted by the learning algorithm for the corresponding
                   note.
    :return: Four dictionaries with key as code label and value as precision, recall, f1, test dataset frequency
             respectively.
    """
    code_to_recall = dict()
    code_to_precision = dict()
    code_to_f1 = dict()
    code_to_test_freq = dict()
    for code in set_code_labels:
        index = code_to_index[code]
        code_to_recall[code] = recall_score(y_true=y_true[:, index], y_pred=y_pred[:, index])
        code_to_precision[code] = precision_score(y_true=y_true[:, index], y_pred=y_pred[:, index])
        code_to_f1[code] = f1_score(y_true=y_true[:, index], y_pred=y_pred[:, index])
        code_to_test_freq[code] = int(sum(y_true[:, index]))

    return code_to_recall, code_to_precision, code_to_f1, code_to_test_freq


def get_code_to_metrics(test_scores, ground_truth_labels_file_path, specialty, code_set,
                        is_path_test_scores=True):
    """
    For every code label compute precision, recall, f1 and test frequency.
    :param test_scores: Score values or the file path to score values predicted for test set.
    :param ground_truth_labels_file_path: Ground truth labels for the test set.
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :param is_path_test_scores: If True, test_scores parameter defines a file path to score values, otherwise defines
                                the actual score values themselves.
    :return: Four dictionaries with key as code label and value as precision, recall, f1, test dataset frequency
             respectively, raw probability scores array with code label and note as dimensions along with a dictionary
             with key as code and value as the index in the raw scores array.
    """

    labels = get_labels_column_name_for(code_set)

    # Read the ground-truth labels
    test_df = pd.read_csv(ground_truth_labels_file_path, dtype=object)
    test_df[labels] = test_df[labels].apply(lambda x: x.split(";"))

    # Read result
    if is_path_test_scores:
        result = from_pickle(test_scores)
    else:
        result = test_scores

    # For each unique label (i.e., unique code from cardio specialty labels data) we map it to a unique index.
    # This is useful to create y_true (ground truth labels) and y_pred (predictions) arrays needed for metric
    # computation.
    note_ids = list(result.keys())

    set_code_labels = get_set_code_labels(specialty, code_set)
    code_to_index = get_code_to_index(set_code_labels)
    y_true, y_pred, y_raw = get_y_true_y_pred_y_raw(labels, test_df, result, note_ids, set_code_labels, code_to_index,
                                                    __THRESH)
    code_to_recall, code_to_precision, code_to_f1, code_to_test_freq = get_code_to_metrics_given_scores(set_code_labels,
                                                                                                        code_to_index,
                                                                                                        y_true, y_pred)
    return code_to_recall, code_to_precision, code_to_f1, code_to_test_freq, y_raw, code_to_index


def compute_code_metric_frequency(specialty, code_set, test_scores, model_name, is_path_test_scores=True,
                                  save_result=True, is_validate=False):
    """
    Compute the different metrics such as precision, recall, f1 per code label along with the frequency in test data.
    A csv file with the results is stored on disk in the present working directory.
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :param test_scores: Score values or the file path to score values predicted for test set.
    :param model_name: The model directory folder name.
    :param is_path_test_scores: If True, test_scores parameter defines a file path to score values, otherwise defines
                                the actual score values themselves.
    :param save_result: If True, the results dataframe is stored in a csv file.
    :param is_validate: If True, this is computation is for validation data set as opposed to test data set.
    :return: result data frame with columns as code label, test frequency, recall, precision, f1, and total frequency.
    """

    ground_truth_labels_file_path = DATA_DIR + "{}_{}_{}.csv".format(specialty, code_set,
                                                                     "validate" if is_validate else "test")
    code_to_recall, code_to_precision, code_to_f1, code_to_test_freq, y_raw, code_to_index = get_code_to_metrics(
        test_scores,
        ground_truth_labels_file_path,
        specialty,
        code_set,
        is_path_test_scores)

    note_id_text_length_labels_df = pd.read_csv(DATA_DIR + specialty + "_notes_with_" + code_set + "_feedback.csv",
                                                dtype=object)
    note_id_text_length_labels_df[OutputColumns.CPT_LABELS] = note_id_text_length_labels_df[
        OutputColumns.CPT_LABELS].apply(lambda x: x.split(";"))

    # Compute property_frequency_df for plotting_utils function. Here property is "code label".
    code_to_frequency = sort_dict_by_value(get_code_frequency(
        generate_values_in_column(note_id_text_length_labels_df, OutputColumns.CPT_LABELS)),
        reverse=True)
    code_frequency_df = pd.DataFrame(list(code_to_frequency.items()), columns=[__CODE, __FREQUENCY])
    code_f1_df = pd.DataFrame(list(code_to_f1.items()), columns=[__CODE, __F1])
    code_recall_df = pd.DataFrame(list(code_to_recall.items()), columns=[__CODE, __RECALL])
    code_precision_df = pd.DataFrame(list(code_to_precision.items()), columns=[__CODE, __PRECISION])
    code_test_freq_df = pd.DataFrame(list(code_to_test_freq.items()), columns=[__CODE, __TEST_FREQ])
    result_df = pd.merge(code_f1_df, code_frequency_df, on=__CODE)
    result_df = pd.merge(code_precision_df, result_df, on=__CODE)
    result_df = pd.merge(code_recall_df, result_df, on=__CODE)
    result_df = pd.merge(code_test_freq_df, result_df, on=__CODE)
    result_df.sort_values(by=__F1, inplace=True, ascending=False)
    if save_result:
        result_df.to_csv("{}_{}_thresh_{}_{}.csv".format(code_set, specialty, __THRESH, model_name), index=False)
    print(linregress(result_df[__TEST_FREQ], result_df[__F1]))
    return result_df
