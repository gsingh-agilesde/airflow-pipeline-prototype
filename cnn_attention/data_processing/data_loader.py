import itertools
import time

import torch
from torch.autograd import Variable
from torchtext.vocab import Vectors
from torchtext.data import Field, BucketIterator, Dataset, Example, Iterator
from torchtext.data import TabularDataset
import pandas as pd

from cnn_attention import DATA_DIR, ICD10_CODE_SET, CPT_LABEL_CODE_SETS
from cnn_attention.data_model.data_constants import OutputColumns, SpecialtyNames
from utils.file_io_utils import from_pickle, to_pickle


class DataFields:
    """
    class for defining the Fields for the datasets required by the model.
    """


    def __init__(self, label_sep=";"):
        self.TEXT = Field(sequential=True, tokenize=lambda x: x.split(), lower=True)
        self.ID = Field(sequential=False)
        self.LABEL = Field(sequential=True, tokenize=lambda x: x.split(label_sep), unk_token=None, pad_token=None)


    def build_vocab_for_text(self, vocab_itr, vectors):
        """
        This method uses an iterator of words to build the vocab for TEXT field
        :param vocab_itr: Iterator containing the words
        :param vectors: word embeddings
        :return: builds the vocab for the TEXT field
        """
        vocab_list = [[word] for word in vocab_itr]
        self.TEXT.build_vocab(vocab_list, vectors=vectors)


    def build_vocab_for_label(self, vocab_itr):
        """
        This method uses an iterator of words to build the vocab for LABEL field
        :param vocab_itr: Iterator containing the words
        :return: builds the vocab for the LABEL field
        """
        vocab_list = [[word] for word in vocab_itr]
        self.LABEL.build_vocab(vocab_list)


    def build_vocab_for_id(self, vocab_itr):
        """
        This method uses an iterator of words to build the vocab for ID field
        :param vocab_itr: Iterator containing the words
        :return: builds the vocab for the ID field
        """
        vocab_list = [[word] for word in vocab_itr]
        self.ID.build_vocab(vocab_list)


class DataIterator:
    """
    This class creates the iterators for the train, test and validate splits.
    """

    __TRAIN = "train"
    __VALIDATE = "validate"
    __TEST = "test"


    def __init__(self, data_path, code_set, specialty=SpecialtyNames.CARDIO, label_sep=";",
                 build_data_generators=True):
        """
        Defines the fields for the splits and builds the vocab for the required fields.
        :param data_path: the path to the file containing the entire dataset.
        :param code_set: the code_set for which the set of labels should be returned. It supports ICD10 and CPT
        :param specialty: the speciality for which the model should be trained.
        :param label_sep: the separator used for storing the code labels
        :param build_data_generators: flag to specify if the data model should be able to
         build and generate batches from the dataset located at {data_path}

        Note:
        Only values in the class SpecialityNames are supported.
        """
        self.data_path = data_path
        self.code_set = code_set
        self.specialty = specialty
        self.label_sep = label_sep

        self.data_fields = DataFields()
        self.train_data_fields = [(OutputColumns.NOTE_ID, self.data_fields.ID),
                                  (OutputColumns.TEXT, self.data_fields.TEXT)]
        if code_set in CPT_LABEL_CODE_SETS:
            self.train_data_fields.append((OutputColumns.CPT_LABELS, self.data_fields.LABEL))
            self.label_column = OutputColumns.CPT_LABELS
        elif code_set == ICD10_CODE_SET:
            self.train_data_fields.append((OutputColumns.ICD_LABELS, self.data_fields.LABEL))
            self.label_column = OutputColumns.ICD_LABELS
        else:
            raise ValueError("code_set {} is not supported.".format(self.code_set))

        vocab_file = DATA_DIR + "{}_{}_vocab.pickle".format(self.specialty, self.code_set)
        vocab = from_pickle(vocab_file)

        vectors = Vectors(DATA_DIR + "{}_{}_processed_full.embed".format(self.specialty, self.code_set))

        codes = self.__get_codes_in_label()
        # build vocabs for the fields.
        self.data_fields.build_vocab_for_label(codes)
        self.data_fields.build_vocab_for_text(vocab, vectors)

        if build_data_generators:
            note_ids = self.__get_note_ids_in_dataset()
            self.data_fields.build_vocab_for_id(note_ids)

        self.num_words = len(self.data_fields.TEXT.vocab.itos)
        self.num_labels = len(self.data_fields.LABEL.vocab.itos)


    def __get_codes_in_label(self):
        """
        This method saves and returns the set of codes in the dataset to a pickle file
        If the file already exists, it reads it and returns the codes.
        """
        file_name = DATA_DIR + "{}_codes_in_{}_specialty.pickle".format(self.code_set, self.specialty)

        try:
            return from_pickle(file_name)
        except FileNotFoundError:
            df = pd.read_csv(self.data_path, dtype=object)
            df[self.label_column] = df[self.label_column].apply(lambda x: str(x).split(self.label_sep))
            codes = set(itertools.chain.from_iterable(df[self.label_column].values))
            to_pickle(file_name, codes)
        return codes


    def __get_note_ids_in_dataset(self):
        """
        This method returns the set of note_ids in the dataset.
        """
        df = pd.read_csv(self.data_path)
        note_ids = set(df[OutputColumns.NOTE_ID].values.tolist())
        return note_ids


    def get_train_validate_test_data(self, train_path):
        """
        This method loads and returns the training, validation and test data set in the appropriate format.
        :param train_path: Path to training data set. Since validation and test data sets are also assumed to live in
                           the same directory, we only need the path to training data set.
        :return: training, validation and test data sets.
        """
        t1 = time.time()
        trn, vld, test = TabularDataset.splits(
            path=DATA_DIR,
            train=train_path,
            validation=train_path.replace(self.__TRAIN, self.__VALIDATE),
            test=train_path.replace(self.__TRAIN, self.__TEST),
            format='csv',
            skip_header=True,
            fields=self.train_data_fields)
        print("created train and validate dataset in {} sec".format(str(time.time() - t1)))

        return trn, vld, test


    @staticmethod
    def get_train_validate_data_itr(batch_size, train_data, validate_data, sort=True):
        """
        This method returns the data iterators for the input training and validation data sets.
        :param batch_size: batch size.
        :param train_data: train data set.
        :param validate_data: validation data set.
        :param sort: sorts the input data by the length of the input text.
        :return: iterators to training and validation data sets.
        """
        t1 = time.time()
        train_iter, val_iter = BucketIterator.splits(
            (train_data, validate_data),
            sort=sort,  # If True, this sorts the dataset in ascending order. Required for efficient padding.
            batch_sizes=(batch_size, batch_size),
            sort_key=lambda x: len(x.text),
            sort_within_batch=False,
            repeat=False
        )
        print("created train and validate itr in {} sec".format(str(time.time() - t1)))
        return train_iter, val_iter


    def create_dataset_from_list_of_dict(self, data, note_id_field, content_field):
        """
        This method creates an object of type torchtext.data.Dataset from a list of dictionaries.
        :param data: list of dictionaries containing the note_id and content
        :param note_id_field: name of the note_id field
        :param content_field: name of the content field
        :return: Dataset object
        """

        t1 = time.time()
        note_ids = [z[note_id_field] for z in data]
        self.data_fields.build_vocab_for_id(note_ids)
        fields_list = [(note_id_field, self.data_fields.ID), (content_field, self.data_fields.TEXT)]
        fields = {note_id_field: (note_id_field, self.data_fields.ID),
                  content_field: (content_field, self.data_fields.TEXT)}
        examples = [Example.fromdict(val, fields) for val in data]
        test = Dataset(examples, fields_list)
        print("created test data set in {} sec".format(str(time.time() - t1)))
        return test


    @staticmethod
    def get_test_data_itr(test_data, x_field, sort=True, is_train=False, batch_size="default"):
        """
        This method returns the data iterator to the input test data set.
        :param test_data: test data set.
        :param x_field: x field column name str (features)
        :param sort: sorts the input data by the length of the input text.
        :param is_train: If True, then data is loaded for model training mode,
                         otherwise for model prediction mode.
        :param batch_size: batch size.
        :return: iterator to test data set.
        """
        return Iterator(test_data,
                        train=is_train,
                        sort=sort,  # sorts the dataset in ascending order. Required for efficient padding.
                        batch_size=len(test_data) if str(batch_size) == "default" else batch_size,
                        sort_key=lambda x: getattr(x, x_field),
                        sort_within_batch=False,
                        repeat=False
                        )


class BatchGenerator:
    """
    This class is a simple wrapper to make the batches easy to use.
    It takes in the names of the fields in the dataIterator and returns the batch with the given names.
    """


    def __init__(self, dl, id_field, x_field, y_field=None, num_labels=None):
        """
        :param dl: The iterator generated by the data loader
        :param id_field: the id filed in the dataset
        :param x_field: the data/text field in the dataset
        :param y_field: the label field in the dataset
        :param num_labels: number of labels in the dataset
        """
        self.dl = dl
        self.id_field = id_field
        self.x_field = x_field
        self.y_field = y_field
        self.num_labels = num_labels


    def __len__(self):
        return len(self.dl)


    def __iter__(self):
        """
        :return: an iterator of tuples containing the id, data and label
        """
        for batch in self.dl:
            id_ = getattr(batch, self.id_field)
            x = getattr(batch, self.x_field)
            x = x.t()
            x = Variable(torch.LongTensor(x))
            if self.y_field:
                y_ = getattr(batch, self.y_field)
                y_ = y_.t()
                y = torch.zeros(y_.shape[0], self.num_labels)
                for row, val in enumerate(y_):
                    for v in val.numpy():
                        if v != 1.0:
                            try:
                                y[row, v] = 1.0
                            except IndexError as ex:
                                print("Encountered an index exception: {}".format(ex))
            else:
                y = None
            yield (id_, x, y)


class DataLoader:

    def __init__(self, specialty, code_set, rest_api):
        """
        :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class
                          SpecialtyNames.
        :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
        :param rest_api: True if predictions are to be obtained with a pre-trained model for the rest api endpoint.
        """
        self.specialty = specialty
        self.code_set = code_set
        self.data_iterator = DataIterator(
            DATA_DIR + "{}_notes_with_{}_feedback.csv".format(self.specialty, self.code_set), self.code_set,
            specialty=self.specialty, build_data_generators=not rest_api)
        self.label_col = self.__get_label_col()


    def __get_label_col(self):
        """
        Checks if code set is valid and returns the appropriate label column name str
        :return: label column name str
        """
        if self.code_set in CPT_LABEL_CODE_SETS:
            label_col = OutputColumns.CPT_LABELS
        elif self.code_set == ICD10_CODE_SET:
            label_col = OutputColumns.ICD_LABELS
        else:
            raise ValueError("{} code set is not defined. Choose a valid code set.".format(self.code_set))
        return label_col


    def load_datasets(self):
        """
        :return: datasets for training, validation and testing. These are of type torchtext.data.TabularDataset
        """
        train_data, validate_data, test_data = self.data_iterator.get_train_validate_test_data(
            train_path=DATA_DIR + "{}_{}_train.csv".format(self.specialty, self.code_set))
        return train_data, validate_data, test_data


    def get_train_validate_batch_generators(self, batch_size, train_data, validate_data, sort=True):
        """
        Encapsulates the training and validation datasets in batches using the BatchGenerator class. This is
        required for batching of data. Here the batch_size and sort serve as two hyperparameters.
        :param batch_size: batch size.
        :param train_data: train data set.
        :param validate_data: validation data set.
        :param sort: sorts the input data by the length of the input text.
        :return: batch generators for input train and validation datasets
        """
        train_itr, validate_itr = self.data_iterator.get_train_validate_data_itr(batch_size, train_data,
                                                                                 validate_data, sort=sort)

        train_batches_gen = BatchGenerator(train_itr,
                                           OutputColumns.NOTE_ID,
                                           OutputColumns.TEXT,
                                           y_field=self.label_col,
                                           num_labels=self.data_iterator.num_labels)
        validate_batches_gen = BatchGenerator(validate_itr,
                                              OutputColumns.NOTE_ID,
                                              OutputColumns.TEXT,
                                              y_field=self.label_col,
                                              num_labels=self.data_iterator.num_labels)

        return train_batches_gen, validate_batches_gen


    def get_test_batch_generator(self, test_data, id_field, x_field, y_field=None, sort=True, is_train=False,
                                 batch_size="default"):
        """
        Encapsulates the test dataset using the BatchGenerator class. This is designed slightly different since it will
        be used by the model_prediction.py module which is called by external projects. Here we accept additional inputs
        as id_field and x_field so that the external projects have control over the naming conventions for the id column
        and x_field column names.
        :param test_data: test data set
        :param id_field: id column name str
        :param x_field: x field column name str (features)
        :param y_field: optional y field that represents the output labels. Default value is None. Only during model
                        training mode, this parameter would have a value other than None.
        :param sort: sorts the input data by the length of the input text.
        :param is_train: If True, then data is loaded for model training mode,
                         otherwise for model prediction mode.
        :param batch_size: batch size.
        :return: Batch generator for input test data set.
        """
        test_itr = self.data_iterator.get_test_data_itr(test_data, x_field, sort=sort, is_train=is_train,
                                                        batch_size=batch_size)
        test_batches_gen = BatchGenerator(test_itr, id_field, x_field, y_field,
                                          num_labels=self.data_iterator.num_labels)
        return test_batches_gen


    def get_test_batch_generator_from_list_dict(self, data, note_id_field, content_field):
        """
                This method loads data from a list of dictionaries into batches of specified size.
                :param data: list of dictionaries containing the note_id and content
                :param note_id_field: name of the note_id field
                :param content_field: name of the content field (features)
                :return: data iterator for the data set
                """
        t1 = time.time()
        test_data = self.data_iterator.create_dataset_from_list_of_dict(data, note_id_field, content_field)
        test_batches_gen = self.get_test_batch_generator(test_data, note_id_field, content_field)
        print("created test data generator in {} sec".format(str(time.time() - t1)))
        return test_batches_gen
