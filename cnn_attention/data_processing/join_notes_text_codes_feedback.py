import time
import pandas as pd

from data_processing_global import set_cpt_codes, set_icd10_cm_codes
from utils.file_io_utils import from_text, file_names_from_directory

from cnn_attention.utils import process_note, extract_note_id
from cnn_attention.data_model.data_constants import OutputColumns
from cnn_attention import DATA_DIR, ICD10_CODE_SET, CPT_CODE_SET


class CodesFeedbackColumns:
    """
    Stores the column names from two tables storing codes feedback, one for CPT and the other for ICD-10-CM.
    These are named cpt_table_id_in_*.csv and icd_table_id_in_*.csv must be in cnn_attention/data_files/ folder.
    The * here represents the specialty: "cardio", "ortho" or "pcp".
    """
    HV_PROC_ID = "hv_proc_id"
    PROC_CD = "proc_cd"
    HV_DIAG_ID = "hv_diag_id"
    DIAG_CD = "diag_cd"


# Temporary column name. We construct this column to only sort the data frame by length.
__TEXT_LENGTH = "text_length"


def join_notes_and_labels(specialty):
    """
    Based on the selected specialty, this script joins the clinical notes and the corresponding labels table to create
    a single consolidated data frame that can be used for further downstream preprocessing tasks. The script saves this
    data frame to a csv file
    ############
    Requisites:
    ############
    1. Labels tables:  cpt_table_id_in_*.csv and icd_table_id_in_*.csv must be in cnn_attention/data_files/ folder.
                       The link to this folder is given by the DATA_DIR constant defined in cnn_attention/__init__.py.
    2. Clincial notes: *_notes_edited folders containing the clinical notes must also be in cnn_attention/data_files/
                       folder.
    ############
    Note:
    ############
    The * here represents the specialty and must be replaced with one of the three options: cardio, ortho or pcp.
    These options are defined under cnn_attention/data_model/data_constants class SpecialtyNames.

    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :return:
    """

    # Set base path for clinical notes.
    notes_base_path = DATA_DIR + specialty + "_notes_edited/"
    # List of file names for the clinical notes
    specialty_notes_file_names = file_names_from_directory(notes_base_path)

    # Set base path for the feedback files.
    cpt_feedback_file_name = "cpt_table_id_in_" + specialty + ".csv"
    icd_feedback_file_name = "icd_table_id_in_" + specialty + ".csv"

    # Extract pandas data frame with Note id, CPT feedback
    temp_df = pd.read_csv(DATA_DIR + cpt_feedback_file_name, dtype=object)
    cpt_feedback_df = temp_df[[CodesFeedbackColumns.HV_PROC_ID, CodesFeedbackColumns.PROC_CD]]
    cpt_feedback_df.rename(
        columns={CodesFeedbackColumns.HV_PROC_ID: OutputColumns.NOTE_ID,
                 CodesFeedbackColumns.PROC_CD: OutputColumns.CPT_LABELS},
        inplace=True)
    cpt_feedback_df.dropna(inplace=True)
    # Keep only legitimate CPT codes per 2018 CPT link.
    cpt_feedback_df = cpt_feedback_df[cpt_feedback_df[OutputColumns.CPT_LABELS].isin(set_cpt_codes)]
    temp_series = cpt_feedback_df.groupby(OutputColumns.NOTE_ID)[OutputColumns.CPT_LABELS].apply(set)
    cpt_feedback_df = pd.DataFrame(
        {OutputColumns.NOTE_ID: temp_series.index, OutputColumns.CPT_LABELS: temp_series.values})

    # Extract pandas data frame with Note id, ICD-10-CM feedback
    temp_df = pd.read_csv(DATA_DIR + icd_feedback_file_name, dtype=object)
    icd_feedback_df = temp_df[[CodesFeedbackColumns.HV_DIAG_ID, CodesFeedbackColumns.DIAG_CD]]
    icd_feedback_df.rename(
        columns={CodesFeedbackColumns.HV_DIAG_ID: OutputColumns.NOTE_ID,
                 CodesFeedbackColumns.DIAG_CD: OutputColumns.ICD_LABELS},
        inplace=True)
    icd_feedback_df.dropna(inplace=True)
    # Keep only legitimate ICD10CM codes per 2019 standards.
    icd_feedback_df = icd_feedback_df[icd_feedback_df[OutputColumns.ICD_LABELS].isin(set_icd10_cm_codes)]
    temp_series = icd_feedback_df.groupby(OutputColumns.NOTE_ID)[OutputColumns.ICD_LABELS].apply(set)
    icd_feedback_df = pd.DataFrame(
        {OutputColumns.NOTE_ID: temp_series.index, OutputColumns.ICD_LABELS: temp_series.values})

    # Define a empty pandas data frame with NOTE id, Text, Text length.
    notes_full_df = pd.DataFrame(columns=[OutputColumns.NOTE_ID, OutputColumns.TEXT, __TEXT_LENGTH],
                                 index=range(len(specialty_notes_file_names)))

    # Populate the above defined data frame row by row.
    t0 = time.time()
    for i, file_name in enumerate(specialty_notes_file_names):
        if (i + 1) % 10000 == 0:
            print("elapsed time for 10000 itrs = {} sec.".format(time.time() - t0))
            t0 = time.time()
        note_id = extract_note_id(file_name)
        # note_type = extract_note_type(file_name)
        file_path = notes_base_path + file_name
        note_text = "".join(from_text(file_path))
        preprocessed_note_text, _ = process_note(note_text)
        notes_full_df.loc[i] = pd.Series({OutputColumns.NOTE_ID: note_id, OutputColumns.TEXT: preprocessed_note_text,
                                          __TEXT_LENGTH: len(preprocessed_note_text.split())})

    # Join the data frames storing Notes Info and CPT Feedback info on the unique Note id field.
    note_id_text_length_cpt_df = pd.merge(notes_full_df, cpt_feedback_df, on=OutputColumns.NOTE_ID)
    # Join the data frames storing Notes Info and ICD-10-CM Feedback info on the unique Note id field.
    note_id_text_length_icd_df = pd.merge(notes_full_df, icd_feedback_df, on=OutputColumns.NOTE_ID)

    # Sort the data frames by the length of the words in the note.
    note_id_text_length_cpt_df.sort_values(by=[__TEXT_LENGTH], inplace=True)
    note_id_text_length_icd_df.sort_values(by=[__TEXT_LENGTH], inplace=True)

    # Remove the temporary __TEXT_LENGTH column.
    note_id_text_length_cpt_df.drop(__TEXT_LENGTH, axis=1, inplace=True)
    note_id_text_length_icd_df.drop(__TEXT_LENGTH, axis=1, inplace=True)

    # Reformat the sets of labels according to requirements -- Code separated by semi-colon, i.e., ';' character.
    note_id_text_length_cpt_df[OutputColumns.CPT_LABELS] = note_id_text_length_cpt_df[OutputColumns.CPT_LABELS].apply(
        lambda codes: ";".join([code for code in codes]))
    note_id_text_length_icd_df[OutputColumns.ICD_LABELS] = note_id_text_length_icd_df[OutputColumns.ICD_LABELS].apply(
        lambda codes: ";".join([code for code in codes]))

    # Store the data frames to csv files.
    note_id_text_length_cpt_df.to_csv(DATA_DIR + specialty + "_notes_with_" + CPT_CODE_SET + "_feedback.csv",
                                      index=False)
    note_id_text_length_icd_df.to_csv(DATA_DIR + specialty + "_notes_with_" + ICD10_CODE_SET + "_feedback.csv",
                                      index=False)


if __name__ == '__main__':
    from cnn_attention.data_model.data_constants import SpecialtyNames

    join_notes_and_labels(SpecialtyNames.CARDIO)
    join_notes_and_labels(SpecialtyNames.ORTHO)
    join_notes_and_labels(SpecialtyNames.PCP)
