import pandas as pd

from utils.file_io_utils import from_pickle, to_pickle

from cnn_attention import DATA_DIR

from cnn_attention.data_model.data_constants import SpecialtyNames
from cnn_attention import CPT_CODE_SET, ICD10_CODE_SET


def make_files_all_specialty(code_set, all_specialty=SpecialtyNames.ALL):
    """
    From preprocessing_readme.txt, for the 'all' specialty, we need to combine the files numbered
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :param all_specialty: Optional parameter to specify the type of "all" specialty used. This is done primarily for
                          allowing variants on all specialty.
    :return:
    """

    # make: all_CPT_train.csv
    cardio_train_df = pd.read_csv(DATA_DIR + "{}_{}_train.csv".format(SpecialtyNames.CARDIO, code_set))
    ortho_train_df = pd.read_csv(DATA_DIR + "{}_{}_train.csv".format(SpecialtyNames.ORTHO, code_set))
    pcp_train_df = pd.read_csv(DATA_DIR + "{}_{}_train.csv".format(SpecialtyNames.PCP, code_set))
    frames = [cardio_train_df, ortho_train_df, pcp_train_df]
    all_train_df = pd.concat(frames)
    all_train_df.to_csv(DATA_DIR + "{}_{}_train.csv".format(all_specialty, code_set), index=False)

    # make: all_CPT_validate.csv
    cardio_validate_df = pd.read_csv(DATA_DIR + "{}_{}_validate.csv".format(SpecialtyNames.CARDIO, code_set))
    ortho_validate_df = pd.read_csv(DATA_DIR + "{}_{}_validate.csv".format(SpecialtyNames.ORTHO, code_set))
    pcp_validate_df = pd.read_csv(DATA_DIR + "{}_{}_validate.csv".format(SpecialtyNames.PCP, code_set))
    frames = [cardio_validate_df, ortho_validate_df, pcp_validate_df]
    all_validate_df = pd.concat(frames)
    all_validate_df.to_csv(DATA_DIR + "{}_{}_validate.csv".format(all_specialty, code_set), index=False)

    # make: all_CPT_test.csv
    cardio_test_df = pd.read_csv(DATA_DIR + "{}_{}_test.csv".format(SpecialtyNames.CARDIO, code_set))
    ortho_test_df = pd.read_csv(DATA_DIR + "{}_{}_test.csv".format(SpecialtyNames.ORTHO, code_set))
    pcp_test_df = pd.read_csv(DATA_DIR + "{}_{}_test.csv".format(SpecialtyNames.PCP, code_set))
    frames = [cardio_test_df, ortho_test_df, pcp_test_df]
    all_test_df = pd.concat(frames)
    all_test_df.to_csv(DATA_DIR + "{}_{}_test.csv".format(all_specialty, code_set), index=False)

    cardio_note_ids = from_pickle(DATA_DIR + SpecialtyNames.CARDIO + "_" + code_set + "_train_note_ids.pickle")
    ortho_note_ids = from_pickle(DATA_DIR + SpecialtyNames.ORTHO + "_" + code_set + "_train_note_ids.pickle")
    pcp_note_ids = from_pickle(DATA_DIR + SpecialtyNames.PCP + "_" + code_set + "_train_note_ids.pickle")
    all_note_ids = cardio_note_ids.union(ortho_note_ids).union(pcp_note_ids)
    to_pickle(DATA_DIR + all_specialty + "_" + code_set + "_train_note_ids.pickle", all_note_ids)

    cardio_note_ids = from_pickle(DATA_DIR + SpecialtyNames.CARDIO + "_" + code_set + "_validate_note_ids.pickle")
    ortho_note_ids = from_pickle(DATA_DIR + SpecialtyNames.ORTHO + "_" + code_set + "_validate_note_ids.pickle")
    pcp_note_ids = from_pickle(DATA_DIR + SpecialtyNames.PCP + "_" + code_set + "_validate_note_ids.pickle")
    all_note_ids = cardio_note_ids.union(ortho_note_ids).union(pcp_note_ids)
    to_pickle(DATA_DIR + all_specialty + "_" + code_set + "_validate_note_ids.pickle", all_note_ids)

    cardio_note_ids = from_pickle(DATA_DIR + SpecialtyNames.CARDIO + "_" + code_set + "_test_note_ids.pickle")
    ortho_note_ids = from_pickle(DATA_DIR + SpecialtyNames.ORTHO + "_" + code_set + "_test_note_ids.pickle")
    pcp_note_ids = from_pickle(DATA_DIR + SpecialtyNames.PCP + "_" + code_set + "_test_note_ids.pickle")
    all_note_ids = cardio_note_ids.union(ortho_note_ids).union(pcp_note_ids)
    to_pickle(DATA_DIR + all_specialty + "_" + code_set + "_test_note_ids.pickle", all_note_ids)

    # Make embeddings.



if __name__ == '__main__':
    make_files_all_specialty(ICD10_CODE_SET)
    make_files_all_specialty(CPT_CODE_SET)
