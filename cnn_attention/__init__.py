import os
import nltk

nltk.download('stopwords')

from constants.code_constants import *

EMBEDDING_SIZE = 100
MAX_LENGTH = 2500

PARENT_DIR = os.path.dirname(os.path.realpath(__file__))

# where you want to save any models you may train
MODEL_DIR = PARENT_DIR + '/saved_models/'
SAVED_MODEL_NAME = "/model.pth"

CNN = "conv_attn"
MULTI_VIEW_CNN = "multi_view_conv_attn"
# where you want to store or have stored the data.
DATA_DIR = PARENT_DIR + '/data_files/'

CUDA = "cuda:"
