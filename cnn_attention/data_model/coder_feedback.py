class CoderFeedback:
    FILE_NAME = "fileName"
    SPECIALTY = "specialty"
    CPT_CODES = "cptCodes"
    ICD_CODES = "icdCodes"
    CODE = "code"
    MODIFIER = "modifier"
    CODE_SET = "codeSet"
    CODE_SOURCE = "codeSource"
    MENTION = "mention"
    START_POS = "startPos"
    END_POS = "endPos"
    CONFIDENCE = "confidence"
    REASON = "reason"


class Confidence:
    YES = "YES"
    NO = "NO"
    MAYBE = "MAYBE"
    NONE = None


class Specialty:
    CARDIO = "CARDIO"
    ORTHO = "ORTHO"
    PCP = "PCP"
