class OutputColumns:
    """
    Stores the column names for the output data frames or csv files that will be generated as part of data
    preprocessing.
    """
    NOTE_ID = "note_id"
    TEXT = "text"
    ICD_LABELS = "icd_labels"
    CPT_LABELS = "cpt_labels"


class SpecialtyNames:
    """
    Documents the names of the specialties used for the 300K clinical notes data set.
    """
    CARDIO = "cardio"
    ORTHO = "ortho"
    PCP = "pcp"
    ALL = "all"
    CARDIO_NO_EM = "cardio_no_em"
    ORTHO_NO_EM = "ortho_no_em"
    PCP_NO_EM = "pcp_no_em"
    ALL_NO_EM = "all_no_em"
    LOS_ALAMITOS = "los_alamitos"
    LOS_ALAMITOS_NO_CODES = "los_alamitos_no_codes"
    ALL_ENTITIES = "all_entities"
    MAMMOGRAPHY = "mammography"
    MRI = "mri"
    XRAY = "xray"
    CT_SCAN = "ct_scan"
