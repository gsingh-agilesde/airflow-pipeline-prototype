import unittest
from unittest.mock import patch

from cnn_attention import utils


class TestUtils(unittest.TestCase):

    def setUp(self):
        # For testing process_note()
        self.test_text = "Doctor X made the following diagnosis: Since yesterday 10/10, constant Tylenol 1 hr ago. " \
                         "+nausea."
        self.expected_processed_text = "doctor x made the following diagnosis since yesterday constant tylenol hr " \
                                       "ago nausea"
        self.expected_mapping = {0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 9, 9: 10, 10: 12, 11: 13, 12: 14}

        # For testing extract_note_id(), extract_note_type()
        self.test_filename = "Cardio_5_2018-04-20_10418_73027266.txt"
        self.expected_note_id = "5_2018-04-20_10418_73027266"
        self.expected_note_type = "Cardio"

        code_1 = "99202"
        code_2 = "00100"
        code_set_1 = frozenset({code_1, code_2})
        code_set_2 = frozenset({code_1})

        # For testing get_code_frequency.
        self.test_set_codes_generator_v1 = iter([code_set_1, code_set_2])
        self.expected_key_1_v1, self.expected_value_1_v1 = code_1, 2
        self.expected_key_2_v1, self.expected_value_2_v1 = code_2, 1

        # For testing get_set_codes_frequency.
        self.test_set_codes_generator_v2 = iter([code_set_1, code_set_2])
        self.expected_key_1_v2, self.expected_value_1_v2 = code_set_1, 2
        self.expected_key_2_v2, self.expected_value_2_v2 = code_set_2, 1


    def test_process_note(self):
        processed_text, mapping = utils.process_note(self.test_text)
        self.assertEqual(self.expected_processed_text, processed_text)
        self.assertEqual(self.expected_mapping, mapping)


    def test_extract_note_id(self):
        note_id = utils.extract_note_id(self.test_filename)
        self.assertEqual(self.expected_note_id, note_id)


    def test_extract_note_type(self):
        note_type = utils.extract_note_type(self.test_filename)
        self.assertEqual(self.expected_note_type, note_type)


    def __side_effect_inc_key_in_dict_v1(self, dict_, k_):
        dict_[self.expected_key_1_v1] = self.expected_value_1_v1
        dict_[self.expected_key_2_v1] = self.expected_value_2_v1


    def __side_effect_inc_key_in_dict_v2(self, dict_, k_):
        dict_[self.expected_key_1_v2] = self.expected_value_1_v2
        dict_[self.expected_key_2_v2] = self.expected_value_2_v2


    def test_get_code_frequency(self):
        with patch.object(utils, 'inc_key_in_dict', side_effect=self.__side_effect_inc_key_in_dict_v1):
            result_ = utils.get_code_frequency(self.test_set_codes_generator_v1)
            self.assertEqual(result_[self.expected_key_1_v1], self.expected_value_1_v1)
            self.assertEqual(result_[self.expected_key_2_v1], self.expected_value_2_v1)


    def test_get_set_codes_frequency(self):
        with patch.object(utils, 'inc_key_in_dict', side_effect=self.__side_effect_inc_key_in_dict_v2):
            result_ = utils.get_set_codes_frequency(self.test_set_codes_generator_v2)
            self.assertEqual(result_[self.expected_key_1_v2], self.expected_value_1_v2)
            self.assertEqual(result_[self.expected_key_2_v2], self.expected_value_2_v2)


    # Integration tests...
    def test_it_get_code_frequency(self):
        result_ = utils.get_code_frequency(self.test_set_codes_generator_v1)
        self.assertEqual(result_[self.expected_key_1_v1], self.expected_value_1_v1)
        self.assertEqual(result_[self.expected_key_2_v1], self.expected_value_2_v1)


    def test_it_get_set_codes_frequency(self):
        result_ = utils.get_code_frequency(self.test_set_codes_generator_v1)
        self.assertEqual(result_[self.expected_key_1_v1], self.expected_value_1_v1)
        self.assertEqual(result_[self.expected_key_2_v1], self.expected_value_2_v1)
