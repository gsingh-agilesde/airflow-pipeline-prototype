import pandas as pd
import unittest
from unittest.mock import patch

from cnn_attention import CPT_CODE_SET, ICD10_CODE_SET, MODEL_DIR, DATA_DIR
from cnn_attention.evaluation import test_model_performance
from cnn_attention.data_model.data_constants import OutputColumns
from cnn_attention.evaluation.metric_set import MetricSetCnnAttention, AveragingTechniques, MetricNames


class TestTestModelPerformance(unittest.TestCase):

    def setUp(self):
        self.code_set_1_negative = "HCC"
        self.code_set_2_negative = "RxNorm"
        self.code_set_3_negative = "BlahBlue"

        self.specialty = "sample_2"
        self.code_set = CPT_CODE_SET
        self.test_score_file_path = MODEL_DIR + "conv_attn_{}_{}_Jan_00_00_00/test_scores.pickle".format(self.specialty,
                                                                                                         self.code_set)
        self.ground_truth_labels_file_path = DATA_DIR + "{}_{}_test.csv".format(self.specialty, self.code_set)
        self.set_codes_file_path = DATA_DIR + "{}_codes_in_{}_specialty.pickle".format(self.code_set, self.specialty)

        self.note_id_1, self.note_id_2, self.note_id_3 = "note_id_1", "note_id_2", "note_id_3"

        self.code_1, self.code_2, self.code_3, self.code_4 = "99214", "76775", "Q2035", "90471"
        self.code_5, self.code_6, self.code_7, self.code_8 = "99397", "99245", "93228", "93880"
        self.code_9, self.code_10, self.code_11, self.code_12 = "90649", "20550", "83014", "90710"
        self.code_13, self.code_14, self.code_15, self.code_16 = "90747", "20605", "93295", "69210"
        self.code_17, self.code_18, self.code_19, self.code_20 = "11200", "94640", "90715", "99387"

        self.score_1_1, self.score_2_1, self.score_3_1, self.score_4_1 = 0.95, 0.921, 0.834, 0.77
        self.score_5_1, self.score_6_1, self.score_7_1, self.score_8_1 = 0.49, 0.439, 0.43, 0.374
        self.score_9_1, self.score_10_1, self.score_11_1, self.score_12_1 = 0.37, 0.1, 0.0834, 0.077
        self.score_13_1, self.score_14_1, self.score_15_1, self.score_16_1 = 0.0419, 0.0319, 0.00219, 0.00019
        self.score_17_1, self.score_18_1, self.score_19_1, self.score_20_1 = 0.0000025, 0.0000023, 0.000001, 0.0000001

        self.score_1_2, self.score_2_2, self.score_3_2, self.score_4_2 = 0.95, 0.021, 0.134, 0.77
        self.score_5_2, self.score_6_2, self.score_7_2, self.score_8_2 = 0.23, 0.31, 0.11, 0.0374
        self.score_9_2, self.score_10_2, self.score_11_2, self.score_12_2 = 0.37, 0.1, 0.08, 0.077
        self.score_13_2, self.score_14_2, self.score_15_2, self.score_16_2 = 0.00019, 0.92, 0.73, 0.00019
        self.score_17_2, self.score_18_2, self.score_19_2, self.score_20_2 = 0.002, 0.91, 0.0002, 0.0081247

        self.res_note_1 = {self.code_1: self.score_1_1, self.code_2: self.score_2_1,
                           self.code_3: self.score_3_1, self.code_4: self.score_4_1,
                           self.code_5: self.score_5_1, self.code_6: self.score_6_1,
                           self.code_7: self.score_7_1, self.code_8: self.score_8_1,
                           self.code_9: self.score_9_1, self.code_10: self.score_10_1,
                           self.code_11: self.score_11_1, self.code_12: self.score_12_1,
                           self.code_13: self.score_13_1, self.code_14: self.score_14_1,
                           self.code_15: self.score_15_1, self.code_16: self.score_16_1,
                           self.code_17: self.score_17_1, self.code_18: self.score_18_1,
                           self.code_19: self.score_19_1, self.code_20: self.score_20_1
                           }
        self.res_note_2 = {self.code_1: self.score_1_2, self.code_2: self.score_2_2,
                           self.code_3: self.score_3_2, self.code_4: self.score_4_2,
                           self.code_5: self.score_5_2, self.code_6: self.score_6_2,
                           self.code_7: self.score_7_2, self.code_8: self.score_8_2,
                           self.code_9: self.score_9_2, self.code_10: self.score_10_2,
                           self.code_11: self.score_11_2, self.code_12: self.score_12_2,
                           self.code_13: self.score_13_2, self.code_14: self.score_14_2,
                           self.code_15: self.score_15_2, self.code_16: self.score_16_2,
                           self.code_17: self.score_17_2, self.code_18: self.score_18_2,
                           self.code_19: self.score_19_2, self.code_20: self.score_20_2
                           }
        self.res_note_3 = {self.code_1: 1, self.code_2: 0.99,
                           self.code_3: 0.98, self.code_4: 0.97,
                           self.code_5: 0.96, self.code_6: 0.95,
                           self.code_7: 0.94, self.code_8: 0.93,
                           self.code_9: 0.92, self.code_10: 0.91,
                           self.code_11: 0.9, self.code_12: 0.89,
                           self.code_13: 0.88, self.code_14: 0.87,
                           self.code_15: 0.86, self.code_16: 0.85,
                           self.code_17: 0.84, self.code_18: 0.83,
                           self.code_19: 0.82, self.code_20: 0.81
                           }

        self.result = {
            self.note_id_1: dict(sorted(self.res_note_1.items(), key=lambda kv: kv[1], reverse=True)),
            self.note_id_2: dict(sorted(self.res_note_2.items(), key=lambda kv: kv[1], reverse=True)),
            self.note_id_3: dict(sorted(self.res_note_3.items(), key=lambda kv: kv[1], reverse=True))
        }

        self.set_codes = {self.code_1, self.code_2, self.code_3, self.code_4, self.code_5, self.code_6, self.code_7,
                          self.code_8, self.code_9, self.code_10, self.code_11, self.code_12, self.code_13,
                          self.code_14, self.code_15, self.code_16, self.code_17, self.code_18, self.code_19,
                          self.code_20
                          }

        self.expected_result_accuracy = {
            AveragingTechniques.MICRO.value: 0.424242,
            AveragingTechniques.MACRO.value: 0.491666
        }
        self.expected_result_precision_recall_f1 = {
            AveragingTechniques.MICRO.value: (0.482758, 0.777777, 0.595744),
            AveragingTechniques.MACRO.value: (0.55, 0.575, 0.539999)
        }
        self.expected_result_precision_recall_f1_at_n = {
            6: {
                AveragingTechniques.MICRO.value: (0.333333, 0.333333, 0.333333),
                AveragingTechniques.MACRO.value: (0.15, 0.225, 0.164999)
            },
            8: {
                AveragingTechniques.MICRO.value: (0.375, 0.5, 0.428571),
                AveragingTechniques.MACRO.value: (0.216666, 0.325, 0.24)
            },
            15: {
                AveragingTechniques.MICRO.value: (0.288888, 0.722222, 0.412698),
                AveragingTechniques.MACRO.value: (0.216666, 0.5, 0.295)
            },
            20: {
                AveragingTechniques.MICRO.value: (0.3, 1, 0.461538),
                AveragingTechniques.MACRO.value: (0.299999, 0.7, 0.41)
            },
            30: {
                AveragingTechniques.MICRO.value: (0.3, 1, 0.461538),
                AveragingTechniques.MACRO.value: (0.299999, 0.7, 0.41)
            },
            50: {
                AveragingTechniques.MICRO.value: (0.3, 1, 0.461538),
                AveragingTechniques.MACRO.value: (0.299999, 0.7, 0.41)
            },
            100: {
                AveragingTechniques.MICRO.value: (0.3, 1, 0.461538),
                AveragingTechniques.MACRO.value: (0.299999, 0.7, 0.41)
            }
        }
        self.expected_result_auroc = {
            AveragingTechniques.MICRO.value: 0.726851,
            AveragingTechniques.MACRO.value: 0.767857
        }
        self.expected_result = {
            MetricNames.ACCURACY: self.expected_result_accuracy,
            MetricNames.PRECISION_RECALL_F1: self.expected_result_precision_recall_f1,
            MetricNames.PRECISION_RECALL_F1_AT_N: self.expected_result_precision_recall_f1_at_n,
            MetricNames.AUROC: self.expected_result_auroc
        }

        label_column = None
        if self.code_set == CPT_CODE_SET:
            label_column = OutputColumns.CPT_LABELS
        elif self.code_set == ICD10_CODE_SET:
            label_column = OutputColumns.CPT_LABELS
        else:
            print("(ERROR in __init__): Please check selected code set.")
        self.ground_truth_dict = {OutputColumns.NOTE_ID: [self.note_id_1, self.note_id_2, self.note_id_3],
                                  OutputColumns.TEXT: ["Blah blah blah", "Blue blue blue", "Yada yada yada"],
                                  label_column: ["99214;76775;93228;20605;99387", "99214;20605;69210",
                                                 "99397;90715;90710;93228;20550;90649;90471;99387;11200;93880"]}
        self.test_df = pd.DataFrame(self.ground_truth_dict)


    def test_test_negative(self):
        self.assertRaises(ValueError, test_model_performance.test, "", "", "", self.code_set_1_negative)
        self.assertRaises(ValueError, test_model_performance.test, "", "", "", self.code_set_2_negative)
        self.assertRaises(ValueError, test_model_performance.test, "", "", "", self.code_set_3_negative)


    def __side_effect_from_pickle(self, file_path):
        if file_path == self.test_score_file_path:
            return self.result
        elif file_path == self.set_codes_file_path:
            return self.set_codes


    def __side_effect_pd_read_csv(self, ground_truth_labels_file_path):
        if ground_truth_labels_file_path == self.ground_truth_labels_file_path:
            return self.test_df


    def test_test_positive(self):
        with patch.object(test_model_performance, "from_pickle", side_effect=self.__side_effect_from_pickle):
            with patch.object(test_model_performance.pd, "read_csv", side_effect=self.__side_effect_pd_read_csv):
                with patch.object(test_model_performance.MetricSetCnnAttention, "compute") as mock_compute:
                    mock_compute.return_value = self.expected_result
                    result = test_model_performance.test(self.test_score_file_path,
                                                         self.ground_truth_labels_file_path,
                                                         self.specialty,
                                                         self.code_set)
                    self.assertTrue(MetricSetCnnAttention.is_almost_equal_metric_set_result(result,
                                                                                            self.expected_result))


    # Integration test: putting it together.
    def test_test_positive_it(self):
        result = test_model_performance.test(self.test_score_file_path,
                                             self.ground_truth_labels_file_path,
                                             self.specialty,
                                             self.code_set)
        self.assertTrue(MetricSetCnnAttention.is_almost_equal_metric_set_result(result, self.expected_result))
