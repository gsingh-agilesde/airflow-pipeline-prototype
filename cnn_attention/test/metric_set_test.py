import unittest
import numpy as np

from cnn_attention.evaluation.metric_set import MetricSetCnnAttention, AveragingTechniques, MetricNames


class MetricSetTest(unittest.TestCase):

    def setUp(self):
        self.at_n_values = (1, 4)
        self.metric_set = MetricSetCnnAttention(at_n_values=self.at_n_values)
        self.y_true = np.array([[0, 0, 1, 1, 1], [1, 1, 0, 0, 0]])
        self.y_pred = np.array([[0.2, 0.6, 0.2, 0.01, 0.78], [0.2, 0.8, 0.9, 0.1, 0.9]])

        # For accuracy.
        self.expected_result_micro_accuracy = 0.25
        self.expected_result_macro_accuracy = 0.2

        # For precision_recall_f1
        self.expected_result_micro_precision = 0.4
        self.expected_result_macro_precision = 0.2
        self.expected_result_micro_recall = 0.4
        self.expected_result_macro_recall = 0.4
        self.expected_result_micro_f1 = 0.4
        self.expected_result_macro_f1 = 0.2666666

        # For precision_recall_f1_at_n
        self.expected_result_micro_1_precision = 0.5
        self.expected_result_macro_1_precision = 0.1
        self.expected_result_micro_1_recall = 0.2
        self.expected_result_macro_1_recall = 0.2
        self.expected_result_micro_1_f1 = 0.285714
        self.expected_result_macro_1_f1 = 0.1333333

        self.expected_result_micro_4_precision = 0.5
        self.expected_result_macro_4_precision = 0.4
        self.expected_result_micro_4_recall = 0.8
        self.expected_result_macro_4_recall = 0.8
        self.expected_result_micro_4_f1 = 0.615384
        self.expected_result_macro_4_f1 = 0.5333333

        # For auroc
        self.expected_result_micro_auroc = 0.36
        self.expected_result_macro_auroc = 0.3

        self.expected_result_accuracy = {
            AveragingTechniques.MICRO.value: self.expected_result_micro_accuracy,
            AveragingTechniques.MACRO.value: self.expected_result_macro_accuracy
        }
        self.expected_result_precision_recall_f1 = {
            AveragingTechniques.MICRO.value: (
                self.expected_result_micro_precision,
                self.expected_result_micro_recall,
                self.expected_result_micro_f1
            ),
            AveragingTechniques.MACRO.value: (
                self.expected_result_macro_precision,
                self.expected_result_macro_recall,
                self.expected_result_macro_f1
            )
        }
        self.expected_result_precision_recall_f1_at_n = {
            1: {
                AveragingTechniques.MICRO.value: (
                    self.expected_result_micro_1_precision,
                    self.expected_result_micro_1_recall,
                    self.expected_result_micro_1_f1
                ),
                AveragingTechniques.MACRO.value: (
                    self.expected_result_macro_1_precision,
                    self.expected_result_macro_1_recall,
                    self.expected_result_macro_1_f1
                )
            },
            4: {
                AveragingTechniques.MICRO.value: (
                    self.expected_result_micro_4_precision,
                    self.expected_result_micro_4_recall,
                    self.expected_result_micro_4_f1
                ),
                AveragingTechniques.MACRO.value: (
                    self.expected_result_macro_4_precision,
                    self.expected_result_macro_4_recall,
                    self.expected_result_macro_4_f1
                )
            }
        }
        self.expected_result_auroc = {
            AveragingTechniques.MICRO.value: self.expected_result_micro_auroc,
            AveragingTechniques.MACRO.value: self.expected_result_macro_auroc
        }
        self.expected_result_ = {
            MetricNames.ACCURACY: self.expected_result_accuracy,
            MetricNames.PRECISION_RECALL_F1: self.expected_result_precision_recall_f1,
            MetricNames.PRECISION_RECALL_F1_AT_N: self.expected_result_precision_recall_f1_at_n,
            MetricNames.AUROC: self.expected_result_auroc
        }


    def test_accuracy(self):
        result_ = self.metric_set.accuracy(y_true=self.y_true, y_pred=self.y_pred)
        self.assertTrue(MetricSetCnnAttention.is_almost_equal_accuracy(result_, self.expected_result_accuracy))


    def test_precision_recall_f1(self):
        result_ = self.metric_set.precision_recall_f1(y_true=self.y_true, y_pred=self.y_pred)
        self.assertTrue(
            MetricSetCnnAttention.is_almost_equal_precision_recall_f1(result_,
                                                                      self.expected_result_precision_recall_f1
                                                                      )
        )


    def test_precision_recall_f1_at_n(self):
        result_ = self.metric_set.precision_recall_f1_at_n(y_true=self.y_true, y_pred=self.y_pred)
        self.assertTrue(
            MetricSetCnnAttention.is_almost_equal_precision_recall_f1_at_n(result_,
                                                                           self.expected_result_precision_recall_f1_at_n
                                                                           )
        )


    def test_auroc(self):
        result_ = self.metric_set.auroc(y_true=self.y_true, y_pred=self.y_pred)
        self.assertTrue(MetricSetCnnAttention.is_almost_equal_auroc(result_, self.expected_result_auroc))


    def test_compute(self):
        result_ = self.metric_set.compute(y_true=self.y_true, y_pred=self.y_pred)
        self.assertTrue(MetricSetCnnAttention.is_almost_equal_metric_set_result(result_, self.expected_result_))
