import unittest

from tqdm import tqdm

from cnn_attention import CPT_CODE_SET
from cnn_attention.data_model.data_constants import OutputColumns
from cnn_attention.data_processing.data_loader import DataLoader


class TestDataLoader(unittest.TestCase):

    def setUp(self):

        self.batch_size_train_validate = 8
        self.last_batch_size_train_validate = 2
        self.batch_size_test = 18
        self.last_batch_size_test = 18
        self.expected_batch_count_train_validate = 3
        self.expect_batch_count_test = 1
        self.label_col = OutputColumns.CPT_LABELS
        self.data_loader = DataLoader(specialty="sample", code_set=CPT_CODE_SET, rest_api=False)


    def test_data_loader(self):

        train_data, validate_data, test_data = self.data_loader.load_datasets()
        train_batches_gen, validate_batches_gen = self.data_loader.get_train_validate_batch_generators(
            self.batch_size_train_validate,
            train_data,
            validate_data)
        test_batches_gen = self.data_loader.get_test_batch_generator(test_data,
                                                                     OutputColumns.NOTE_ID,
                                                                     OutputColumns.TEXT)

        self._test_batch_gen(train_batches_gen, self.batch_size_train_validate, self.last_batch_size_train_validate,
                             self.expected_batch_count_train_validate)
        self._test_batch_gen(validate_batches_gen, self.batch_size_train_validate, self.last_batch_size_train_validate,
                             self.expected_batch_count_train_validate)
        self._test_batch_gen(test_batches_gen, self.batch_size_test, self.last_batch_size_test,
                             self.expect_batch_count_test)


    def _test_batch_gen(self, batch_gen, batch_size, last_batch_size, expected_batch_count):
        batch_count = 0
        prev_data_size = 0
        for note_ids, data, target in tqdm(batch_gen):
            batch_count += 1
            if batch_count < expected_batch_count:
                self.assertEqual(note_ids.shape[0], batch_size)
                self.assertEqual(data.shape[0], batch_size)
                # to test the sorting within the batch sizes
                self.assertGreater(data.shape[1], prev_data_size)
                prev_data_size = data.shape[1]
                if target is not None:
                    self.assertEqual(target.shape[0], batch_size)
            else:
                self.assertEqual(note_ids.shape[0], last_batch_size)
                self.assertEqual(data.shape[0], last_batch_size)
                if target is not None:
                    self.assertEqual(target.shape[0], last_batch_size)
                # to test the sorting within the batch sizes
                self.assertGreater(data.shape[1], prev_data_size)
                prev_data_size = data.shape[1]

        self.assertEqual(batch_count, expected_batch_count)


    def test_data_loader_incorrect_code_set(self):
        self.assertRaises(ValueError, DataLoader, specialty="sample", code_set="xyz", rest_api=False)
        self.assertRaises(ValueError, DataLoader, specialty="sample", code_set="xyz", rest_api=True)


    def test_get_data_loader_from_list_of_dict(self):
        batch_size = 2
        last_batch_size = 2
        batch_count = 1
        data = [{"id": "123", "content": "abc"}, {"id": "321", "content": "asd"}]
        data_gen = self.data_loader.get_test_batch_generator_from_list_dict(data, "id", "content")
        self._test_batch_gen(data_gen, batch_size, last_batch_size, batch_count)
