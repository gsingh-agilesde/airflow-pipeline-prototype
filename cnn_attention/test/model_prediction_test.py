import unittest

from cnn_attention import MODEL_DIR, CPT_CODE_SET, CNN
from cnn_attention.training_and_prediction.model_prediction import ModelPrediction


# class TestModelPrediction(unittest.TestCase):
#
#     def setUp(self):
#         model_dir__ = MODEL_DIR + "conv_attn_sample_2_CPT_Jan_00_00_00"
#         specialty = "sample"
#         code_set = CPT_CODE_SET
#         self.model_prediction = ModelPrediction(model_dir__)
#         processed_note = "doctor x made the following diagnosis since yesterday constant tylenol hr ago nausea"
#         self.set_words = set(processed_note.split())
#         self.set_words.add("<unk>")
#         self.set_indices = set(range(len(processed_note.split())))
#         self.note_id = "d821-blah-b1e72"
#         self.ind_to_ind = {0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 9, 9: 10, 10: 12, 11: 13, 12: 14}
#         self.notes = [{"noteId": self.note_id, "content": processed_note}]
#         self.expected_set_codes = {"3117F", "93005", "93306", "93320", "93325", "93351", "99212", "99213"}
#
#
#     def test_get_code_to_attention(self):
#         note_id_to_code_score_, note_id_to_code_attn_ = self.model_prediction.get_prediction_for_notes(self.notes,
#                                                                                                        top_k_codes=8,
#                                                                                                        is_mention=False)
#         self.assertEqual(len(note_id_to_code_score_), 1)
#         self.assertEqual(len(note_id_to_code_score_[self.note_id]), 8)
#         self.assertEqual(len(note_id_to_code_score_[self.note_id]), len(note_id_to_code_attn_[self.note_id]))
#         self.assertEqual(self.expected_set_codes, set(note_id_to_code_score_[self.note_id].keys()))
#
#         for cpt_code in note_id_to_code_attn_[self.note_id]:
#             self.assertTrue(set(note_id_to_code_attn_[self.note_id][cpt_code].keys()).issubset(self.set_words))
#
#         note_id_to_code_score_, note_id_to_code_attn_ = self.model_prediction.get_prediction_for_notes(self.notes,
#                                                                                                        top_k_codes=8,
#                                                                                                        is_mention=True)
#         self.assertEqual(len(note_id_to_code_score_), 1)
#         self.assertEqual(len(note_id_to_code_score_[self.note_id]), 8)
#         self.assertEqual(len(note_id_to_code_score_[self.note_id]), len(note_id_to_code_attn_[self.note_id]))
#         self.assertEqual(self.expected_set_codes, set(note_id_to_code_score_[self.note_id].keys()))
#         for cpt_code in note_id_to_code_attn_[self.note_id]:
#             self.assertTrue(set(note_id_to_code_attn_[self.note_id][cpt_code].keys()).issubset(self.set_indices))
