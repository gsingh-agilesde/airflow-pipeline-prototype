import unittest
import numpy as np
from collections import defaultdict

from cnn_attention.data_processing.cross_validation import StratifiedKFold


class TestCrossValidation(unittest.TestCase):

    def setUp(self):
        self.n_splits_2 = 2
        self.n_splits_3 = 3
        self.n_splits_4 = 4
        self.n_split_to_skf = {self.n_splits_2: StratifiedKFold(n_splits=self.n_splits_2),
                               self.n_splits_3: StratifiedKFold(n_splits=self.n_splits_3),
                               self.n_splits_4: StratifiedKFold(n_splits=self.n_splits_4)}

        self.y = np.array([frozenset({"99201"}), frozenset({"20610", "99201"}), frozenset({"20610", "99201"}),
                           frozenset({"99214"}), frozenset({"99201"}), frozenset({"99201"}), frozenset({"99214"}),
                           frozenset({"99201"}), frozenset({"99214"}), frozenset({"99214"}), frozenset({"99214"}),
                           frozenset({"99214"}), frozenset({"99201"}), frozenset({"99214"})])

        self.unique_y_to_indices = defaultdict(list)
        for i, label in enumerate(self.y):
            self.unique_y_to_indices[label].append(i)


    def test_n_splits_less_than_2(self):
        self.assertRaises(ValueError, StratifiedKFold, 1)
        self.assertRaises(ValueError, StratifiedKFold, 0)
        self.assertRaises(ValueError, StratifiedKFold, -1)


    def test_meta_information(self):
        for n_splits in self.n_split_to_skf:
            self.check_n_splits(n_splits)


    def check_n_splits(self, n_splits):
        """
        :param n_splits: Number of folds in cross-validation
        :return:
        """
        z = 0
        for train_indices, validate_indices in self.n_split_to_skf[n_splits].split(self.y):
            # Check: No duplicates.
            self.assertEqual(len(set(train_indices)), len(train_indices))
            self.assertEqual(len(set(validate_indices)), len(validate_indices))
            # Check: No common values between train and validate.
            self.assertFalse(set(train_indices).intersection(set(validate_indices)))
            z += 1
        # Check: Number of train, validate datasets are equal to the number of folds or n_splits.
        self.assertEqual(z, n_splits)
