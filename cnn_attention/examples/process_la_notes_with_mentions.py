from utils import file_io_utils
from cnn_attention import DATA_DIR, MODEL_DIR
from cnn_attention.training_and_prediction.model_prediction import ModelPrediction
from cnn_attention.utils import process_note


cpt_model_dir__ = MODEL_DIR + "conv_attn_los_alamitos_CPT_Jan_29_00_08_06"
icd_model_dir__ = MODEL_DIR + "conv_attn_los_alamitos_ICD10_Jan_26_04_01_22"

model_prediction_cpt = ModelPrediction(model_dir=cpt_model_dir__)
model_prediction_icd = ModelPrediction(model_dir=icd_model_dir__)

# print(note_id_to_code_score_)
# print(note_id_to_code_attn_)


result_cpt = list()
result_icd = list()

import os

notes_folder_path = "/Users/gotitkumarsingh/Documents/Datasets/la_notes/"
notes = os.listdir(notes_folder_path)
if ".DS_Store" in notes:
    notes.remove(".DS_Store")


counter = 0
for note_id in notes:

    counter += 1

    with open(notes_folder_path + note_id, encoding="utf-8", errors='ignore') as f:
        text = f.read()
    note_processed, token_map_processed_original = process_note(text)
    note_processed_tokens = note_processed.split()

    note = [{"noteId": note_id, "content": note_processed}]
    cpt_code_score_, cpt_code_attn_ = model_prediction_cpt.get_prediction_for_notes(note, top_k_codes=8,
                                                                                    top_n_attentions=4)
    icd_code_score_, icd_code_attn_ = model_prediction_icd.get_prediction_for_notes(note, top_k_codes=8,
                                                                                    top_n_attentions=4)

    # for code, attn_dict in cpt_code_attn_.get(note_id).items():
    #
    #     intermediate = [note_id, code] + [(note_processed_tokens[k], v) for k, v in attn_dict.items()]
    #     extn = [None] * (81 - len(intermediate))
    #     result_cpt.append(intermediate + extn)

    # for code, attn_dict in icd_code_attn_.get(note_id).items():
    #     intermediate = [note_id, code] + [(note_processed_tokens[k], v) for k, v in attn_dict.items()]
    #     extn = [None] * (81-len(intermediate))
    #     result_icd.append(intermediate + extn)

    if counter == 100:
        print("Counter at: {}".format(counter))

    # ------------------------------------- #
    for code, attn_dict in cpt_code_attn_.get(note_id).items():
        intermediate = list()
        for k, v in attn_dict.items():
            k_original = token_map_processed_original.get(k)
            word_original = text.split()[k_original]
            word_score = v
            start = len(text[:k_original])
            end = start + len(text[k_original])
            result_cpt.append([note_id, code, word_original, word_score, start, end])

    for code, attn_dict in icd_code_attn_.get(note_id).items():
        intermediate = list()
        for k, v in attn_dict.items():
            k_original = token_map_processed_original.get(k)
            word_original = text.split()[k_original]
            word_score = v
            start = len(text[:k_original])
            end = start + len(text[k_original])
            result_icd.append([note_id, code, word_original, word_score, start, end])

import pandas as pd
# cols_mentions = ["NoteID", "Code"]+["Mention_{}".format(i) for i in range(1, 80)]
cols_mentions = ["NoteID", "Code", "Word", "Word_Score", "Start", "End"]

df = pd.DataFrame(result_cpt, columns=cols_mentions)
df.to_csv("/Users/gotitkumarsingh/Downloads/la_cpt_mentions_output.csv", index=None)

df = pd.DataFrame(result_icd, columns=cols_mentions)
df.to_csv("/Users/gotitkumarsingh/Downloads/la_icd_mentions_output.csv", index=None)