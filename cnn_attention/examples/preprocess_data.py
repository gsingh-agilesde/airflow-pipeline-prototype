import time

from utils.file_io_utils import to_pickle

from cnn_attention import DATA_DIR, ICD10_CODE_SET, CPT_CODE_SET

from cnn_attention.data_model.data_constants import SpecialtyNames
from cnn_attention.data_processing.join_notes_text_codes_feedback import join_notes_and_labels
from cnn_attention.data_processing.train_validate_test_split import create_train_validate_test_data_splits
from cnn_attention.data_processing.word_embeddings import generate_wv_embeddings
from cnn_attention.data_processing.all_specialty_make_files import make_files_all_specialty


def preprocess_data(specialty, code_sets=(ICD10_CODE_SET, CPT_CODE_SET)):
    """
    Runs the entire pre processing pipeline given the specialty and code set for the 300K clinical notes data set.
    This workflow is described in detail in the cnn_attention/data_processing/preprocessing_readme.txt file.
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_sets: The selected code sets. Presently, we only support "CPT" and "ICD10" as defined in __init__ for
                      cnn_attention project.
    :return: None
    """
    # 1. Joins the clinical notes and the feedback in terms of ICD10 and CPT labels.
    #    Saves results to disk.
    print("Join notes and label feedback for {} specialty...".format(specialty))
    t0 = time.time()
    join_notes_and_labels(specialty)
    print("...done in {} sec.".format(time.time() - t0))
    # 2. Creates the train, validate and test data sets for given specialty and code set.
    #    Saves results to disk.
    print("Creating train, validate and test data sets for {} specialty...".format(specialty))
    t0 = time.time()
    for code_set in code_sets:
        create_train_validate_test_data_splits(specialty, code_set)
    print("...done in {} sec.".format(time.time() - t0))

    # 3. Computes the word embeddings using the clinical notes from entire data set for a given specialty and code set.
    #   Saves results to disk.
    print("Computing word embeddings for {} specialty...".format(specialty))
    t0 = time.time()
    for code_set in code_sets:
        vocab_ = generate_wv_embeddings(specialty, code_set, 100, 0, 4, 5)
        to_pickle(DATA_DIR + "{}_{}_vocab.pickle".format(specialty, code_set), vocab_)
    print("...done in {} sec.".format(time.time() - t0))


if __name__ == '__main__':
    # Entire workflow for preprocessing data.
    # For every specialty except all, perform the following.
    for specialty in (SpecialtyNames.CARDIO, SpecialtyNames.ORTHO, SpecialtyNames.PCP):
        preprocess_data(specialty)

    # Once all specialties are processed, compute the results for "all" specialty as follows:
    print("Creating files for {} specialty for both cpt and icd10 code sets...".format(SpecialtyNames.ALL))
    t0 = time.time()
    for code_set in (ICD10_CODE_SET, CPT_CODE_SET):
        make_files_all_specialty(code_set)
    print("...done in {} sec.".format(time.time() - t0))
