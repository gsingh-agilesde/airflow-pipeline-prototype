import pandas as pd

from utils.file_io_utils import file_names_from_directory, from_text, to_pickle

from cnn_attention import DATA_DIR, CPT_CODE_SET, ICD10_CODE_SET

from cnn_attention.data_model.data_constants import OutputColumns, SpecialtyNames
from cnn_attention.utils import process_note
from cnn_attention.data_processing.train_validate_test_split import create_train_validate_test_data_splits
from cnn_attention.data_processing.word_embeddings import generate_wv_embeddings


class Claim:
    PATIENT_NAME = " patient_name"
    DATE_OF_SERVICE_START = " date_of_service_start"
    KEY = "key"
    CPT = " procedure_code"
    CLAIM_ID = " claim_id"
    DIAGNOSIS_CODE_1 = " diagnosis_code_1"
    DIAGNOSIS_CODE_2 = " diagnosis_code_2"
    DIAGNOSIS_CODE_3 = " diagnosis_code_3"
    DIAGNOSIS_CODE_4 = " diagnosis_code_4"
    ICD = " diagnosis_codes"


class NoteFile:
    FILENAME = "filename"
    DATE_OF_SERVICE = "date of service"
    PATIENT_NAME = "patient name"
    KEY = "key"


def main(code_set=CPT_CODE_SET):
    base_dir = DATA_DIR + "los_alamitos/"
    claims_details_fp = base_dir + "claims_details.csv"
    notes_dir = base_dir + "notes/"

    if code_set == CPT_CODE_SET:
        final_label_column_name = OutputColumns.CPT_LABELS
    elif code_set == ICD10_CODE_SET:
        final_label_column_name = OutputColumns.ICD_LABELS

    print("Joining notes with coder feedback...")
    ##########################################################################################
    # CLAIMS Processing: Group CPT codes by PatientName_DateOfServiceStart key.
    ##########################################################################################
    # Claims dataframe.
    claims_df = pd.read_csv(claims_details_fp)

    # Convert service date start column to string.
    claims_df[Claim.DATE_OF_SERVICE_START] = claims_df[Claim.DATE_OF_SERVICE_START].apply(lambda x: str(x))
    # Convert Patient name column to lower case and no spaces.
    claims_df[Claim.PATIENT_NAME] = claims_df[Claim.PATIENT_NAME].apply(
        lambda x: "".join([name.lower() for name in x.split()]))
    # Create new "key" column with PatientName_DateOfServiceStart as id.
    claims_df[Claim.KEY] = claims_df[[Claim.PATIENT_NAME, Claim.DATE_OF_SERVICE_START]].apply(lambda x: "_".join(x),
                                                                                              axis=1)
    # Find number of claims per "KEY". In some cases we find that one KEY corresponds to more than one claim.
    # We need to fix this "KEY" by making a unique one per claim and re-inserting the updated keys back to claims_df.

    temp_res_df = claims_df.groupby(by=Claim.CLAIM_ID)[Claim.KEY].apply(set).to_frame().reset_index()
    mask = temp_res_df[Claim.KEY].apply(lambda x: len(x) > 1)
    claim_to_multiple_keys = temp_res_df[mask]
    claim_to_multiple_keys.reset_index(inplace=True, drop=True)

    # Get Data frame with unique Claim to KEY mapping.
    mask = claims_df[Claim.CLAIM_ID].apply(lambda x: x not in claim_to_multiple_keys[Claim.CLAIM_ID].values.tolist())
    claim_to_unique_key_df = claims_df[mask]

    key_to_claim_id_df = claim_to_unique_key_df.groupby(by=Claim.KEY)[Claim.CLAIM_ID].apply(
        set).to_frame().reset_index()
    mask = key_to_claim_id_df[Claim.CLAIM_ID].apply(lambda x: len(x) > 1)
    key_to_multiple_claim_id_df = key_to_claim_id_df[mask]
    key_to_multiple_claim_id_df.reset_index(inplace=True, drop=True)
    mask = claim_to_unique_key_df[Claim.KEY].apply(
        lambda x: x not in key_to_multiple_claim_id_df[Claim.KEY].values.tolist())

    # The following dataframe has a one-to-one relationship between claim id and note id ("key")
    unqiue_claim_to_unique_key_df = claim_to_unique_key_df[mask]

    if code_set == ICD10_CODE_SET:
        unqiue_claim_to_unique_key_df[Claim.DIAGNOSIS_CODE_1] = unqiue_claim_to_unique_key_df[
            Claim.DIAGNOSIS_CODE_1].astype(str)
        unqiue_claim_to_unique_key_df[Claim.DIAGNOSIS_CODE_2] = unqiue_claim_to_unique_key_df[
            Claim.DIAGNOSIS_CODE_2].astype(str)
        unqiue_claim_to_unique_key_df[Claim.DIAGNOSIS_CODE_3] = unqiue_claim_to_unique_key_df[
            Claim.DIAGNOSIS_CODE_3].astype(str)
        unqiue_claim_to_unique_key_df[Claim.DIAGNOSIS_CODE_4] = unqiue_claim_to_unique_key_df[
            Claim.DIAGNOSIS_CODE_4].astype(str)

        # Combine ICD10 codes from all columns ignoring all NaN values.
        unqiue_claim_to_unique_key_df[Claim.ICD] = unqiue_claim_to_unique_key_df[
            [Claim.DIAGNOSIS_CODE_1, Claim.DIAGNOSIS_CODE_2, Claim.DIAGNOSIS_CODE_3, Claim.DIAGNOSIS_CODE_4]].apply(
            lambda x: ";".join([v for v in x if v != "nan"]), axis=1)

        temp_label_column_name = Claim.ICD

    elif code_set == CPT_CODE_SET:
        temp_label_column_name = Claim.CPT

    res_df = unqiue_claim_to_unique_key_df.groupby(by=Claim.KEY)[temp_label_column_name].apply(
        list).to_frame().reset_index()
    res_df[temp_label_column_name] = res_df[temp_label_column_name].apply(lambda x: ";".join(list(set(x))))

    ##########################################################################################
    # NOTE NAMES Processing: Find records with unique PatientName_DateOfServiceStart key.
    #                        WHEN KEY IS DUPLICATED, DROP ALL OCCURRENCES OF THAT KEY.
    ##########################################################################################

    # Read clinical note file names.
    note_file_names = file_names_from_directory(notes_dir)
    note_file_names_df = pd.DataFrame(note_file_names)
    note_file_names_df.rename(columns={0: NoteFile.FILENAME}, inplace=True)

    # Extract patient name.
    note_file_names_df[NoteFile.PATIENT_NAME] = note_file_names_df[NoteFile.FILENAME].apply(
        lambda x: x.split("_")[-2].lower())
    # Extract date of service.
    note_file_names_df[NoteFile.DATE_OF_SERVICE] = note_file_names_df[NoteFile.FILENAME].apply(
        lambda x: x.split("_")[-1].split(".")[0][:8])
    # Create a new "key" column with requisite PatientName_DateOfServiceStart as id.
    note_file_names_df[NoteFile.KEY] = note_file_names_df[[NoteFile.PATIENT_NAME, NoteFile.DATE_OF_SERVICE]].apply(
        lambda x: "_".join(x), axis=1)

    new_key_df = note_file_names_df[NoteFile.KEY].drop_duplicates(keep=False).to_frame().reset_index(drop=True)
    mask = note_file_names_df[NoteFile.KEY].apply(
        lambda x: True if x in new_key_df[NoteFile.KEY].values.tolist() else False)
    notes_unique_df = note_file_names_df[mask]

    ##########################################################################################
    # JOIN CLAIMS DATAFRAME AND NOTES DATA FRAME.
    ##########################################################################################

    joined_df = pd.merge(notes_unique_df, res_df, on=NoteFile.KEY)

    joined_df[OutputColumns.TEXT] = joined_df[NoteFile.FILENAME].apply(lambda x: " ".join(from_text(notes_dir + x)))
    joined_df.rename(columns={NoteFile.KEY: OutputColumns.NOTE_ID, temp_label_column_name: final_label_column_name},
                     inplace=True)

    output_df = joined_df[[OutputColumns.NOTE_ID, OutputColumns.TEXT, final_label_column_name]]

    output_df[OutputColumns.TEXT] = output_df[OutputColumns.TEXT].apply(lambda x: process_note(x)[0])

    output_df.to_csv(DATA_DIR + SpecialtyNames.LOS_ALAMITOS + "_notes_with_" + code_set + "_feedback.csv",
                     index=False)
    print("...done.")
    print("Creating train, validate and test datasets...")
    create_train_validate_test_data_splits(SpecialtyNames.LOS_ALAMITOS, code_set)
    print("...done.")
    print("Creating word embeddings...")
    vocab_la = generate_wv_embeddings(SpecialtyNames.LOS_ALAMITOS, code_set, 100, 0, 4, 5)
    to_pickle(DATA_DIR + "{}_{}_vocab.pickle".format(SpecialtyNames.LOS_ALAMITOS, code_set), vocab_la)
    print("...done.")


if __name__ == '__main__':
    main(CPT_CODE_SET)
    main(ICD10_CODE_SET)
