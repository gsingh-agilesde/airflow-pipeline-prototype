import numpy as np
import itertools
from matplotlib import pyplot as py

from utils.file_io_utils import to_pickle

from cnn_attention import CNN, DATA_DIR

from cnn_attention.data_processing.data_loader import DataLoader
from cnn_attention.data_model.data_constants import OutputColumns
from cnn_attention.training_and_prediction.model_training import ModelTraining
from cnn_attention.evaluation.test_model_performance import test
from cnn_attention.data_processing.analysis.code_metric_frequency_analysis import compute_code_metric_frequency, \
    __TEST_FREQ, __F1, __FREQUENCY

__THRESH_TEST_FREQ = 5
__THRESH_FREQ = 10


def save_loss_fig(loss_each_epoch_train_, loss_each_epoch_val_, model_directory):
    """
    Save the loss vs epoch figure for both training and validation sets.
    :param loss_each_epoch_train_: loss on training set vs epoch
    :param loss_each_epoch_val_: loss on validation set vs epoch
    :param model_directory: directory where the current model is stored.
    :return:
    """
    py.figure()
    py.plot(range(0, len(loss_each_epoch_train_)), loss_each_epoch_train_)
    py.plot(range(0, len(loss_each_epoch_val_)), loss_each_epoch_val_)
    py.savefig(model_directory + "/loss_vs_time.png")


def train_test_evaluate(specialties, code_sets):
    """
    Script to run the full model training and testing loop for various hyperparameters in params_tuple variable and
    additionally for all input specialties and code sets.
    :param specialties: List of specialties where a specialty is as defined under
                        cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_sets: List of code sets where presently a code set is either "CPT" or "ICD10" defined in __init__ for
                      cnn_attention project.
    :return:
    """
    lrs = {0.25 * 1e-5, 0.25 * 1e-4, 0.25 * 1e-3}
    bs = {16, 32, 64}
    es = {True, False}
    ns = {20, 50, 120, 1000}
    text_sort_methods = {True, False}
    kernel_sizes = {4, 6, 8}
    conv_out_sizes = {20, 50, 80}
    dropout_probs = {0.5}
    params_tuple = list(
        itertools.product(lrs, es, bs, ns, text_sort_methods, kernel_sizes, conv_out_sizes, dropout_probs))

    params_to_loss_dict = dict()

    total_runs = len(specialties) * len(code_sets) * len(params_tuple)
    z = 1

    best_model_training = None
    best_learning_rate_ = None
    best_early_stopping = None
    best_batch_size_ = None
    best_n_epochs = None
    best_text_sort = None
    best_kernel_size = None
    best_conv_out_size = None
    best_dropout_prob = None
    best_average_f1 = 0.0
    best_model_directory = None

    for specialty in specialties:
        for code_set in code_sets:

            # Data loader
            data_loader = DataLoader(specialty, code_set, rest_api=False)
            train_data, validate_data, test_data = data_loader.load_datasets()
            test_batches_gen = data_loader.get_test_batch_generator(test_data, OutputColumns.NOTE_ID,
                                                                    OutputColumns.TEXT, y_field=data_loader.label_col,
                                                                    is_train=True)
            for learning_rate_, early_stopping, batch_size_, n_epochs, \
                    text_sort, kernel_size, conv_out_size, dropout_prob in params_tuple:
                print(
                    "\n{}/{} for sp={}, cs={}, n={}, lr={}, es={}, bsz={}, sort={}, ksz={}, "
                    "n_out_ch={}, dropout={} \n".format(z,
                                                        total_runs,
                                                        specialty,
                                                        code_set,
                                                        n_epochs,
                                                        learning_rate_,
                                                        early_stopping,
                                                        batch_size_,
                                                        text_sort,
                                                        kernel_size,
                                                        conv_out_size,
                                                        dropout_prob))

                train_batches_gen, validate_batches_gen = data_loader.get_train_validate_batch_generators(batch_size_,
                                                                                                          train_data,
                                                                                                          validate_data,
                                                                                                          sort=text_sort
                                                                                                          )

                model_training = ModelTraining(None, specialty, code_set, kernel_size, conv_out_size, CNN, "0",
                                               epochs=n_epochs, batch_size=batch_size_, weight_decay=0,
                                               learning_rate=learning_rate_, early_stopping=early_stopping, epsilon=0,
                                               patience=5, dropout=dropout_prob,
                                               data_iterator=data_loader.data_iterator)
                loss_each_epoch_train_, loss_each_epoch_val_, model_directory, val_note_id_to_score = model_training \
                    .train_model(train_batches_gen, validate_batches_gen)

                # Compute precision/recall/f1 results for val_note_id_to_score and save the best one along with the
                # corresponding model and hyperparameter choices.
                result_df = compute_code_metric_frequency(specialty, code_set, val_note_id_to_score, None,
                                                          is_path_test_scores=False, save_result=False,
                                                          is_validate=True)

                average_f1 = np.mean(
                    result_df[(result_df[__TEST_FREQ] > __THRESH_TEST_FREQ) & (result_df[__FREQUENCY] > __THRESH_FREQ)][
                        __F1])

                # If current model beats the existing best model, then update the best model to be the current model.
                if average_f1 >= best_average_f1:
                    best_average_f1 = average_f1
                    best_model_training = model_training
                    best_learning_rate_ = learning_rate_
                    best_early_stopping = early_stopping
                    best_batch_size_ = batch_size_
                    best_n_epochs = n_epochs
                    best_text_sort = text_sort
                    best_kernel_size = kernel_size
                    best_conv_out_size = conv_out_size
                    best_dropout_prob = dropout_prob
                    best_model_directory = model_directory

                # Save results to model directory.
                result_df.to_csv(
                    model_directory + "/code_labels_f1_thresholds_{}_{}.csv".format(__THRESH_TEST_FREQ, __THRESH_FREQ),
                    index=False)
                save_loss_fig(loss_each_epoch_train_, loss_each_epoch_val_, model_directory)

                z += 1

            loss_test_, test_note_id_to_score = best_model_training.test_model(test_batches_gen,
                                                                               save_attention=True)
            print("loss for test data : {}".format(str(loss_test_)))
            params_to_loss_dict[
                (specialty, code_set, best_learning_rate_, best_early_stopping, best_batch_size_,
                 best_n_epochs, best_text_sort, best_kernel_size, best_conv_out_size, best_dropout_prob)] = loss_test_

            test_scores_file_path = best_model_directory + "/test_scores.pickle"
            # Read the ground-truth labels for cardio specialty.
            # This should be one of the output files for "test" split from
            # cnn_attention/data_processing/train_validate_test_split.py
            ground_truth_labels_file_path = DATA_DIR + "{}_{}_test.csv".format(specialty, code_set)
            # Test model performance.
            result = test(test_scores_file_path, ground_truth_labels_file_path, specialty, code_set)
            to_pickle(best_model_directory + "/evaluation_scores.pickle", result)

            result_df = compute_code_metric_frequency(specialty, code_set, test_note_id_to_score, None,
                                                      is_path_test_scores=False, save_result=False,
                                                      is_validate=False)
            result_df.to_csv(
                best_model_directory + "/code_labels_f1_thresholds_{}_{}_test_dataset.csv".format(__THRESH_TEST_FREQ,
                                                                                                  __THRESH_FREQ),
                index=False)

            print(
                "For specialty={}, code_set={}, the best params = {}".format(specialty, code_set, params_to_loss_dict))
            to_pickle(best_model_directory + "/params_to_loss_dict.pickle", params_to_loss_dict)
    print("Completed for all specialties and code sets")


if __name__ == '__main__':
    from cnn_attention.data_model.data_constants import SpecialtyNames
    from cnn_attention import CPT_CODE_SET

    org_id = "13532"
    train_test_evaluate([SpecialtyNames.ORTHO_NO_EM + "_" + org_id], [CPT_CODE_SET])
