from cnn_attention import CNN, DATA_DIR, MODEL_DIR
from cnn_attention.training_and_prediction.model_training import ModelTraining
from matplotlib import pyplot as py
from cnn_attention.evaluation.test_model_performance import test
from utils.file_io_utils import to_pickle
import itertools


def train_test_evaluate(specialties, code_sets):
    """
    Script to run the full model training and testing loop for various hyperparameters in params_tuple variable and
    additionally for all input specialties and code sets.
    :param specialties: List of specialties where a specialty is as defined under
                        cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_sets: List of code sets where presently a code set is either "CPT" or "ICD10" defined in __init__ for
                      cnn_attention project.
    :return:
    """
    lrs = {0.25 * 1e-3}
    bs = {32, 64}
    es = {True}
    ns = {20, 50, 120, 1000}
    text_sort_methods = {True, False}
    params_tuple = list(itertools.product(lrs, es, bs, ns, text_sort_methods))

    params_to_loss_dict = dict()

    total_runs = len(specialties) * len(code_sets) * len(params_tuple)
    z = 1

    for specialty in specialties:
        for code_set in code_sets:
            for learning_rate_, early_stopping, batch_size_, n_epochs, text_sort in params_tuple:
                print(
                    "\n{}/{} for sp={}, cs={}, n={}, lr={}, es={}, bsz={}, sort={} \n".format(z, total_runs, specialty,
                                                                                              code_set, n_epochs,
                                                                                              learning_rate_,
                                                                                              early_stopping,
                                                                                              batch_size_, text_sort))

                model_training = ModelTraining(None, specialty, code_set, 4, 50, CNN, "0", epochs=n_epochs,
                                               batch_size=batch_size_, weight_decay=0, learning_rate=learning_rate_,
                                               early_stopping=early_stopping, epsilon=0, patience=5, sort=text_sort)
                loss_each_epoch_train_, loss_each_epoch_val_, model_directory = model_training.train_model()
                loss_test_ = model_training.test_model(save_attention=True)
                print("loss for test data : {}".format(str(loss_test_)))
                params_to_loss_dict[(specialty, code_set, learning_rate_, early_stopping, batch_size_, n_epochs,
                                     text_sort)] = loss_test_

                py.figure()
                py.plot(range(0, len(loss_each_epoch_train_)), loss_each_epoch_train_)
                py.plot(range(0, len(loss_each_epoch_val_)), loss_each_epoch_val_)
                py.savefig(model_directory + "/loss_vs_time.png")
                test_scores_file_path = model_directory + "/test_scores.pickle"
                # Read the ground-truth labels for cardio specialty.
                # This should be one of the output files for "test" split from
                # cnn_attention/data_processing/train_validate_test_split.py
                ground_truth_labels_file_path = DATA_DIR + "{}_{}_test.csv".format(specialty, code_set)
                # Test model performance.
                result = test(test_scores_file_path, ground_truth_labels_file_path, specialty, code_set)
                to_pickle(model_directory + "/evaluation_scores.pickle", result)

                z += 1

    print(params_to_loss_dict)
    to_pickle(MODEL_DIR + "/params_to_loss_dict.pickle", params_to_loss_dict)
    print("Completed for all specialties and code sets")
