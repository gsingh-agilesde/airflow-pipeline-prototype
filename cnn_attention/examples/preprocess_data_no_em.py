import time
import pandas as pd

from utils.file_io_utils import to_pickle

from data_processing_global import set_em_codes

from cnn_attention import DATA_DIR, CPT_CODE_SET

from cnn_attention.data_model.data_constants import SpecialtyNames
from cnn_attention.data_model.data_constants import OutputColumns
from cnn_attention.data_processing.train_validate_test_split import create_train_validate_test_data_splits
from cnn_attention.data_processing.word_embeddings import generate_wv_embeddings
from cnn_attention.data_processing.all_specialty_make_files import make_files_all_specialty


def preprocess_data(specialty, new_specialty):
    """
    Runs the entire pre processing pipeline given the specialty and code set for the 300K clinical notes data set.
    This workflow is described in detail in the cnn_attention/data_processing/preprocessing_readme.txt file.
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_sets: The selected code sets. Presently, we only support "CPT" and "ICD10" as defined in __init__ for
                      cnn_attention project.
    :return: None
    """
    # 1.a Read the joined notes and codes feedback file
    print("Read the joined notes and codes feedback file...".format(specialty))
    t0 = time.time()
    notes_feedback_df = pd.read_csv(DATA_DIR + specialty + "_notes_with_" + CPT_CODE_SET + "_feedback.csv")

    # 1.b Remove the EM codes and then remove rows with contain empty list for labels.
    notes_feedback_df[OutputColumns.CPT_LABELS] = notes_feedback_df[OutputColumns.CPT_LABELS].apply(
        lambda x: [c for c in x.split(";") if c not in set_em_codes])
    mask = notes_feedback_df[OutputColumns.CPT_LABELS].apply(lambda x: len(x) != 0)
    notes_feedback_df = notes_feedback_df[mask]
    notes_feedback_df[OutputColumns.CPT_LABELS] = notes_feedback_df[OutputColumns.CPT_LABELS].apply(
        lambda x: ";".join(x))
    notes_feedback_df.to_csv(DATA_DIR + new_specialty + "_notes_with_" + CPT_CODE_SET + "_feedback.csv",
                             index=False)

    print("...done in {} sec.".format(time.time() - t0))
    # 2. Creates the train, validate and test data sets for given specialty and code set.
    #    Saves results to disk.
    print("Creating train, validate and test data sets for {} specialty...".format(new_specialty))
    t0 = time.time()
    create_train_validate_test_data_splits(new_specialty, CPT_CODE_SET)
    print("...done in {} sec.".format(time.time() - t0))

    # 3. Computes the word embeddings using the clinical notes from entire data set for a given specialty and code set.
    #   Saves results to disk.
    print("Computing word embeddings for {} specialty...".format(new_specialty))
    t0 = time.time()
    vocab_ = generate_wv_embeddings(new_specialty, CPT_CODE_SET, 100, 0, 4, 5)
    to_pickle(DATA_DIR + "{}_{}_vocab.pickle".format(new_specialty, CPT_CODE_SET), vocab_)
    print("...done in {} sec.".format(time.time() - t0))


if __name__ == '__main__':
    # Entire workflow for preprocessing data.
    # For every specialty except all, perform the following.
    specialties = (SpecialtyNames.CARDIO, SpecialtyNames.ORTHO, SpecialtyNames.PCP)
    new_specialties = (SpecialtyNames.CARDIO_NO_EM, SpecialtyNames.ORTHO_NO_EM, SpecialtyNames.PCP_NO_EM)

    for specialty, new_specialty in zip(specialties, new_specialties):
        preprocess_data(specialty, new_specialty)

    # Once all specialties are processed, compute the results for "all" specialty as follows:
    print("Creating files for {} specialty for both cpt and icd10 code sets...".format(SpecialtyNames.ALL_NO_EM))
    t0 = time.time()
    make_files_all_specialty(CPT_CODE_SET, all_specialty=SpecialtyNames.ALL_NO_EM)
    print("...done in {} sec.".format(time.time() - t0))

    print("Computing word embeddings for {} specialty...".format(SpecialtyNames.ALL_NO_EM))
    t0 = time.time()
    vocab_ = generate_wv_embeddings(SpecialtyNames.ALL_NO_EM, CPT_CODE_SET, 100, 0, 4, 5)
    to_pickle(DATA_DIR + "{}_{}_vocab.pickle".format(SpecialtyNames.ALL_NO_EM, CPT_CODE_SET), vocab_)
    print("...done in {} sec.".format(time.time() - t0))

    print("Computing {}_notes_with_{}_feedback.csv...".format(SpecialtyNames.ALL_NO_EM, CPT_CODE_SET))
    t0 = time.time()
    train_df = pd.read_csv(DATA_DIR + "{}_{}_train.csv".format(SpecialtyNames.ALL_NO_EM, CPT_CODE_SET))
    validate_df = pd.read_csv(DATA_DIR + "{}_{}_validate.csv".format(SpecialtyNames.ALL_NO_EM, CPT_CODE_SET))
    test_df = pd.read_csv(DATA_DIR + "{}_{}_test.csv".format(SpecialtyNames.ALL_NO_EM, CPT_CODE_SET))
    notes_with_feedback_df = pd.concat([train_df, validate_df, test_df])
    notes_with_feedback_df.to_csv(
        DATA_DIR + "{}_notes_with_{}_feedback.csv".format(SpecialtyNames.ALL_NO_EM, CPT_CODE_SET),
        index=False)
    print("...done in {} sec.".format(time.time() - t0))
