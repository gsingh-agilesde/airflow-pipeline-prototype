import pandas as pd
import time

from utils.file_io_utils import to_pickle

from cnn_attention import DATA_DIR, CPT_CODE_SET

from cnn_attention.data_model.data_constants import SpecialtyNames, OutputColumns
from cnn_attention.data_processing.train_validate_test_split import create_train_validate_test_data_splits
from cnn_attention.data_processing.word_embeddings import generate_wv_embeddings
from cnn_attention.data_processing.analysis.organization_groups_analysis import basic_analysis, \
    __NO_OF_UNIQUE_CODE_LABELS, __PHYS_GROUP_ID, __NO_OF_NOTES
from cnn_attention.examples.train_test_evaluate_models import train_test_evaluate

specialty = SpecialtyNames.ORTHO_NO_EM
code_set = CPT_CODE_SET

if __name__ == '__main__':

    # 0. Read csv file containing notes, labels feedback for given specialty and code set.
    note_id_text_length_labels_df = pd.read_csv(DATA_DIR + specialty + "_notes_with_" + code_set + "_feedback.csv")

    # 1. Compute dataframe grouped by organization group id for given specialty and code set.
    org_group_df = basic_analysis(specialty, code_set)
    # 2.  Compute a subset of that dataframe such that we focus on organizations in which the number of code labels
    #     are more than 1.
    org_group_subset_df = org_group_df[
        (org_group_df[__NO_OF_UNIQUE_CODE_LABELS] > 1) & (org_group_df[__NO_OF_NOTES] > 5)]
    # 3. Extract all organization group ids.
    org_group_ids = org_group_subset_df[__PHYS_GROUP_ID].values.tolist()

    # 4. For each org_group_id that forms a "new" specialty, we perform the usual preprocessing steps.
    new_specialties = [None] * len(org_group_ids)
    for i, org_group_id in enumerate(org_group_ids):
        # 4.1 Each org_group_id defines a "new" specialty.
        new_specialty = specialty + "_" + org_group_id
        new_specialties[i] = new_specialty
        # 4.2 Compute a subset of original notes, labels feedback dataframe having only the records of curent
        #     org_group_id
        mask_ = note_id_text_length_labels_df[OutputColumns.NOTE_ID].apply(lambda x: x.split("_")[2] == org_group_id)
        subset_df = note_id_text_length_labels_df[mask_]
        # ---------
        # 4.3 Perform usual preprocessing steps.
        # ---------
        print("Creating _notes_with_ file....")
        t0 = time.time()
        subset_df.to_csv(DATA_DIR + new_specialty + "_notes_with_" + CPT_CODE_SET + "_feedback.csv", index=False)
        print("...done in {} sec.".format(time.time() - t0))

        print("Creating train, validate and test data sets for {} specialty...".format(new_specialty))
        t0 = time.time()
        create_train_validate_test_data_splits(new_specialty, code_set)
        print("...done in {} sec.".format(time.time() - t0))

        print("Computing word embeddings for {} specialty...".format(new_specialty))
        t0 = time.time()
        vocab_ = generate_wv_embeddings(new_specialty, code_set, 100, 0, 4, 5)
        to_pickle(DATA_DIR + "{}_{}_vocab.pickle".format(new_specialty, code_set), vocab_)
        print("...done in {} sec.".format(time.time() - t0))
        # ---------
        # ---------

    # ---------
    # 5. Perform train evaluate and test loop to perform micro modeling for the "new" specialty.
    # ---------
    train_test_evaluate(new_specialties, [code_set])
    # ---------
    # ---------
