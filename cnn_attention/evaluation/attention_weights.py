import os

import pandas as pd

from cnn_attention import MODEL_DIR, DATA_DIR, CPT_CODE_SET, CNN
from cnn_attention.data_model.data_constants import SpecialtyNames
from cnn_attention.training_and_prediction.model_training import ModelTraining
from utils.dict_utils import sort_dict_by_value
from utils.file_io_utils import from_pickle

from utils.pandas_utils import from_csv

"""
To run this script, we need code description file.
"""

CODE = "code"
DESC = "desc"
MODEL_NAME = "conv_attn_cardio_Jan_18_11_40"
FILE_NAMES = ["/test_attention_weights_fn.pickle", "/test_attention_weights_tp.pickle",
              "/test_attention_weights_fp.pickle"]
FILE_PATHS = [MODEL_DIR + MODEL_NAME + FILE_NAME for FILE_NAME in FILE_NAMES]
CODE_DESC_FILES = [DATA_DIR + "CPT2018_level1_descriptions.csv", DATA_DIR + "CPT2018_level2_descriptions.csv"]
COLUMNS = [CODE, DESC]

if not os.path.isfile(FILE_PATHS[0]):
    # load a pre-trained model
    n_epochs = 100
    batch_size = 32
    learning_rate = 0.25 * 1e-3
    model_training = ModelTraining(n_epochs, batch_size, MODEL_DIR + MODEL_NAME, 0, learning_rate,
                                   SpecialtyNames.CARDIO, CPT_CODE_SET,
                                   4, 50, CNN, False, 0, 0, "0")
    # calculate the attention weights and save the values in pickle file.
    loss_test_ = model_training.test_model(save_attention=True, top_k_codes=8)

code_to_desc = pd.DataFrame(columns=COLUMNS)
for CODE_DESC_FILE in CODE_DESC_FILES:
    code_to_desc_1 = from_csv(CODE_DESC_FILE, columns=COLUMNS)
    code_to_desc = code_to_desc.append(code_to_desc_1)

for FILE_NAME in FILE_NAMES:
    print("creating text for file : {}".format(FILE_NAME))
    attn_pickle = from_pickle(MODEL_DIR + MODEL_NAME + FILE_NAME)
    with open(MODEL_DIR + MODEL_NAME + FILE_NAME.replace("pickle", "txt"), "w") as file:
        for note_id, attn in attn_pickle.items():
            if attn:
                file.write("Note Id: {} \n\n".format(note_id))
                for code, attn_weights in attn.items():
                    code_desc = code_to_desc[code_to_desc[CODE] == code][DESC].values[0]
                    file.write(" " + code + " : " + code_desc + "\n\n")
                    for word, weight in sort_dict_by_value(attn_weights, reverse=True).items():
                        file.write("   " + word + " : " + str(weight) + "\n")
                    file.write("\n\n")
