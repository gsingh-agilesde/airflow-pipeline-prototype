import numpy as np
import pandas as pd
import time

from itertools import chain

from utils.file_io_utils import from_pickle
from utils.file_io_utils import load_json, file_names_from_directory

from cnn_attention import MODEL_DIR, DATA_DIR, CPT_CODE_SET, ICD10_CODE_SET

from cnn_attention.data_model.coder_feedback import CoderFeedback, Confidence, Specialty

from cnn_attention.evaluation.metric_set import MetricSetCnnAttentionNoAuroc
from cnn_attention.data_model.data_constants import OutputColumns, SpecialtyNames

# ######################################################################################################################
# Outline
# ######################################################################################################################
# 1. Load all 8 recent models - (CPT, ICD-10-CM) * (Cardio, Ortho, PCP, All)
#
# 1.1 Load test_scores.pickle file for each.
#
#
# 2. Load all ground truth files from DATA_DIR + "/coder_feedback/" folder.
#
# 2.1 Each file gives a list of dict instances. Append all lists to create one single list, L.
#
# 2.2 For each item in L, extract specialty, s.
#
# 2.3 For each item in L, focus on particular code set, c.
#
# 2.4 Given, c and s and code co, if confidence == 'YES' or confidence == 'MAYBE' or confidence == None, we add it to
#     ground truth for the particular (code set + specialty) test csv as well as to (code set + 'all' specialty) test
#     csv. Thus for every code we should update 2 csv files -- one for the specific specialty and one for 'all'
#     specialty.
#
# 2.5 In this way, we rebuild all the ground truth test csv files.
#
# 2.6 We need to create custom specialties since existing specialty names have already be taken.
#     We append the string _coder to all specialties to achieve this.
#
#
# 2.6 Once ground truth csv files are built, we call test() function with "ALL" specialty and supplying all 3 main
#     test scores file paths, one each for cardio, ortho and pcp.
#     Another test may be done by supplying only one test score file path, namely, for the one for "all" specialty.
# ######################################################################################################################

__SPECIALTY_TO_NAME = {Specialty.CARDIO: SpecialtyNames.CARDIO,
                       Specialty.ORTHO: SpecialtyNames.ORTHO,
                       Specialty.PCP: SpecialtyNames.PCP}

__CODER = "coder"


def create_single_list(file_paths):
    result = []
    for file_path in file_paths:
        result += load_json(file_path)
    return result


def create_ground_truth_csv_files():
    # 1. Load all 8 recent models.

    # 2. Load all ground truth files from DATA_DIR + "/coder_feedback/" folder.
    # 2.1 Each file gives a list of dict instances. Append all lists to create one single list, L.
    coder_feedback_path = DATA_DIR + "/coder_feedback/"
    coder_feedback_output = create_single_list([coder_feedback_path + file_name
                                                for file_name in file_names_from_directory(coder_feedback_path)])

    # 2.2 For each item in L, extract specialty, s.
    # 2.3 For each item in L, focus on particular code set, c.
    # 2.4 Given, c and s and code co, if confidence == 'YES' or confidence == 'MAYBE' or confidence == None, we add it
    #     to ground truth for the particular (code set + specialty) test csv as well as to (code set + 'all' specialty)
    #     test csv. Thus for every code we should update 2 csv files -- one for the specific specialty and one for 'all'
    #     specialty.

    # Define an empty output dictionary to create 6 csv files for ground truth.
    # We then create the  2 remaining csv file for 'all' specialty from these 6 csv files.
    ground_truth_total = {CPT_CODE_SET:
                              {SpecialtyNames.CARDIO:
                                   {OutputColumns.NOTE_ID: [], OutputColumns.CPT_LABELS: []
                                    },
                               SpecialtyNames.ORTHO:
                                   {OutputColumns.NOTE_ID: [], OutputColumns.CPT_LABELS: []
                                    },
                               SpecialtyNames.PCP:
                                   {OutputColumns.NOTE_ID: [], OutputColumns.CPT_LABELS: []
                                    },
                               },
                          ICD10_CODE_SET:
                              {SpecialtyNames.CARDIO:
                                   {OutputColumns.NOTE_ID: [], OutputColumns.ICD_LABELS: []
                                    },
                               SpecialtyNames.ORTHO:
                                   {OutputColumns.NOTE_ID: [], OutputColumns.ICD_LABELS: []
                                    },
                               SpecialtyNames.PCP:
                                   {OutputColumns.NOTE_ID: [], OutputColumns.ICD_LABELS: []
                                    },
                               }
                          }

    for output in coder_feedback_output:
        note_id = "_".join(output[CoderFeedback.FILE_NAME].split(".")[0].split("_")[1:])
        specialty = output[CoderFeedback.SPECIALTY]

        cpt_codes = output[CoderFeedback.CPT_CODES]
        ground_truth_cpt_codes = ";".join([code[CoderFeedback.CODE] for code in cpt_codes
                                           if code[CoderFeedback.CONFIDENCE] == Confidence.YES
                                           or code[CoderFeedback.CONFIDENCE] == Confidence.NONE
                                           or code[CoderFeedback.CONFIDENCE] == Confidence.MAYBE])
        ground_truth_total[CPT_CODE_SET][__SPECIALTY_TO_NAME[specialty]][OutputColumns.NOTE_ID].append(note_id)
        ground_truth_total[CPT_CODE_SET][__SPECIALTY_TO_NAME[specialty]][OutputColumns.CPT_LABELS].append(
            ground_truth_cpt_codes)

        icd_codes = output[CoderFeedback.ICD_CODES]
        ground_truth_icd_codes = ";".join([code[CoderFeedback.CODE].replace(".", "") for code in icd_codes
                                           if code[CoderFeedback.CONFIDENCE] == Confidence.YES
                                           or code[CoderFeedback.CONFIDENCE] == Confidence.NONE
                                           or code[CoderFeedback.CONFIDENCE] == Confidence.MAYBE])
        ground_truth_total[ICD10_CODE_SET][__SPECIALTY_TO_NAME[specialty]][OutputColumns.NOTE_ID].append(note_id)
        ground_truth_total[ICD10_CODE_SET][__SPECIALTY_TO_NAME[specialty]][OutputColumns.ICD_LABELS].append(
            ground_truth_icd_codes)

    cardio_cpt_test_df = pd.DataFrame(ground_truth_total[CPT_CODE_SET][SpecialtyNames.CARDIO])
    cardio_cpt_test_df.to_csv(DATA_DIR + SpecialtyNames.CARDIO + "_" + __CODER + "_" + CPT_CODE_SET + "_test.csv",
                              index=False)
    ortho_cpt_test_df = pd.DataFrame(ground_truth_total[CPT_CODE_SET][SpecialtyNames.ORTHO])
    ortho_cpt_test_df.to_csv(DATA_DIR + SpecialtyNames.ORTHO + "_" + __CODER + "_" + CPT_CODE_SET + "_test.csv",
                             index=False)
    pcp_cpt_test_df = pd.DataFrame(ground_truth_total[CPT_CODE_SET][SpecialtyNames.PCP])
    pcp_cpt_test_df.to_csv(DATA_DIR + SpecialtyNames.PCP + "_" + __CODER + "_" + CPT_CODE_SET + "_test.csv",
                           index=False)
    cardio_icd_test_df = pd.DataFrame(ground_truth_total[ICD10_CODE_SET][SpecialtyNames.CARDIO])
    cardio_icd_test_df.to_csv(DATA_DIR + SpecialtyNames.CARDIO + "_" + __CODER + "_" + ICD10_CODE_SET + "_test.csv",
                              index=False)
    ortho_icd_test_df = pd.DataFrame(ground_truth_total[ICD10_CODE_SET][SpecialtyNames.ORTHO])
    ortho_icd_test_df.to_csv(DATA_DIR + SpecialtyNames.ORTHO + "_" + __CODER + "_" + ICD10_CODE_SET + "_test.csv",
                             index=False)
    pcp_icd_test_df = pd.DataFrame(ground_truth_total[ICD10_CODE_SET][SpecialtyNames.PCP])
    pcp_icd_test_df.to_csv(DATA_DIR + SpecialtyNames.PCP + "_" + __CODER + "_" + ICD10_CODE_SET + "_test.csv",
                           index=False)

    all_cpt_test_df = pd.concat([cardio_cpt_test_df, ortho_cpt_test_df, pcp_cpt_test_df])
    all_cpt_test_df.to_csv(DATA_DIR + SpecialtyNames.ALL + "_" + __CODER + "_" + CPT_CODE_SET + "_test.csv",
                           index=False)
    all_icd_test_df = pd.concat([cardio_icd_test_df, ortho_icd_test_df, pcp_icd_test_df])
    all_icd_test_df.to_csv(DATA_DIR + SpecialtyNames.ALL + "_" + __CODER + "_" + ICD10_CODE_SET + "_test.csv",
                           index=False)


def test(test_scores_file_paths, ground_truth_labels_file_path, code_set):
    """
    Tests model performance using metrics defined under MetricSetCnnAttention.
    :param test_scores_file_paths: Scores predicted for test set.
    :param ground_truth_labels_file_path: Ground truth labels for the test set.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :returns result dictionary containing the scores for the metrics.
    """

    if code_set == CPT_CODE_SET:
        labels = OutputColumns.CPT_LABELS
    elif code_set == ICD10_CODE_SET:
        labels = OutputColumns.ICD_LABELS
    else:
        raise ValueError("{} code set is not defined. Choose a valid code set.".format(code_set))

    # Read the ground-truth labels.
    test_df = pd.read_csv(ground_truth_labels_file_path)
    test_df[labels] = test_df[labels].apply(lambda x: str(x).split(";"))

    note_ids_from_ground_truth = set(test_df[OutputColumns.NOTE_ID].values.tolist())

    # Read result
    result = {}
    for file_path in test_scores_file_paths:
        result.update(from_pickle(file_path))

    # For each unique label (i.e., unique code from cardio specialty labels data) we map it to a unique index.
    # This is useful to create y_true (ground truth labels) and y_pred (predictions) arrays needed for metric
    # computation.
    note_ids_model_scored = list(result.keys())

    note_ids = note_ids_from_ground_truth.intersection(note_ids_model_scored)

    print(
        "Number of note ids model scored: {}\nNumber of note ids in ground truth: {}\nNumber of common note ids: {}\n"
            .format(len(note_ids_model_scored), len(note_ids_from_ground_truth), len(note_ids)))

    set_code_labels = set()
    for note_id in note_ids:
        set_code_labels = set_code_labels.union(set(result[note_id].keys()))

    set_code_labels_from_ground_truth = set(chain.from_iterable(test_df[labels].values.tolist()))
    set_code_labels_from_ground_truth.remove("nan")

    set_code_labels = set_code_labels.union(set_code_labels_from_ground_truth)

    code_to_index = dict()
    z = 0
    for code in set_code_labels:
        code_to_index[code] = z
        z += 1

    # Initialize y_true and y_pred numpy arrays.
    y_true = np.zeros((len(note_ids), len(set_code_labels)), dtype=float)
    y_pred = np.zeros((len(note_ids), len(set_code_labels)), dtype=float)

    t0 = time.time()
    # Set y_true and y_pred
    for i, note_id in enumerate(note_ids):
        if (i + 1) % 1000 == 0:
            print("index = {}, time elapsed = {}".format(i + 1, time.time() - t0))
            t0 = time.time()

        code_to_score = result[note_id]

        slice_df = test_df[test_df[OutputColumns.NOTE_ID] == note_id]
        ground_truth_labels = slice_df[labels].values.tolist()

        for code in ground_truth_labels[0]:
            if str(code) != "nan":
                y_true[i, code_to_index[code]] = 1

        for code in code_to_score:
            raw_score = code_to_score[code]
            y_pred[i, code_to_index[code]] = raw_score

    # Parameters for defining the metric set.
    # Threshold for rounding predictions (values between 0 and 1) to either 0 (not predicted) or 1 (predicted).
    thresh = 0.5
    # Values of n for computing the @n statistics
    # at_n_values = (6, 8, 15, 20, 30, 50, 100)
    at_n_values = (20, 40)
    # Define the metric set.
    metric_set = MetricSetCnnAttentionNoAuroc(thresh=thresh, at_n_values=at_n_values)

    # Compute the metric set.
    print("\nModel testing results:\n")
    result = metric_set.compute(y_true=y_true, y_pred=y_pred)
    print(result)
    return result


if __name__ == '__main__':
    # Create ground-truth files from coder feedback.
    create_ground_truth_csv_files()

    # Read results saved under saved_models/, i.e., MODEL_DIR directory.
    code_set = CPT_CODE_SET
    specialty = SpecialtyNames.ALL
    test_scores_file_paths = [MODEL_DIR + "/conv_attn_cardio_CPT_120_Apr_08_12_27/test_scores.pickle",
                              MODEL_DIR + "/conv_attn_ortho_CPT_120_Apr_08_14_58/test_scores.pickle",
                              MODEL_DIR + "/conv_attn_pcp_CPT_120_Apr_08_18_29/test_scores.pickle"]

    # Read the ground-truth labels for "all" and "coder" specialty.
    ground_truth_labels_file_path = DATA_DIR + "{}_{}_{}_test.csv".format(specialty, __CODER, code_set)

    # Test model performance.
    test(test_scores_file_paths, ground_truth_labels_file_path, code_set)
