from cnn_attention.evaluation.test_model_performance import test
from utils.file_io_utils import to_pickle, from_pickle
import pandas as pd
from cnn_attention import MODEL_DIR, DATA_DIR, EM_NEW, EM_EST

from cnn_attention.data_model.data_constants import SpecialtyNames

code_set_ = EM_EST
# models = {SpecialtyNames.CARDIO: "/conv_attn_cardio_EM_Jun_03_11_47",
#           SpecialtyNames.ORTHO: "/conv_attn_ortho_EM_May_30_17_41",
#           SpecialtyNames.PCP: "/conv_attn_pcp_EM_May_30_17_42",
#           SpecialtyNames.ALL: "/conv_attn_all_EM_May_30_17_47"}
models = {SpecialtyNames.ALL: "/conv_attn_all_EM_Jun_03_16_12"}
for specialty_, model in models.items():
    test_scores_file_path = MODEL_DIR + model + "/test_scores.pickle"
    # Read the ground-truth labels for cardio specialty.
    # This should be one of the output files for "test" split from
    # cnn_attention/data_processing/train_validate_test_split.py
    ground_truth_labels_file_path = DATA_DIR + "{}_{}_test.csv".format(specialty_, code_set_)
    # Test model performance for only non E/M codes
    test_df = pd.read_csv(ground_truth_labels_file_path, dtype=object)
    result = from_pickle(test_scores_file_path)
    top_code = dict()
    for note_id, scores in result.items():
        max_score = 0
        max_code = ""
        for code, score in scores.items():
            if score >= max_score:
                max_code = code

        top_code[note_id] = max_code
    prediction_df = pd.DataFrame(list(top_code.items()), columns=['note_id', 'output'])
    final_df = test_df.merge(prediction_df, on="note_id")[["note_id","output","cpt_labels"]]
    final_df["level"] = final_df["cpt_labels"].apply(lambda x: int(x)) - final_df["output"].apply(lambda x: int(x))
    undercoded_df = final_df[final_df["level"] >= 0]
    final_df.to_csv(MODEL_DIR + model + "/em_code_prediction_level.csv")
    print("done")