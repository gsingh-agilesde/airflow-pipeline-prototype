import numpy as np
import pandas as pd
import time

from pipeline.data.em_code import EMCodeConstraints
from utils.file_io_utils import from_pickle

from cnn_attention import MODEL_DIR, DATA_DIR, CPT_CODE_SET, ICD10_CODE_SET, CPT_LABEL_CODE_SETS

from cnn_attention.evaluation.metric_set import MetricSetCnnAttention, MetricSetCnnAttentionOnlyPRF1
from cnn_attention.data_model.data_constants import OutputColumns, SpecialtyNames


def get_labels_column_name_for(code_set):
    """
    Returns the output labels column name corresponding to the input code set if code set is valid.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :return: output labels column name corresponding to the input code set
    """
    if code_set in CPT_LABEL_CODE_SETS:
        labels = OutputColumns.CPT_LABELS
    elif code_set == ICD10_CODE_SET:
        labels = OutputColumns.ICD_LABELS
    else:
        raise ValueError("{} code set is not defined. Choose a valid code set.".format(code_set))
    return labels


def test(test_scores_file_path, ground_truth_labels_file_path, specialty, code_set, non_em_codes_only=False):
    """
    Tests model performance using metrics defined under MetricSetCnnAttention.
    :param test_scores_file_path: Scores predicted for test set.
    :param ground_truth_labels_file_path: Ground truth labels for the test set.
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :param non_em_codes_only: flag for indicating if the testing should be done for only non E/M codes.
    :returns result dictionary containing the scores for the metrics.
    """

    labels = get_labels_column_name_for(code_set)

    # Read the ground-truth labels
    test_df = pd.read_csv(ground_truth_labels_file_path)
    test_df[labels] = test_df[labels].apply(lambda x: x.split(";"))

    # Read result
    result = from_pickle(test_scores_file_path)

    # For each unique label (i.e., unique code from cardio specialty labels data) we map it to a unique index.
    # This is useful to create y_true (ground truth labels) and y_pred (predictions) arrays needed for metric
    # computation.
    note_ids = list(result.keys())
    file_name = DATA_DIR + "{}_codes_in_{}_specialty.pickle".format(code_set, specialty)
    set_code_labels = from_pickle(file_name)

    code_to_index = dict()
    z = 0
    em_code_list = set()
    for code in set_code_labels:
        if non_em_codes_only and str(code).startswith("99"):
            if EMCodeConstraints.MIN_EM_CODE_VALUE <= int(code) <= EMCodeConstraints.MAX_EM_CODE_VALUE:
                em_code_list.add(code)
                continue
        code_to_index[code] = z
        z += 1

    # Initialize y_true and y_pred numpy arrays.
    y_true = np.zeros((len(note_ids), len(code_to_index)), dtype=float)
    y_pred = np.zeros((len(note_ids), len(code_to_index)), dtype=float)

    t0 = time.time()
    # Set y_true and y_pred
    for i, note_id in enumerate(note_ids):
        if (i + 1) % 1000 == 0:
            print("index = {}, time elapsed = {}".format(i + 1, time.time() - t0))
            t0 = time.time()

        code_to_score = result[note_id]

        slice_df = test_df[test_df[OutputColumns.NOTE_ID] == note_id]
        ground_truth_labels = slice_df[labels].values.tolist()

        for code in ground_truth_labels[0]:
            if code not in em_code_list:
                y_true[i, code_to_index[code]] = 1

        for code in code_to_score:
            if code not in em_code_list:
                raw_score = code_to_score[code]
                y_pred[i, code_to_index[code]] = raw_score

    # Parameters for defining the metric set.
    # Threshold for rounding predictions (values between 0 and 1) to either 0 (not predicted) or 1 (predicted).
    thresh = 0.5
    # Values of n for computing the @n statistics
    at_n_values = (6, 8, 15, 20, 30, 50, 100)

    # Define the metric set.
    metric_set = MetricSetCnnAttention(thresh=thresh, at_n_values=at_n_values)

    # Compute the metric set.
    print("\nModel testing results:\n")
    result = metric_set.compute(y_true=y_true, y_pred=y_pred)
    print(result)
    return result


def test_performance_for_code_groups(test_scores_file_dir, ground_truth_labels_file_path, specialty, code_set,
                                     thresholds):
    """
    Tests model performance using Precision Recall and F1 Score metrics at different threshold values.
    :param test_scores_file_dir: Scores predicted for test set.
    :param ground_truth_labels_file_path: Ground truth labels for the test set.
    :param specialty: The selected specialty defined under cnn_attention/data_model/data_constants class SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
    :param thresholds: threshold values for calculating the prediction metrics.
    :returns saves the metric set results in a csv.
    """

    labels = get_labels_column_name_for(code_set)

    # Read the ground-truth labels
    test_df = pd.read_csv(ground_truth_labels_file_path, dtype=object)
    code_freq = test_df.groupby(OutputColumns.CPT_LABELS).count().reset_index()[
        [OutputColumns.CPT_LABELS, OutputColumns.NOTE_ID]]
    test_groups = test_df.groupby(OutputColumns.CPT_LABELS)[OutputColumns.NOTE_ID].apply(list).reset_index()
    test_df[labels] = test_df[labels].apply(lambda x: x.split(";"))

    # Read result
    result = from_pickle(test_scores_file_dir + "test_scores.pickle")

    # For each unique label (i.e., unique code from cardio specialty labels data) we map it to a unique index.
    # This is useful to create y_true (ground truth labels) and y_pred (predictions) arrays needed for metric
    # computation.
    note_ids = list(result.keys())
    file_name = DATA_DIR + "{}_codes_in_{}_specialty.pickle".format(code_set, specialty)
    set_code_labels = from_pickle(file_name)

    code_to_index = dict()
    z = 0
    for code in set_code_labels:
        code_to_index[code] = z
        z += 1

    # Initialize y_true and y_pred numpy arrays.
    y_true = np.zeros((len(note_ids), len(code_to_index)), dtype=float)
    y_pred = np.zeros((len(note_ids), len(code_to_index)), dtype=float)

    # Set y_true and y_pred
    for thresh in thresholds:
        missing_note_id = list()
        group_df = pd.DataFrame(
            columns=[OutputColumns.CPT_LABELS, "precision_micro", "recall_micro", "f1_micro", "precision_macro",
                     "recall_macro", "f1_macro"])

        # Define the metric set.
        metric_set = MetricSetCnnAttentionOnlyPRF1(thresh=thresh)

        for row in test_groups.iterrows():
            for i, note_id in enumerate(row[1][OutputColumns.NOTE_ID]):
                if note_id in result:
                    code_to_score = result[note_id]

                    slice_df = test_df[test_df[OutputColumns.NOTE_ID] == note_id]
                    ground_truth_labels = slice_df[labels].values.tolist()

                    for code in ground_truth_labels[0]:
                        y_true[i, code_to_index[code]] = 1

                    for code in code_to_score:
                        raw_score = code_to_score[code]
                        y_pred[i, code_to_index[code]] = raw_score
                else:
                    missing_note_id.append(note_id)

            # Compute the metric set.
            metric_score = metric_set.compute(y_true=y_true, y_pred=y_pred)["PrecisionRecallF1"]
            micro_score = metric_score["micro"]
            macro_score = metric_score["macro"]
            precision_micro = micro_score[0]
            recall_micro = micro_score[1]
            f1_micro = micro_score[2]
            precision_macro = macro_score[0]
            recall_macro = macro_score[1]
            f1_macro = macro_score[2]
            group_df = group_df.append({OutputColumns.CPT_LABELS: row[1][OutputColumns.CPT_LABELS],
                                        "precision_micro": precision_micro,
                                        "recall_micro": recall_micro,
                                        "f1_micro": f1_micro,
                                        "precision_macro": precision_macro,
                                        "recall_macro": recall_macro,
                                        "f1_macro": f1_macro}, ignore_index=True)
        final_df = group_df.merge(code_freq).sort_values(OutputColumns.NOTE_ID, ascending=False)
        final_df.to_csv(
            test_scores_file_dir + "code_group_metric_results_at_{}.csv".format(str(thresh).replace(".", "_")),
            index=False)


if __name__ == '__main__':
    # Read result under saved_models/, i.e., MODEL_DIR directory.
    # Let us assume that we have a result stored for model trained on specialty="cardio".
    code_set_ = CPT_CODE_SET
    specialty_ = SpecialtyNames.CARDIO
    test_scores_file_path_ = MODEL_DIR + "/conv_attn_cardio_CPT_20_Apr_08_11_08/test_scores.pickle"
    # Read the ground-truth labels for cardio specialty.
    # This should be one of the output files for "test" split from
    # cnn_attention/data_processing/train_validate_test_split.py
    ground_truth_labels_file_path_ = DATA_DIR + "{}_{}_test.csv".format(specialty_, code_set_)
    # Test model performance.
    test(test_scores_file_path_, ground_truth_labels_file_path_, specialty_, code_set_)
    # Test model performance for only non E/M codes
    test(test_scores_file_path_, ground_truth_labels_file_path_, specialty_, code_set_, non_em_codes_only=True)
    # Test the model performance for code groups

    test_scores_file_dir_ = MODEL_DIR + "/conv_attn_cardio_CPT_20_Apr_08_11_08/"
    thresholds_ = [0.5, 0.8, 0.92]
    test_performance_for_code_groups(test_scores_file_dir_, ground_truth_labels_file_path_, specialty_, code_set_,
                                     thresholds_)
