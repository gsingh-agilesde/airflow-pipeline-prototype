from cnn_attention.evaluation.test_model_performance import test
from utils.file_io_utils import to_pickle

from cnn_attention import MODEL_DIR, DATA_DIR, CPT_CODE_SET

from cnn_attention.data_model.data_constants import SpecialtyNames

code_set_ = CPT_CODE_SET
models = {SpecialtyNames.CARDIO: "/conv_attn_cardio_CPT_120_Apr_08_12_27",
          SpecialtyNames.ORTHO: "/conv_attn_ortho_CPT_120_Apr_08_14_58",
          SpecialtyNames.PCP: "/conv_attn_pcp_CPT_120_Apr_08_18_29",
          SpecialtyNames.ALL: "/conv_attn_all_CPT_120_Apr_08_23_59"}
for specialty_, model in models.items():
    test_scores_file_path_ = MODEL_DIR + model + "/test_scores.pickle"
    # Read the ground-truth labels for cardio specialty.
    # This should be one of the output files for "test" split from
    # cnn_attention/data_processing/train_validate_test_split.py
    ground_truth_labels_file_path_ = DATA_DIR + "{}_{}_test.csv".format(specialty_, code_set_)
    # Test model performance for only non E/M codes
    result_ = test(test_scores_file_path_, ground_truth_labels_file_path_, specialty_, code_set_,
                   non_em_codes_only=True)
    to_pickle(MODEL_DIR + model + "/evaluation_scores_non_em_codes.pickle", result_)
