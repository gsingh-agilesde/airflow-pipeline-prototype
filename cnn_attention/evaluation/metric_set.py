import time
import math
from typing import Dict

from sklearn.metrics import precision_recall_fscore_support, roc_auc_score, accuracy_score
import numpy as np
from enum import Enum


class MetricNames:
    ACCURACY = "accuracy"
    PRECISION_RECALL_F1 = "PrecisionRecallF1"
    PRECISION_RECALL_F1_AT_N = "PrecisionRecallF1AtN"
    AUROC = "AuRoc"


class AveragingTechniques(Enum):
    MICRO = "micro"
    MACRO = "macro"


class MetricSet:

    def __init__(self, thresh=0.5, at_n_values=(8, 15)):
        """
        :param thresh: Threshold for rounding predictions (values between 0 and 1) to either 0 (not predicted) or
                       1 (predicted).
        :param at_n_values: Values of n for computing the @n statistics
        """
        self.metric_name_to_function = dict()
        self.thresh = thresh
        self.at_n_values = at_n_values


    def compute(self, y_true, y_pred):
        """
        Computes all metric values found in the chosen the MetricSet class.
        :param y_true: ground truth (M, N) numpy array
        :param y_pred: predictions (M, N) numpy array
        :return: dictionary with key as the metric name and value as its calculated value.
        """
        result_dict: Dict[MetricNames, Dict] = dict()
        for metric_name in self.metric_name_to_function:
            f = self.metric_name_to_function[metric_name]
            result_dict[metric_name] = f(y_true=y_true, y_pred=y_pred)
        return result_dict


    def accuracy(self, y_true, y_pred):
        """
        Computes the accuracy using the ground truth and the predictions.

        This function converts the continuous probability values in y_pred to discrete class labels and then
        applies the scikit-learn accuracy_Score function to obtain the metric for continuous valued y_pred.

        [Note: that the choice of threshold used to obtain positive or negative label is another parameter and this
        function does not provide any prescription for its choice. This needs to be carefully set per one's goals.]

        :param y_true: ground truth (M, N) numpy array
        :param y_pred: predictions (M, N) numpy array
        :return: dictionary with key as averaging technique value from AveragingTechniques class and value as the value
                 of the computed metric.
        """
        y_pred_copy = np.zeros(y_pred.shape)
        y_pred_copy[y_pred >= self.thresh] = 1

        n_labels = y_true.shape[1]
        both_not_zeros_ind = np.logical_or(y_pred_copy, y_true)

        result_ = dict()
        for technique in AveragingTechniques:
            if technique == AveragingTechniques.MACRO:
                per_label_result_ = np.zeros((n_labels, 1))
                for i in range(n_labels):
                    y_t = y_true[:, i][both_not_zeros_ind[:, i]]
                    y_p = y_pred_copy[:, i][both_not_zeros_ind[:, i]]
                    per_label_result_[i] = accuracy_score(y_true=y_t, y_pred=y_p)
                per_label_result_[np.isnan(per_label_result_)] = 0.0
                result_[technique.value] = np.mean(per_label_result_)
            elif technique == AveragingTechniques.MICRO:
                y_t = y_true.ravel()[both_not_zeros_ind.ravel()]
                y_p = y_pred_copy.ravel()[both_not_zeros_ind.ravel()]
                result_[technique.value] = accuracy_score(y_true=y_t, y_pred=y_p)
        return result_


    def precision_recall_f1(self, y_true, y_pred):
        """
        Computes the precision, recall, and f1 score given the ground truth and the predictions.

        This function converts the continuous probability values in y_pred to discrete class labels and then applies the
        scikit-learn precision_recall_fscore_support function to obtain the metric for continuous valued y_pred.

        [Note: that the choice of threshold used to obtain positive or negative label is another parameter and this
        function does not provide any prescription for its choice. This needs to be carefully set per one's goals.]

        :param y_true: ground truth (M, N) numpy array
        :param y_pred: predictions (M, N) numpy array
        :return: dictionary with key as averaging technique value from AveragingTechniques class and value as the value
                 of the computed metric.
        """
        y_pred_copy = np.zeros(y_pred.shape)
        y_pred_copy[y_pred >= self.thresh] = 1
        result_ = dict()
        for technique in AveragingTechniques:
            # Select [0:3] as we do not want to include support in our result.
            result_[technique.value] = precision_recall_fscore_support(y_true=y_true, y_pred=y_pred_copy,
                                                                       average=technique.value)[0:3]
        return result_


    def precision_recall_f1_at_n(self, y_true, y_pred):
        """
        Computes the precision, recall, and f1 score @n given the ground truth and the predictions. For @n statistics we
        first recompute the y_pred by setting the n highest-scored labels as 1 (i.e., predicted) and setting the
        remaining N-n labels as 0 (i.e., not predicted). We then compute precision, recall and f1-score in the usual way
        using the updated y_pred at n.
        :param y_true: ground truth (M, N) numpy array
        :param y_pred: predictions (M, N) numpy array
        :return: dictionary with key as averaging technique value from AveragingTechniques class and value as the value
                 of the computed metric.
        """
        sorted_y_pred_indices = np.argsort(y_pred)[:, ::-1]
        result_ = dict()
        for n in self.at_n_values:
            print("starting for n  = {}".format(n))
            t1 = time.time()
            y_pred_copy = np.zeros(y_pred.shape)
            top_n = sorted_y_pred_indices[:, :n]
            top_y_pred_x = np.repeat(np.arange(top_n.shape[0]), top_n.shape[1])
            top_y_pred_y = top_n.ravel()
            y_pred_copy[top_y_pred_x, top_y_pred_y] = 1
            avg_dict_ = dict()
            print("calculating the metrics")
            for technique in AveragingTechniques:
                # Select [0:3] as we do not want to include support in our result.
                avg_dict_[technique.value] = precision_recall_fscore_support(y_true=y_true, y_pred=y_pred_copy,
                                                                             average=technique.value)[0:3]
            print("done in {} secs.".format(time.time() - t1))
            result_[n] = avg_dict_
        return result_


    @staticmethod
    def auroc(y_true, y_pred):
        """
        Computes the area under the receiving operating curve given the ground truth and the predictions.
        :param y_true: ground truth (M, N) numpy array
        :param y_pred: predictions (M, N) numpy array
        :return: dictionary with key as averaging technique value from AveragingTechniques class and value as the value
                 of the computed metric.
        """
        result_ = dict()
        for technique in AveragingTechniques:
            if technique == AveragingTechniques.MACRO:
                non_zero_column_indices = ~np.all(y_true == 0, axis=0)
                result_[technique.value] = roc_auc_score(y_true=y_true[:, non_zero_column_indices],
                                                         y_score=y_pred[:, non_zero_column_indices],
                                                         average=technique.value)
            else:
                result_[technique.value] = roc_auc_score(y_true=y_true, y_score=y_pred, average=technique.value)
        return result_


    @staticmethod
    def is_almost_equal_accuracy(result_1, result_2, places=3):
        is_close = True
        for technique in AveragingTechniques:
            is_close &= math.isclose(result_1[technique.value], result_2[technique.value], rel_tol=10 ** (-places))
        return is_close


    @staticmethod
    def is_almost_equal_precision_recall_f1(result_1, result_2, places=3):
        is_close = True
        for technique in AveragingTechniques:
            is_close &= \
                math.isclose(result_1[technique.value][0], result_2[technique.value][0], rel_tol=10 ** (-places)) & \
                math.isclose(result_1[technique.value][1], result_2[technique.value][1], rel_tol=10 ** (-places)) & \
                math.isclose(result_1[technique.value][2], result_2[technique.value][2], rel_tol=10 ** (-places))
        return is_close


    @staticmethod
    def is_almost_equal_precision_recall_f1_at_n(result_1, result_2, places=3):
        if set(result_1.keys()) != set(result_2.keys()):
            return False
        is_close = True
        for n in result_1.keys():
            is_close &= MetricSetCnnAttention.is_almost_equal_precision_recall_f1(result_1[n], result_2[n], places)
        return is_close


    @staticmethod
    def is_almost_equal_auroc(result_1, result_2, places=3):
        is_close = True
        for technique in AveragingTechniques:
            is_close &= math.isclose(result_1[technique.value], result_2[technique.value], rel_tol=10 ** (-places))
        return is_close


class MetricSetCnnAttention(MetricSet):
    """
    Metric set specific to the needs of cnn attention project.
    """


    def __init__(self, thresh=0.5, at_n_values=(8, 15, 20, 30, 50, 100)):
        """
        :param thresh: Threshold for rounding predictions (values between 0 and 1) to either 0 (not predicted) or
                       1 (predicted).
        :param at_n_values: Values of n for computing the @n statistics
        """
        super(MetricSetCnnAttention, self).__init__(thresh, at_n_values)
        self.metric_name_to_function[MetricNames.ACCURACY] = self.accuracy
        self.metric_name_to_function[MetricNames.PRECISION_RECALL_F1] = self.precision_recall_f1
        self.metric_name_to_function[MetricNames.PRECISION_RECALL_F1_AT_N] = self.precision_recall_f1_at_n
        self.metric_name_to_function[MetricNames.AUROC] = self.auroc


    @staticmethod
    def is_almost_equal_metric_set_result(result_1, result_2, places=3):
        if set(result_1.keys()) != set(result_2.keys()):
            return False
        is_close_accuracy = MetricSetCnnAttention.is_almost_equal_accuracy(
            result_1[MetricNames.ACCURACY],
            result_2[MetricNames.ACCURACY],
            places
        )
        is_close_precision_recall_f1 = MetricSetCnnAttention.is_almost_equal_precision_recall_f1(
            result_1[MetricNames.PRECISION_RECALL_F1],
            result_2[MetricNames.PRECISION_RECALL_F1],
            places
        )
        is_close_precision_recall_f1_at_n = MetricSetCnnAttention.is_almost_equal_precision_recall_f1_at_n(
            result_1[MetricNames.PRECISION_RECALL_F1_AT_N],
            result_2[MetricNames.PRECISION_RECALL_F1_AT_N],
            places
        )
        is_close_auroc = MetricSetCnnAttention.is_almost_equal_auroc(
            result_1[MetricNames.AUROC],
            result_2[MetricNames.AUROC],
            places
        )
        return is_close_accuracy & is_close_precision_recall_f1 & is_close_precision_recall_f1_at_n & is_close_auroc


class MetricSetCnnAttentionNoAuroc(MetricSet):
    """
       Metric set specific to the needs of cnn attention project.
       """


    def __init__(self, thresh=0.5, at_n_values=(8, 15, 20, 30, 50, 100)):
        """
        :param thresh: Threshold for rounding predictions (values between 0 and 1) to either 0 (not predicted) or
                       1 (predicted).
        :param at_n_values: Values of n for computing the @n statistics
        """
        super(MetricSetCnnAttentionNoAuroc, self).__init__(thresh, at_n_values)
        self.metric_name_to_function[MetricNames.ACCURACY] = self.accuracy
        self.metric_name_to_function[MetricNames.PRECISION_RECALL_F1] = self.precision_recall_f1
        self.metric_name_to_function[MetricNames.PRECISION_RECALL_F1_AT_N] = self.precision_recall_f1_at_n


class MetricSetCnnAttentionOnlyPRF1(MetricSet):
    """
       Metric set for calculating only Precision, Recall and F1 Score at a threshold.
       """


    def __init__(self, thresh=0.5):
        """
        :param thresh: Threshold for rounding predictions (values between 0 and 1) to either 0 (not predicted) or
                       1 (predicted).
        """
        super(MetricSetCnnAttentionOnlyPRF1, self).__init__(thresh)
        self.metric_name_to_function[MetricNames.PRECISION_RECALL_F1] = self.precision_recall_f1
