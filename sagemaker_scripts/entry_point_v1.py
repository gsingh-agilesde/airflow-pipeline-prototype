CPT_CODE_SET = "CPT"
ICD10_CODE_SET = "ICD10"
EM_EST = "EM_EST"
EM_NEW = "EM_NEW"
CPT_LABEL_CODE_SETS = {CPT_CODE_SET, EM_EST, EM_NEW}

EMBEDDING_SIZE = 100
MAX_LENGTH = 2500

SAVED_MODEL_NAME = "/model.pth"
CNN = "conv_attn"
MULTI_VIEW_CNN = "multi_view_conv_attn"
CUDA = "cuda:"

__CODE = "code"
__FREQUENCY = "frequency"
__TEST_FREQ = "test frequency"
__RECALL = "recall"
__PRECISION = "precision"
__F1 = "f1"

__THRESH = 0.5


class OutputColumns:
    """
    Stores the column names for the output data frames or csv files that will be generated as part of data
    preprocessing.
    """
    NOTE_ID = "note_id"
    TEXT = "text"
    ICD_LABELS = "icd_labels"
    CPT_LABELS = "cpt_labels"


class SpecialtyNames:
    """
    Documents the names of the specialties used for the 300K clinical notes data set.
    """
    CARDIO = "cardio"
    ORTHO = "ortho"
    PCP = "pcp"
    ALL = "all"
    CARDIO_NO_EM = "cardio_no_em"
    ORTHO_NO_EM = "ortho_no_em"
    PCP_NO_EM = "pcp_no_em"
    ALL_NO_EM = "all_no_em"
    LOS_ALAMITOS = "los_alamitos"
    LOS_ALAMITOS_NO_CODES = "los_alamitos_no_codes"
    IR_RAW = "ir_raw"
    IR_NO_CODES = "ir_no_codes"
    MAMMOGRAPHY = "mammography"
    MRI = "mri"
    XRAY = "xray"
    CT_SCAN = "ct_scan"


class DefaultHyperparameters:
    SPECIALTY = SpecialtyNames.ORTHO_NO_EM
    CODE_SET = CPT_CODE_SET

    LEARNING_RATE = 0.001
    BATCH_SIZE = 32
    EARLY_STOPPING = True
    NUM_EPOCHS = 50
    TEXT_SORT = False
    KERNEL_SIZE = 4
    CONV_OUT_SIZE = 50
    DROPOUT = 0.5
    WEIGHT_DECAY = 0
    EPSILON_ES = 0
    PATIENCE_ES = 5


class SagemakerEnvironment:
    SM_MODEL_DIR = "SM_MODEL_DIR"
    SM_CHANNEL_TRAINING = "SM_CHANNEL_TRAINING"
    SM_NUM_GPUS = "SM_NUM_GPUS"


import sys
import subprocess as sb

sb.call([sys.executable, "-m", "pip", "install", "--upgrade", "pip"])
sb.call([sys.executable, "-m", "pip", "install", "sklearn"])
sb.call([sys.executable, "-m", "pip", "install", "torchtext==0.3.1"])

import os
import argparse
import logging
import numpy as np
import pandas as pd
from collections import ChainMap
import pickle
from tqdm import tqdm
import uuid
import itertools
import time
import json
from math import floor
from sklearn.metrics import recall_score, precision_score, f1_score

import torch
from torch.autograd import Variable
from torchtext.vocab import Vectors
from torchtext.data import Field, BucketIterator, Dataset, Example, Iterator
from torchtext.data import TabularDataset
from torch import nn
from torch.nn.init import xavier_uniform_ as xavier_uniform
import torch.nn.functional as F

# where you want to save any models you may train
MODEL_DIR = os.environ[SagemakerEnvironment.SM_MODEL_DIR] + "/"
# where you want to store or have stored the data.
DATA_DIR = os.environ[SagemakerEnvironment.SM_CHANNEL_TRAINING] + "/"


def from_pickle(file_path):
    """
    Read a pickle file using encoding as "bytes".
    :param file_path: path to input pickle file.
    :return: contents of the pickle file.
    """
    with open(file_path, "rb") as f:
        return pickle.load(f, encoding='bytes')


def to_pickle(file_path, data):
    """
    Store the input data to a pickle file at the specified file_path.
    :param file_path: path to output pickle file.
    :param data: data to be stored in the pickle file.
    :return: None
    """
    with open(file_path, "wb") as f:
        pickle.dump(data, f)


def to_json(file_path, data):
    """
    Stores the data to a json file at the specified file_path
    :param file_path: path to output json file.
    :param data: ata to be stored in the json file.
    :return: None
    """
    with open(file_path, 'w') as file:
        json.dump(data, file, indent=1)


################################################################################################
################################################################################################
################################################################################################

####### DATA LOADER python import.


class DataFields:
    """
    class for defining the Fields for the datasets required by the model.
    """


    def __init__(self, label_sep=";"):
        self.TEXT = Field(sequential=True, tokenize=lambda x: x.split(), lower=True)
        self.ID = Field(sequential=False)
        self.LABEL = Field(sequential=True, tokenize=lambda x: str(x).split(label_sep), unk_token=None, pad_token=None)


    def build_vocab_for_text(self, vocab_itr, vectors):
        """
        This method uses an iterator of words to build the vocab for TEXT field
        :param vocab_itr: Iterator containing the words
        :param vectors: word embeddings
        :return: builds the vocab for the TEXT field
        """
        vocab_list = [[word] for word in vocab_itr]
        self.TEXT.build_vocab(vocab_list, vectors=vectors)


    def build_vocab_for_label(self, vocab_itr):
        """
        This method uses an iterator of words to build the vocab for LABEL field
        :param vocab_itr: Iterator containing the words
        :return: builds the vocab for the LABEL field
        """
        vocab_list = [[word] for word in vocab_itr]
        self.LABEL.build_vocab(vocab_list)


    def build_vocab_for_id(self, vocab_itr):
        """
        This method uses an iterator of words to build the vocab for ID field
        :param vocab_itr: Iterator containing the words
        :return: builds the vocab for the ID field
        """
        vocab_list = [[word] for word in vocab_itr]
        self.ID.build_vocab(vocab_list)


class DataIterator:
    """
    This class creates the iterators for the train, test and validate splits.
    """

    __TRAIN = "train"
    __VALIDATE = "validate"
    __TEST = "test"


    def __init__(self, data_path, code_set, specialty=SpecialtyNames.CARDIO, label_sep=";",
                 build_data_generators=True):
        """
        Defines the fields for the splits and builds the vocab for the required fields.
        :param data_path: the path to the file containing the entire dataset.
        :param code_set: the code_set for which the set of labels should be returned. It supports ICD10 and CPT
        :param specialty: the speciality for which the model should be trained.
        :param label_sep: the separator used for storing the code labels
        :param build_data_generators: flag to specify if the data model should be able to
         build and generate batches from the dataset located at {data_path}

        Note:
        Only values in the class SpecialityNames are supported.
        """
        self.data_path = data_path
        self.code_set = code_set
        self.specialty = specialty
        self.label_sep = label_sep

        self.data_fields = DataFields()
        self.train_data_fields = [(OutputColumns.NOTE_ID, self.data_fields.ID),
                                  (OutputColumns.TEXT, self.data_fields.TEXT)]
        if code_set in CPT_LABEL_CODE_SETS:
            self.train_data_fields.append((OutputColumns.CPT_LABELS, self.data_fields.LABEL))
            self.label_column = OutputColumns.CPT_LABELS
        elif code_set == ICD10_CODE_SET:
            self.train_data_fields.append((OutputColumns.ICD_LABELS, self.data_fields.LABEL))
            self.label_column = OutputColumns.ICD_LABELS
        else:
            raise ValueError("code_set {} is not supported.".format(self.code_set))

        vocab_file = DATA_DIR + "{}_{}_vocab.pickle".format(self.specialty, self.code_set)
        vocab = from_pickle(vocab_file)

        vectors = Vectors(DATA_DIR + "{}_{}_processed_full.embed".format(self.specialty, self.code_set))

        codes = self.__get_codes_in_label()
        # build vocabs for the fields.
        self.data_fields.build_vocab_for_label(codes)
        self.data_fields.build_vocab_for_text(vocab, vectors)

        if build_data_generators:
            note_ids = self.__get_note_ids_in_dataset()
            self.data_fields.build_vocab_for_id(note_ids)

        self.num_words = len(self.data_fields.TEXT.vocab.itos)
        self.num_labels = len(self.data_fields.LABEL.vocab.itos)


    def __get_codes_in_label(self):
        """
        This method saves and returns the set of codes in the dataset to a pickle file
        If the file already exists, it reads it and returns the codes.
        """
        file_name = DATA_DIR + "{}_codes_in_{}_specialty.pickle".format(self.code_set, self.specialty)

        try:
            return from_pickle(file_name)
        except FileNotFoundError:
            df = pd.read_csv(self.data_path, dtype=object)
            df[self.label_column] = df[self.label_column].apply(lambda x: str(x).split(self.label_sep))
            codes = set(itertools.chain.from_iterable(df[self.label_column].values))
            to_pickle(file_name, codes)
        return codes


    def __get_note_ids_in_dataset(self):
        """
        This method returns the set of note_ids in the dataset.
        """
        df = pd.read_csv(self.data_path)
        note_ids = set(df[OutputColumns.NOTE_ID].values.tolist())
        return note_ids


    def get_train_validate_test_data(self, train_path):
        """
        This method loads and returns the training, validation and test data set in the appropriate format.
        :param train_path: Path to training data set. Since validation and test data sets are also assumed to live in
                           the same directory, we only need the path to training data set.
        :return: training, validation and test data sets.
        """

        temp = train_path.split(".")[0].split("_")
        temp[-1] = self.__VALIDATE
        validation_path = "_".join(temp) + ".csv"
        temp[-1] = self.__TEST
        test_path = "_".join(temp) + ".csv"

        t1 = time.time()
        trn, vld, test = TabularDataset.splits(
            path=DATA_DIR,
            train=train_path,
            validation=validation_path,
            test=test_path,
            format='csv',
            skip_header=True,
            fields=self.train_data_fields)
        print("created train and validate dataset in {} sec".format(str(time.time() - t1)))

        return trn, vld, test


    @staticmethod
    def get_train_validate_data_itr(batch_size, train_data, validate_data, sort=True):
        """
        This method returns the data iterators for the input training and validation data sets.
        :param batch_size: batch size.
        :param train_data: train data set.
        :param validate_data: validation data set.
        :param sort: sorts the input data by the length of the input text.
        :return: iterators to training and validation data sets.
        """
        t1 = time.time()
        train_iter, val_iter = BucketIterator.splits(
            (train_data, validate_data),
            sort=sort,  # If True, this sorts the dataset in ascending order. Required for efficient padding.
            batch_sizes=(batch_size, batch_size),
            sort_key=lambda x: len(x.text),
            sort_within_batch=False,
            repeat=False
        )
        print("created train and validate itr in {} sec".format(str(time.time() - t1)))
        return train_iter, val_iter


    def create_dataset_from_list_of_dict(self, data, note_id_field, content_field):
        """
        This method creates an object of type torchtext.data.Dataset from a list of dictionaries.
        :param data: list of dictionaries containing the note_id and content
        :param note_id_field: name of the note_id field
        :param content_field: name of the content field
        :return: Dataset object
        """

        t1 = time.time()
        note_ids = [z[note_id_field] for z in data]
        self.data_fields.build_vocab_for_id(note_ids)
        fields_list = [(note_id_field, self.data_fields.ID), (content_field, self.data_fields.TEXT)]
        fields = {note_id_field: (note_id_field, self.data_fields.ID),
                  content_field: (content_field, self.data_fields.TEXT)}
        examples = [Example.fromdict(val, fields) for val in data]
        test = Dataset(examples, fields_list)
        print("created test data set in {} sec".format(str(time.time() - t1)))
        return test


    @staticmethod
    def get_test_data_itr(test_data, x_field, sort=True, is_train=False, batch_size="default"):
        """
        This method returns the data iterator to the input test data set.
        :param test_data: test data set.
        :param x_field: x field column name str (features)
        :param sort: sorts the input data by the length of the input text.
        :param is_train: If True, then data is loaded for model training mode,
                         otherwise for model prediction mode.
        :param batch_size: batch size.
        :return: iterator to test data set.
        """
        return Iterator(test_data,
                        train=is_train,
                        sort=sort,  # sorts the dataset in ascending order. Required for efficient padding.
                        batch_size=len(test_data) if str(batch_size) == "default" else batch_size,
                        sort_key=lambda x: getattr(x, x_field),
                        sort_within_batch=False,
                        repeat=False
                        )


class BatchGenerator:
    """
    This class is a simple wrapper to make the batches easy to use.
    It takes in the names of the fields in the dataIterator and returns the batch with the given names.
    """


    def __init__(self, dl, id_field, x_field, y_field=None, num_labels=None):
        """
        :param dl: The iterator generated by the data loader
        :param id_field: the id filed in the dataset
        :param x_field: the data/text field in the dataset
        :param y_field: the label field in the dataset
        :param num_labels: number of labels in the dataset
        """
        self.dl = dl
        self.id_field = id_field
        self.x_field = x_field
        self.y_field = y_field
        self.num_labels = num_labels


    def __len__(self):
        return len(self.dl)


    def __iter__(self):
        """
        :return: an iterator of tuples containing the id, data and label
        """
        for batch in self.dl:
            id_ = getattr(batch, self.id_field)
            x = getattr(batch, self.x_field)
            x = x.t()
            x = Variable(torch.LongTensor(x))
            if self.y_field:
                y_ = getattr(batch, self.y_field)
                y_ = y_.t()
                y = torch.zeros(y_.shape[0], self.num_labels)
                for row, val in enumerate(y_):
                    for v in val.numpy():
                        if v != 1.0:
                            try:
                                y[row, v] = 1.0
                            except IndexError as ex:
                                print("Encountered an index exception: {}".format(ex))
            else:
                y = None
            yield (id_, x, y)


class DataLoader:

    def __init__(self, specialty, code_set, rest_api):
        """
        :param specialty: The selected specialty defined under cnn_attention/data_model/data_model class
                          SpecialtyNames.
        :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
        :param rest_api: True if predictions are to be obtained with a pre-trained model for the rest api endpoint.
        """
        self.specialty = specialty
        self.code_set = code_set
        self.data_iterator = DataIterator(
            DATA_DIR + "{}_notes_with_{}_feedback.csv".format(self.specialty, self.code_set), self.code_set,
            specialty=self.specialty, build_data_generators=not rest_api)
        self.label_col = self.__get_label_col()


    def __get_label_col(self):
        """
        Checks if code set is valid and returns the appropriate label column name str
        :return: label column name str
        """
        if self.code_set in CPT_LABEL_CODE_SETS:
            label_col = OutputColumns.CPT_LABELS
        elif self.code_set == ICD10_CODE_SET:
            label_col = OutputColumns.ICD_LABELS
        else:
            raise ValueError("{} code set is not defined. Choose a valid code set.".format(self.code_set))
        return label_col


    def load_datasets(self):
        """
        :return: datasets for training, validation and testing. These are of type torchtext.data.TabularDataset
        """
        train_data, validate_data, test_data = self.data_iterator.get_train_validate_test_data(
            train_path=DATA_DIR + "{}_{}_train.csv".format(self.specialty, self.code_set))
        return train_data, validate_data, test_data


    def get_train_validate_batch_generators(self, batch_size, train_data, validate_data, sort=True):
        """
        Encapsulates the training and validation datasets in batches using the BatchGenerator class. This is
        required for batching of data. Here the batch_size and sort serve as two hyperparameters.
        :param batch_size: batch size.
        :param train_data: train data set.
        :param validate_data: validation data set.
        :param sort: sorts the input data by the length of the input text.
        :return: batch generators for input train and validation datasets
        """
        train_itr, validate_itr = self.data_iterator.get_train_validate_data_itr(batch_size, train_data,
                                                                                 validate_data, sort=sort)

        train_batches_gen = BatchGenerator(train_itr,
                                           OutputColumns.NOTE_ID,
                                           OutputColumns.TEXT,
                                           y_field=self.label_col,
                                           num_labels=self.data_iterator.num_labels)
        validate_batches_gen = BatchGenerator(validate_itr,
                                              OutputColumns.NOTE_ID,
                                              OutputColumns.TEXT,
                                              y_field=self.label_col,
                                              num_labels=self.data_iterator.num_labels)

        return train_batches_gen, validate_batches_gen


    def get_test_batch_generator(self, test_data, id_field, x_field, y_field=None, sort=True, is_train=False,
                                 batch_size="default"):
        """
        Encapsulates the test dataset using the BatchGenerator class. This is designed slightly different since it will
        be used by the model_prediction.py module which is called by external projects. Here we accept additional inputs
        as id_field and x_field so that the external projects have control over the naming conventions for the id column
        and x_field column names.
        :param test_data: test data set
        :param id_field: id column name str
        :param x_field: x field column name str (features)
        :param y_field: optional y field that represents the output labels. Default value is None. Only during model
                        training mode, this parameter would have a value other than None.
        :param sort: sorts the input data by the length of the input text.
        :param is_train: If True, then data is loaded for model training mode,
                         otherwise for model prediction mode.
        :param batch_size: batch size.
        :return: Batch generator for input test data set.
        """
        test_itr = self.data_iterator.get_test_data_itr(test_data, x_field, sort=sort, is_train=is_train,
                                                        batch_size=batch_size)
        test_batches_gen = BatchGenerator(test_itr, id_field, x_field, y_field,
                                          num_labels=self.data_iterator.num_labels)
        return test_batches_gen


    def get_test_batch_generator_from_list_dict(self, data, note_id_field, content_field):
        """
                This method loads data from a list of dictionaries into batches of specified size.
                :param data: list of dictionaries containing the note_id and content
                :param note_id_field: name of the note_id field
                :param content_field: name of the content field (features)
                :return: data iterator for the data set
                """
        t1 = time.time()
        test_data = self.data_iterator.create_dataset_from_list_of_dict(data, note_id_field, content_field)
        test_batches_gen = self.get_test_batch_generator(test_data, note_id_field, content_field)
        print("created test data generator in {} sec".format(str(time.time() - t1)))
        return test_batches_gen


################################################################################################
################################################################################################
################################################################################################


################################################################################################
################################################################################################
################################################################################################

##### STRATIFIED K-FOLD VALIDATION

from collections import defaultdict
from itertools import chain
from sklearn.model_selection import KFold


class StratifiedKFold:

    def __init__(self, n_splits=5):
        """
        :param n_splits: int value that indicates the number of folds in cross validation.
                         This must be at least 2.
        """
        if n_splits < 2:
            raise ValueError("n_splits must be at least equal to 2.")
        self.n_splits = n_splits


    def split(self, y=None):
        """
        :param y: numpy array-like such that every element of the array is hashable.
                  This represents the labels array that contains data from both training and validation datasets. Since
                  stratification is required to be done by label, we do not consider the features, X array and only
                  provide splits of indices on labels array y. The same indices must be used to obtain corresponding
                  features X.
        :return: yields the requisite indices for training and validation datasets
        """
        # 0. For every unique code label compute the indices corresponding to the examples with the given code label in
        #    a dictionary with key=code label and value=list of indices. This is why we need all code labels to be
        #    hashable.
        unique_y_to_indices = defaultdict(list)
        for i, label in enumerate(y):
            unique_y_to_indices[label].append(i)

        # 1. For every unique code label check if number of examples are less than self.n_splits.
        #    If number of samples are less than self.n_splits, all the examples corresponding to the code label must be
        #    in the training dataset for all folds.
        common_train_indices = []
        labels = list(unique_y_to_indices.keys())
        for label in labels:
            if len(unique_y_to_indices[label]) < self.n_splits:
                common_train_indices.append(unique_y_to_indices[label])
                del unique_y_to_indices[label]

        common_train_indices = list(chain.from_iterable(common_train_indices))

        # 2. For every unique code label perform the usual kfold splits from sklearn to obtain one generator object per
        #    code.
        kf_generators = dict()
        for label in unique_y_to_indices:
            kf_generators[label] = KFold(n_splits=self.n_splits).split(unique_y_to_indices[label])

        # 3. Finally for all generator objects, make one iteration click and collect all indices for train and validate
        #    separately and perform the yield.
        #    Note: Each code label's generator object may exhaust at different times. Need to handle this issue.
        for i in range(self.n_splits):
            train_indices, validate_indices = [], []
            for label in unique_y_to_indices:
                train_, validate_ = next(kf_generators[label])
                train_ind = [unique_y_to_indices[label][i] for i in train_]
                validate_ind = [unique_y_to_indices[label][i] for i in validate_]
                train_indices.append(train_ind)
                validate_indices.append(validate_ind)
            train_indices.append(common_train_indices)
            train_indices = list(chain.from_iterable(train_indices))
            validate_indices = list(chain.from_iterable(validate_indices))
            yield train_indices, validate_indices


################################################################################################
################################################################################################
################################################################################################

#### METRIC COMPUTATION

def get_set_code_labels(specialty, code_set):
    """
    Given the specialty and code set compute the unique set of code labels.
    :param specialty: The selected specialty defined under cnn_attention_sagemaker/data_model/data_model class
                      SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention_sagemaker project.
    :return: set of unique code labels.
    """
    file_name = DATA_DIR + "{}_codes_in_{}_specialty.pickle".format(code_set, specialty)
    set_code_labels = from_pickle(file_name)
    set_code_labels.add('<unk>')
    set_code_labels.add('<pad>')
    return set_code_labels


def get_code_to_index(set_code_labels):
    """
    Given the set of unique code labels create a mapping between index and label. This is useful for creating the
    binary arrays y_true, y_pred and y_raw.
    :param set_code_labels: the set of code labels for the given dataset.
    :return:
    """
    code_to_index = dict()
    z = 0
    for code in set_code_labels:
        code_to_index[code] = z
        z += 1
    return code_to_index


def get_consolidated_metrics(code_to_recall, code_to_precision, code_to_f1, code_to_test_freq):
    """
    This function converts the input result dictionaries of various metrics into one consolidated pandas data frame.
    :param code_to_recall: dictionary with key as code label and value as recall score
    :param code_to_precision: dictionary with key as code label and value as precision score
    :param code_to_f1: dictionary with key as code label and value as f1 score
    :param code_to_test_freq: dictionary with key as code label and value as frequency of label in test/validation
                              dataset.
    :return: consolidated resulting pandas data frame
    """
    code_f1_df = pd.DataFrame(list(code_to_f1.items()), columns=[__CODE, __F1])
    code_recall_df = pd.DataFrame(list(code_to_recall.items()), columns=[__CODE, __RECALL])
    code_precision_df = pd.DataFrame(list(code_to_precision.items()), columns=[__CODE, __PRECISION])
    code_test_freq_df = pd.DataFrame(list(code_to_test_freq.items()), columns=[__CODE, __TEST_FREQ])
    result_df = pd.merge(code_f1_df, code_precision_df, on=__CODE)
    result_df = pd.merge(code_recall_df, result_df, on=__CODE)
    result_df = pd.merge(code_test_freq_df, result_df, on=__CODE)
    result_df.sort_values(by=__F1, inplace=True, ascending=False)
    return result_df


def get_y_true_y_pred_y_raw_v2(note_ids, set_code_labels, note_id_to_list_labels, note_id_to_code_to_score,
                               code_to_index, thresh=0.5):
    """
    Given raw confidence scores and ground truth predictions for every code label and every note, compute the requisite
    result arrays mentioned in the return.
    :param note_ids: all the note ids for the given dataset.
    :param set_code_labels: the set of code labels for the given dataset.
    :param note_id_to_list_labels: dictionary with key as note id and value as a list of code labels in ground truth.
    :param note_id_to_code_to_score: dictionary with key as note id and value as a dictionary with key as code and
                                     value as predicted confidence score.
    :param code_to_index: dictionary with key as code label and value as an index the code label is mapped to
    :param thresh: threshold for defining whether a code label is considered as predicted or not predicted.
    :return: Three binary 2d array each with row denoting a clinical note id and column denoting a code label such that
             a value of 1 indicates that the code label is in the ground truth (y_true), or is a prediction by the
             learning algorithm (y_pred) or is a confidence score by the learning algorithm (y_raw).
    """
    y_true = np.zeros((len(note_ids), len(set_code_labels)), dtype=float)
    y_pred = np.zeros((len(note_ids), len(set_code_labels)), dtype=float)
    y_raw = np.zeros((len(note_ids), len(set_code_labels)), dtype=float)

    t0 = time.time()
    # Set y_true and y_pred
    for i, note_id in enumerate(note_ids):
        if (i + 1) % 1000 == 0:
            print("index = {}, time elapsed = {}".format(i + 1, time.time() - t0))
            t0 = time.time()

        if note_id in note_id_to_list_labels:

            code_to_score = note_id_to_code_to_score[note_id]
            ground_truth_labels = note_id_to_list_labels[note_id]

            for code in ground_truth_labels:
                y_true[i, code_to_index[code]] = 1

            for code in code_to_score:
                raw_score = code_to_score[code]
                y_pred[i, code_to_index[code]] = 1 if raw_score >= thresh else 0
                y_raw[i, code_to_index[code]] = raw_score
        else:
            raise ValueError("Note id value not in ground truth.")
    return y_true, y_pred, y_raw


def get_code_to_metrics_given_scores(set_code_labels, code_to_index, y_true, y_pred):
    """
    Given the ground truth and predictions for every code label and for every note, compute precision, recall, f1 and
    test frequency per code label.
    :param set_code_labels: the set of code labels for the given dataset.
    :param code_to_index: dictionary with key as code label and value as an index the code label is mapped to
    :param y_true: Binary 2d array with row denoting a clinical note id and column denoting a code label such that a
                   value of 1 indicates that the code label is in the ground truth for the corresponding note.
    :param y_pred: Binary 2d array with row denoting a clinical note id and column denoting a code label such that a
                   value of 1 indicates that the code label is predicted by the learning algorithm for the corresponding
                   note.
    :return: Four dictionaries with key as code label and value as precision, recall, f1, test dataset frequency
             respectively.
    """
    code_to_recall = dict()
    code_to_precision = dict()
    code_to_f1 = dict()
    code_to_test_freq = dict()
    for code in set_code_labels:
        index = code_to_index[code]
        code_to_recall[code] = recall_score(y_true=y_true[:, index], y_pred=y_pred[:, index])
        code_to_precision[code] = precision_score(y_true=y_true[:, index], y_pred=y_pred[:, index])
        code_to_f1[code] = f1_score(y_true=y_true[:, index], y_pred=y_pred[:, index])
        code_to_test_freq[code] = int(sum(y_true[:, index]))

    return code_to_recall, code_to_precision, code_to_f1, code_to_test_freq


def compute_metrics(specialty, code_set, note_id_to_list_labels, note_id_to_code_to_score, thresh=0.5):
    """
    In version 2 of this function, we accept more flexible inputs namely the ground truth data in the form of
    note_id_to_list_labels and predictions in the form of note_id_to_code_to_score. Additionally, in the returned
    data frame we do not provide overall frequency of code label since it can be inferred from the included frequency
    of code labelin the test/validation dataset.
    :param specialty: The selected specialty defined under cnn_attention_sagemaker/data_model/data_model class
                      SpecialtyNames.
    :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention_sagemaker project.
    :param note_id_to_list_labels: dictionary with key as note id and value as a list of code labels in ground truth.
    :param note_id_to_code_to_score: dictionary with key as note id and value as a dictionary with key as code and
                                     value as predicted confidence score.
    :param thresh: threshold for defining whether a code label is considered as predicted or not predicted.
    :return: result data frame with columns as code label, test frequency, recall, precision, f1.
    """
    set_code_labels = get_set_code_labels(specialty, code_set)
    code_to_index = get_code_to_index(set_code_labels)
    note_ids = note_id_to_code_to_score.keys()
    y_true, y_pred, y_raw = get_y_true_y_pred_y_raw_v2(note_ids, set_code_labels, note_id_to_list_labels,
                                                       note_id_to_code_to_score,
                                                       code_to_index, thresh)
    code_to_recall, code_to_precision, code_to_f1, code_to_test_freq = get_code_to_metrics_given_scores(set_code_labels,
                                                                                                        code_to_index,
                                                                                                        y_true, y_pred)

    result_df = get_consolidated_metrics(code_to_recall, code_to_precision, code_to_f1, code_to_test_freq)

    return result_df


################################################################################################
################################################################################################
################################################################################################


################################################################################################
################################################################################################
################################################################################################


##### MODEL


class BaseModel(nn.Module):

    def __init__(self, num_labels, embedding_vectors, embed_size, dropout):
        super(BaseModel, self).__init__()
        torch.manual_seed(1337)
        self.num_labels = num_labels
        self.embed_size = embed_size
        self.embed_drop = nn.Dropout(p=dropout)

        # make embedding layer
        self.embed = nn.Embedding(embedding_vectors.size()[0], embed_size, padding_idx=1)
        self.embed.weight.data.copy_(embedding_vectors)
        # self.embed.weight.requires_grad = False


    @staticmethod
    def _get_loss(y_hat, target):
        # calculate the BCE
        loss = F.binary_cross_entropy(y_hat, target)
        return loss


class ConvAttnPool(BaseModel):
    """
    Mullenbach et al., Explainable Prediction of Medical Codes from Clinical Text, Association for Computational
    Linguistics, N18-1100, 1101-1111, 2018.
    """


    def __init__(self, num_labels, kernel_size, conv_out_size, embedding_vectors, embed_size, dropout):
        super(ConvAttnPool, self).__init__(num_labels, embedding_vectors, embed_size, dropout)

        # initialize conv layer as in 2.1
        self.conv = nn.Conv1d(self.embed_size, conv_out_size, kernel_size=kernel_size, padding=floor(kernel_size / 2))
        xavier_uniform(self.conv.weight)

        # context vectors for computing attention as in 2.2
        self.U = nn.Linear(conv_out_size, num_labels)
        xavier_uniform(self.U.weight)

        # final layer: create a matrix to use for the L binary classifiers as in 2.3
        self.final = nn.Linear(conv_out_size, num_labels)
        # TODO: make this into a hyper-parmeter
        xavier_uniform(self.final.weight)


    def forward(self, x, target, desc_data=None, get_attention=True):
        yhat, alpha = self.predict(x)
        loss = self._get_loss(yhat, target)
        return yhat, loss, alpha


    def predict(self, x):
        """
        This mothod is used to make the predictions for the test data.
        :param x: the data point for which the prediction is to be made
        :return: prediction score and word weights for attention.
        """
        # get embeddings and apply dropout
        x = self.embed(x)
        x = self.embed_drop(x)
        x = x.transpose(1, 2)

        # apply convolution and nonlinearity (tanh)
        H = torch.tanh(self.conv(x).transpose(1, 2))
        # apply attention
        alpha = F.softmax(self.U.weight.matmul(H.transpose(1, 2)), dim=2)
        # document representations are weighted sums using the attention. Can compute all at once as a matmul
        m = alpha.matmul(H)
        # final layer classification
        y = self.final.weight.mul(m).sum(dim=2).add(self.final.bias)

        # final sigmoid to get predictions
        yhat = torch.sigmoid(y)
        return yhat, alpha


class MultiviewConvAttnPool(BaseModel):
    """
    N. Sadoughi et al., Medical code prediction with multi-view convolution and
    description-regularized label-dependent attention, arXiv: 1811.01468, (2018).

    "Presently" (at the time of this writing), the paper has not been published.
    """


    def __init__(self, num_labels, kernel_size, conv_out_size, embedding_vectors, embed_size, dropout,
                 label_vocab_itos=None, code_to_desc=None, is_desc_reg=False, reg_lambda=0.0001):
        """
        :param num_labels: Number of output labels.
        :param kernel_size: Biggest kernel size. Other 3 kernels are computed with offsets as -2, -4 and -6.
        :param conv_out_size: Size of each convolutional output.
        :param embedding_vectors: Input embedding to initialize the embedding matrix. This is done in the super call.
        :param embed_size: Size of embedding dimension.
        :param dropout: Amount of dropout. Technically, Multiview paper does not specify a dropout in any layer.
        :param label_vocab_itos: index to string for LABEL field.
        :param code_to_desc: dictionary with key as as code and value as preprocessed description.
        :param is_desc_reg: if True indicates that we want to include description regularization.
        :param reg_lambda: hyperparameter that weights amount of description regularization.
        """

        super(MultiviewConvAttnPool, self).__init__(num_labels, embedding_vectors, embed_size, dropout)
        self.kernel_size = kernel_size
        self.conv_out_size = conv_out_size
        self.label_vocab_itos = label_vocab_itos
        self.code_to_desc = code_to_desc
        self.is_desc_reg = is_desc_reg
        self.reg_lambda = reg_lambda

        ################################################################################
        ################################# MULTIVIEW CNN ################################
        ################################################################################
        # Define 4 convolution layers for multiview cnn as in section 4.2 of paper.

        self.conv = []
        self.conv.append(nn.Conv1d(in_channels=self.embed_size, out_channels=conv_out_size, kernel_size=kernel_size,
                                   padding=floor(kernel_size / 2)))
        xavier_uniform(self.conv[-1].weight)
        self.conv.append(nn.Conv1d(in_channels=self.embed_size, out_channels=conv_out_size, kernel_size=kernel_size - 2,
                                   padding=floor((kernel_size - 2) / 2)))
        xavier_uniform(self.conv[-1].weight)
        self.conv.append(nn.Conv1d(in_channels=self.embed_size, out_channels=conv_out_size, kernel_size=kernel_size - 4,
                                   padding=floor((kernel_size - 4) / 2)))
        xavier_uniform(self.conv[-1].weight)
        self.conv.append(nn.Conv1d(in_channels=self.embed_size, out_channels=conv_out_size, kernel_size=kernel_size - 6,
                                   padding=floor((kernel_size - 6) / 2)))
        xavier_uniform(self.conv[-1].weight)

        # context vectors for computing attention as in section 4.3 of paper.
        self.V = nn.Linear(conv_out_size, num_labels)
        xavier_uniform(self.V.weight)

        # Parameters for length-embedding function.
        self.K = nn.Linear(1, num_labels)
        xavier_uniform(self.K.weight)

        # final layer: create a matrix to use for the L binary classifiers as in 2.3
        self.U = nn.Linear(conv_out_size, num_labels)
        # TODO: make this into a hyper-parmeter
        xavier_uniform(self.U.weight)
        ################################################################################
        ################################################################################
        ################################################################################

        ################################################################################
        ############################### DESC REGULARIZATION ############################
        ################################################################################

        self.conv_dr = nn.Conv1d(in_channels=self.embed_size, out_channels=conv_out_size, kernel_size=kernel_size,
                                 padding=floor(kernel_size / 2))
        self.max_pool_dr = nn.MaxPool1d(kernel_size=kernel_size - 6, stride=1)
        # self.final_dr = nn.Linear(in_features=, out_features=1)


    def forward(self, x, target):
        yhat, alpha = self.predict(x)
        temp_loss = self._get_loss(yhat, target)
        # TODO: Description regularization.
        if self.is_desc_reg and self.label_vocab_itos:
            # loss = temp_loss +
            pass
        else:
            loss = temp_loss

        return yhat, loss, alpha


    def f_of_D(self, target):
        # 1. Based on target, extract the code labels.
        # 2. For all code labels j={1,...,L} extract the preprocessed long descriptions, D_j.
        # 3. Use the same self.embed layer to obtain embeddings for words in long descriptions.
        for val in target:
            for code_ind in chain.from_iterable(val.nonzero().tolist()):
                code = self.label_vocab_itos[code_ind]
                code_desc = self.code_to_desc[code]

        # Embedding layer, same embedding layer

        # Convolutional layer with kernel size as largest kernel size from the MCNN network and with same no. of filters
        # as that of the MCNN network.
        # Spatial Max pooling layer.
        # Dense Layer
        # Sigmoid Layer.


    def predict(self, x):
        # get embeddings
        x_length = x.shape[1] - (x == 1).sum(dim=1)
        x = self.embed(x)
        x = x.transpose(1, 2)

        # -- Multiview CNN defined per section 4.2, Eq. (1)

        if self.kernel_size % 2 == 0:
            add_factor = 1
        else:
            add_factor = 0

        chat = torch.zeros((len(self.conv), x.shape[0], x.shape[2] + add_factor, self.conv_out_size))
        for i in range(len(self.conv)):
            chat[i, :, :, :] = self.conv[i](x).transpose(1, 2)
        # ------ View pooling.
        C = torch.tanh(torch.max(chat, dim=0)[0])

        # Self-attention computed per section 4.3, Eq. (2)
        # -----attention weights
        alpha = C.matmul(self.V.weight.t())
        # ----- attention weighted output.
        P = C.transpose(1, 2).matmul(alpha)

        # Length embedding function defined per section 4.4, Eq. (3).
        T = self.K(x_length.float().reshape(-1, 1))
        # Final layer defined per section 4.4, Eq. (4)
        # Following is without length embedding (For now!)
        y = self.U.weight.mul(P.transpose(1, 2)).sum(dim=2).add(self.U.bias).add(T)
        yhat = torch.sigmoid(y)

        return yhat, alpha


def pick_model(model_name, num_labels, kernel_size, conv_out_size, embedding_vectors, embed_size=100, dropout=0.5,
               model_path=None, label_vocab_itos=None, code_to_desc=None):
    """
    This method selects and initiates a model according to the params of the function.

    NOTE: When a pre-trained model is loaded using the param {model_path},
    the state dictionaries(parameters) of the pre-trained model and model skeleton initiated here should be the same.

    :param model_name: The name of the model to be initialized.
    :param num_labels: number of labels in the dataset
    :param kernel_size: convolutional filter size
    :param conv_out_size: length of the output of the conv1D layer
    :param embedding_vectors: embedding vectors
    :param embed_size: dimension of each embedding vector
    :param dropout: dropout at the embedding layer
    :param model_path: If pre-trained model needs to be loaded then provide the path to the saved model here.
    :param label_vocab_itos: index to string for LABEL field. (Used only in Multi-view model.)
    :param code_to_desc: dictionary with key as as code and value as preprocessed description. (Used only in Multi-view
                         model.)
    :return: initialized model with the given param.
    """
    if model_name == CNN:
        model = ConvAttnPool(num_labels, kernel_size, conv_out_size, embedding_vectors, embed_size, dropout)
    elif model_name == MULTI_VIEW_CNN:
        model = MultiviewConvAttnPool(num_labels, kernel_size, conv_out_size, embedding_vectors, embed_size, dropout,
                                      label_vocab_itos=label_vocab_itos, code_to_desc=code_to_desc)
    else:
        raise ValueError("{} model is not defined. Choose a valid model.".format(str(model_name)))
    if model_path:
        sd = torch.load(model_path + SAVED_MODEL_NAME)
        model.load_state_dict(sd)
    return model


################################################################################################
################################################################################################
################################################################################################


################################################################################################
################################################################################################
################################################################################################

### MODEL UTILS.


__OPTIM_EXP_AVG = 'exp_avg'
__OPTIM_EXP_AVG_SQ = 'exp_avg_sq'
__CPU_DEVICE = "cpu"
__STOP_WORDS = ["i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", "yourself",
                "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "itself",
                "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that",
                "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had",
                "having", "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as",
                "until", "while", "of", "at", "by", "for", "with", "about", "against", "between", "into", "through",
                "during", "before", "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off",
                "over", "under", "again", "further", "then", "once", "here", "there", "when", "where", "why", "how",
                "all", "any", "both", "each", "few", "more", "most", "other", "some", "such", "no", "nor", "not",
                "only", "own", "same", "so", "than", "too", "very", "s", "t", "can", "will", "just", "don", "should",
                "now"]


def load_model(model_dir, kernel_size, conv_out_size, model_name, cuda_device_number, dropout, data_iterator,
               label_vocab_itos=None, code_to_desc=None):
    """
    Initiates the model. If {model_dir} is provided then loads the pre-trained model from that directory
    :param model_dir: if loading a saved pre-trained data, then provide the path to saved model directory, else
                          None
    :param kernel_size: convolutional filter size
    :param conv_out_size: length of the output of the conv1D layer
    :param model_name: model to be trained. Currently, only cnn_attention.CNN is supported
    :param cuda_device_number: the cuda device number on which the model should be run.
    :param dropout: The dropout rate, presently only used for embedding layer.
    :param data_iterator: the data iterator object for input data.
    :param label_vocab_itos: index to string for LABEL field. (Used only in Multi-view model.)
    :param code_to_desc: dictionary with key as as code and value as preprocessed description. (Used only in Multi-view
                         model.)
    :return: device and model
    """
    # Device configuration
    device = torch.device(
        CUDA + cuda_device_number if torch.cuda.is_available() else __CPU_DEVICE)
    embedding_vectors = data_iterator.data_fields.TEXT.vocab.vectors
    model = pick_model(model_name, data_iterator.num_labels, kernel_size, conv_out_size, embedding_vectors,
                       model_path=model_dir, dropout=dropout, label_vocab_itos=label_vocab_itos,
                       code_to_desc=code_to_desc).to(device)
    return device, model


def get_code_scores(num_of_codes, output_scores, data_iterator):
    """
    This method selects the top {num_of_codes} based on the prediction scores.
    :param num_of_codes: top number of codes based on the prediction score.
    :param output_scores: output score array containing the scores of all the code labels.
    :param data_iterator: the data iterator object for input data.
    :return: returns the dictionary containing the scores of top {num_of_codes} codes with highest scores
    """
    sorted_out_idx = np.argsort(output_scores)
    out_score = dict()
    for idx, code_idx in enumerate(sorted_out_idx[-num_of_codes:]):
        val = output_scores[code_idx]
        out_score[data_iterator.data_fields.LABEL.vocab.itos[code_idx]] = val
    return out_score


def get_code_to_attention(data_iterator, kernel_size, code_idxs, top_n_attentions, attn_weights, note_data,
                          is_mention=False):
    """
    1. Find the attention weight for the code by querying {attn_weights}.
    2. Get the indices for the top n weighted attentions.
    3. Get the word id from the note_data for each of the top n indices and its next three indices from note_data
    4. Get the corresponding word from the TEXT vocab for each of the word ids.

    :param data_iterator: the data iterator object for input data.
    :param kernel_size: convolutional filter size
    :param code_idxs: indices to code labels
    :param top_n_attentions: The size of the attention for each label. The total size of the attention will be
                                top_n_attention*kernel_size of the convolution layer.
    :param attn_weights: attention weights for a single clinical note.
    :param note_data: text of single clinical note.
    :param is_mention: denotes if we want to run the function in the mentions mode.
                       If the is_mention is set to True, we obtain attention weights dictionary
                       with key as a word index in note and value as the corresponding attention weight.
                       Else we obtain attention weights dictionary with key as a word (not word index)
                       in the note and value as the corresponding attention weight.
    :return : code to the top n attention words with their attention weights dictionary
    """
    code_to_attention = dict()
    for code_idx in code_idxs:
        code = data_iterator.data_fields.LABEL.vocab.itos[code_idx]
        attn_for_code = attn_weights[code_idx]
        attn_for_code_sorted_indices = np.argsort(attn_for_code)[::-1]
        word_weight = dict()
        for word_idx in attn_for_code_sorted_indices:
            if len(word_weight) >= top_n_attentions * kernel_size:
                break
            for itr in range(word_idx, word_idx + kernel_size):
                if itr < attn_for_code.size - 1:
                    word_id = note_data[itr].item()
                    word = data_iterator.data_fields.TEXT.vocab.itos[word_id]
                    if word not in __STOP_WORDS and word != "<pad>":
                        word_id = note_data[itr].item()
                        word = data_iterator.data_fields.TEXT.vocab.itos[word_id]
                        if word not in word_weight.keys():
                            if is_mention:
                                word_weight[itr] = attn_for_code[word_idx]
                            else:
                                word_weight[word] = attn_for_code[word_idx]
        code_to_attention[code] = word_weight
    return code_to_attention


################################################################################################
################################################################################################
################################################################################################


################################################################################################
################################################################################################
################################################################################################

#### MODEL TRAINING


class ModelTraining:
    __OPTIM_EXP_AVG = 'exp_avg'
    __OPTIM_EXP_AVG_SQ = 'exp_avg_sq'
    __CPU_DEVICE = "cpu"

    """
    Files required for training a model or test batch of notes with a pre-trained model : 
        1. {specialty}_notes_with_{code_set}_feedback.csv
        2. {specialty}_{code_set}_train.csv
        3. {specialty}_{code_set}_validate.csv
        4. {specialty}_{code_set}_test.csv
        5. {specialty}_{code_set}_vocab.pickle
        6. {specialty}_{code_set}_processed_full.embed
        7. {specialty}_{code_set}_train_note_ids.pickle
        8. {specialty}_{code_set}_validate_note_ids.pickle
        9. {specialty}_{code_set}_test_note_ids.pickle 
    Files required for loading a pre-trained model and getting the predictions for a SINGLE note : 
        1. {specialty}_{code_set}_vocab.pickle
        2. {specialty}_{code_set}_processed_full.embed   
        6. {code_set}_codes_in_{specialty}_specialty.pickle  
    """


    def __init__(self, model_dir, specialty, code_set,
                 kernel_size, conv_out_size, model_name, cuda_device_number,
                 epochs=100, batch_size=32, weight_decay=0, learning_rate=0.00025, early_stopping=False,
                 epsilon=0, patience=0, dropout=0.5, data_iterator=None):
        """
        This class initializes and loads the data and model for training and testing.

        :param model_dir: if loading a saved pre-trained data, then provide the path to saved model directory, else
                          None
        :param specialty: The selected specialty defined under cnn_attention/data_model/data_model class
                          SpecialtyNames.
        :param code_set: The selected code set "CPT" or "ICD10" defined in __init__ for cnn_attention project.
        :param kernel_size: convolutional filter size
        :param conv_out_size: length of the output of the conv1D layer
        :param model_name: model to be trained. Currently, only cnn_attention.CNN is supported
        :param cuda_device_number: the cuda device number on which the model should be run.

        :param epochs: number of epochs to train the model for.
        :param batch_size: batch size
        :param weight_decay: weight decay
        :param learning_rate: learning rate
        :param early_stopping: True if early stopping is desired.
        :param epsilon: The amount by which the loss should have changed.
        :param patience: The number of epochs for which the loss has not changed.
        :param dropout: The dropout rate, presently only used for embedding layer.
        :param data_iterator: the data iterator object for input data.
        """
        self.model_dir = model_dir
        self.specialty = specialty
        self.code_set = code_set
        self.kernel_size = kernel_size
        self.conv_out_size = conv_out_size
        self.model_name = model_name
        self.cuda_device_number = cuda_device_number
        self.epochs = epochs
        self.batch_size = batch_size
        self.weight_decay = weight_decay
        self.learning_rate = learning_rate
        self.early_stopping = early_stopping
        self.patience = patience
        self.epsilon = epsilon
        self.dropout = dropout
        self.data_iterator = data_iterator

        self.device, self.model = load_model(self.model_dir, self.kernel_size, self.conv_out_size, self.model_name,
                                             self.cuda_device_number, self.dropout, self.data_iterator)
        if not self.model_dir:
            self.optimizer = torch.optim.Adam(self.model.parameters(),
                                              weight_decay=self.weight_decay,
                                              lr=self.learning_rate)


    def train_model(self, train_batches_gen, validate_batches_gen):
        """
        Trains the model and calculates the train loss, validation loss .
        Saves the model at the {model_dir} directory.
        :param train_batches_gen: data_processing.data_loader.BatchGenerator object for training dataset
        :param validate_batches_gen: data_processing.data_loader.BatchGenerator object for validation dataset
        :return: loss_each_epoch_train, loss_each_epoch_val, model_dir and val_note_id_to_scores
        """
        loss_each_epoch_train = []
        loss_each_epoch_val = []
        val_note_id_to_score = None
        for epoch in range(1, self.epochs + 1):
            print("Epoch: {}".format(str(epoch)))
            train_loss = self.__train(train_batches_gen)
            loss_each_epoch_train.append(train_loss)

            val_loss, temp_val_note_id_to_score = self.__test(validate_batches_gen)
            loss_each_epoch_val.append(val_loss)
            val_note_id_to_score = temp_val_note_id_to_score
            if self.early_stopping and self.early_stopping_criteria(loss_each_epoch_val):
                # stop training, do tests on test and train sets, and then stop the script
                print("loss hasn't improved in {} epochs, early stopping.".format(self.patience))
                break
        # Use a random id using python library uuid to avoid collision of folder names. This happens mainly when
        # performing micro-modeling for organization-based models
        model_dir_ = MODEL_DIR + self.model_name + "_{}".format(self.specialty) + "_{}".format(
            self.code_set) + time.strftime('_%b_%d_%H_%M_%S', time.localtime()) + "_ID_" + str(uuid.uuid4())
        os.mkdir(model_dir_)
        self.model_dir = model_dir_
        self.save_model_params()
        self.save_model()
        print("Model trained and saved at location : {}".format(self.model_dir))
        return loss_each_epoch_train, loss_each_epoch_val, self.model_dir, val_note_id_to_score


    def __train(self, train_batches_gen):
        """
        trains the model for 1 epoch.
        :param train_batches_gen: data_processing.data_loader.BatchGenerator object for training dataset
        :return: train and validation loss for the epoch
        """
        losses = []
        self.model.train()
        prev_device = self.device
        for note_ids, data, target in tqdm(train_batches_gen):
            data = data.to(self.device)
            target = target.to(self.device)
            self.optimizer.zero_grad()
            try:
                output, loss, _ = self.model(data, target)
                del output, _
                torch.cuda.empty_cache()
                loss.backward()
                temp_loss = loss
            except RuntimeError:

                # Leaving this commented code here that can be used for checking the memory usage while running epochs
                # result = subprocess.check_output(
                #     [
                #         'nvidia-smi', '--query-gpu=memory.free',
                #         '--format=csv,nounits,noheader'
                #     ], encoding='utf-8')
                # print("memory in free before empty cache: " + result.strip().split('\n')[0])

                # PUT STUFF FROM GPU TO CPU: model, optimizer, data, target
                print("Offloading computation to {}".format(str(ModelTraining.__CPU_DEVICE)))
                self.model = self.model.to(ModelTraining.__CPU_DEVICE)
                for d in list(self.optimizer.state.values()):
                    d[ModelTraining.__OPTIM_EXP_AVG] = d[ModelTraining.__OPTIM_EXP_AVG].to(
                        ModelTraining.__CPU_DEVICE)
                    d[ModelTraining.__OPTIM_EXP_AVG_SQ] = d[ModelTraining.__OPTIM_EXP_AVG_SQ].to(
                        ModelTraining.__CPU_DEVICE)
                data = data.to(ModelTraining.__CPU_DEVICE)
                target = target.to(ModelTraining.__CPU_DEVICE)

                # Compute forward pass again for this batch; this time on CPU.
                self.optimizer.zero_grad()
                output, loss2, _ = self.model(data, target)
                del output, _
                loss2.backward()
                temp_loss = loss2
                self.device = ModelTraining.__CPU_DEVICE

            losses.append(temp_loss.item())
            del temp_loss
            del data, target
            self.optimizer.step()
        if self.device != prev_device:
            self.device = prev_device
            # PUT STUFF BACK TO GPU FROM CPU: Model and optimizer only. Other variables MUST be deleted.
            print("Moving model back to {}".format(str(self.device)))
            self.model = self.model.to(self.device)
            for d in list(self.optimizer.state.values()):
                d[ModelTraining.__OPTIM_EXP_AVG] = d[ModelTraining.__OPTIM_EXP_AVG].to(self.device)
                d[ModelTraining.__OPTIM_EXP_AVG_SQ] = d[ModelTraining.__OPTIM_EXP_AVG_SQ].to(self.device)
        torch.cuda.empty_cache()
        return np.mean(losses, dtype=np.float64)


    def early_stopping_criteria(self, loss_each_epoch):
        """
        This method checks if the loss has changed by more than {epsilon} for {patience} number of epochs.
        :param loss_each_epoch: list containing loss for each epoch.
        :return: True if the loss has not changed and False otherwise
        """
        loss_diff = 0
        curr_loss = loss_each_epoch[-1]
        z = 0
        for loss in loss_each_epoch[-2::-1]:
            if (loss - curr_loss) < self.epsilon:
                loss_diff += 1
            z += 1
            if z == self.patience:
                break
        print("early stopping, loss_diff: {}".format(loss_diff))
        if loss_diff == self.patience:
            return True
        else:
            return False


    def test_model(self, test_batches_gen, save_attention=False, top_k_codes=8, top_n_attentions=4,
                   top_n_code_scores=100, is_top_n=False):
        """
        Predict and saves the scores of the predictions on the test data set.
        :param test_batches_gen: data_processing.data_loader.BatchGenerator object for held out test dataset
        :param save_attention: boolean for saving the attention.
        :param top_k_codes: The number of codes for which the attention should be saved.
        :param top_n_attentions: The size of the attention for each label. The total size of the attention will be
                                top_n_attention*kernel_size of the convolution layer.
        :param top_n_code_scores: the number of codes for which the scores should be saved.
        :param is_top_n: True, if the method of code prediction is top n. False, if the method of code prediction is
                         thresholding.
        :return: loss incurred by the model on the test set and a nested dictionary with key as note id and value as
                 another dictionary with key as code label and value as score.
        """
        if save_attention:
            loss_test, test_note_id_to_score = self.__test(test_batches_gen, save_attention=save_attention,
                                                           top_n_attentions=top_n_attentions, top_k_codes=top_k_codes,
                                                           save_output=True, top_n_code_scores=top_n_code_scores,
                                                           is_top_n=is_top_n)
        else:
            loss_test, test_note_id_to_score = self.__test(test_batches_gen, save_output=True, is_top_n=is_top_n)
        return loss_test, test_note_id_to_score


    def test_model_for_attention_weights(self, test_batches_gen, attention_select_codes):
        """
        Predicts and saves the scores of the predictions on the test data set with a focus on saving attention weights
        for select set of codes. Additionally, we use the thresholding method as opposed to the top n method for
        predicting a code.
        :param test_batches_gen: data_processing.data_loader.BatchGenerator object for held out test dataset
        :param attention_select_codes: codes for which we wish to obtain results for attention weights.
        :return: loss incurred by the model on the data set
        """
        return self.__test(test_batches_gen, save_attention=True, save_output=True, top_n_attentions=4,
                           is_top_n=False, attention_select_codes=attention_select_codes, prediction_threshold=0.5)


    def __test(self, data_gen, save_attention=False, save_output=False, top_k_codes=8, top_n_attentions=4,
               top_n_code_scores=100, is_top_n=False, attention_select_codes=(), prediction_threshold=0.5):
        """
        Predicts the probability of the labels for the {data_gen} data set.
        :param data_gen: data generator for which the loss is to be calculated
        :param save_attention: True, if the output attention should be saved to the {model_dir}
        :param save_output: True, if the output predictions should be saved to the {model_dir}
        :param top_k_codes: The number of codes for which the attention should be saved.
        :param top_n_attentions: The size of the attention for each label. The total size of the attention will be
                                top_n_attention*kernel_size of the convolution layer.
        :param top_n_code_scores: the number of codes for which the scores should be saved.
        :param is_top_n: True, if the method of code prediction is top n. False, if the method of code prediction is
                         thresholding.
        :param attention_select_codes: codes for which we wish to obtain results for attention weights.
        :param prediction_threshold: threshold value for the thresholding method of code prediction.
        :return: loss incurred by the model on the data set
        """
        losses = []
        self.model.eval()
        note_id_to_score = dict()
        note_id_to_attn_weight_tp = dict()
        note_id_to_attn_weight_fp = dict()
        note_id_to_attn_weight_fn = dict()
        prev_device = self.device
        for note_ids, data, target in tqdm(data_gen):
            with torch.no_grad():
                data = data.to(self.device)
                target = target.to(self.device)
                self.model.zero_grad()
                try:
                    output, loss, alpha = self.model(data, target)
                except RuntimeError:
                    # PUT STUFF FROM GPU TO CPU: model, data, target
                    print("Offloading computation to {}".format(str(ModelTraining.__CPU_DEVICE)))
                    self.model = self.model.to(ModelTraining.__CPU_DEVICE)
                    data = data.to(ModelTraining.__CPU_DEVICE)
                    target = target.to(ModelTraining.__CPU_DEVICE)

                    self.model.zero_grad()
                    output, loss, alpha = self.model(data, target)
                    self.device = ModelTraining.__CPU_DEVICE

                losses.append(loss.item())

                output = output.cpu().numpy()
                alpha = alpha.cpu().numpy()
                for i, note_id in enumerate(note_ids):
                    note_id_val = self.data_iterator.data_fields.ID.vocab.itos[note_id.item()]
                    out = output[i]
                    sorted_out_idx = np.argsort(out)
                    # If the way we compute true positive, false positive, etc. is based on "top n" statistics
                    # In this case, we assume that recall@n is the relevant metric and hence we only need to store
                    # top n code scores to file.
                    if is_top_n:
                        note_id_to_score[note_id_val] = get_code_scores(top_n_code_scores, out, self.data_iterator)
                    # This else statement assumes that we default to thresholding to compute true positive, false
                    # positive, etc. In this case, we assume that we care about both recall and precision for each
                    # code label individually. Hence we need to store code scores for all codes.
                    else:
                        note_id_to_score[note_id_val] = self.get_code_scores_all(out)
                    if save_attention:
                        true_label_idx = set([i for i, j in enumerate(target[i]) if j == 1])
                        attn_weights = alpha[i]
                        if is_top_n or attention_select_codes == ():
                            predicted_code_idxs = set(sorted_out_idx[-top_k_codes:])
                        else:
                            predicted_code_idxs = self.get_predicted_code_idxs(attention_select_codes, out,
                                                                               thresh=prediction_threshold)

                        code_idxs_tp = true_label_idx.intersection(predicted_code_idxs)
                        code_to_attention_tp = get_code_to_attention(self.data_iterator, self.kernel_size, code_idxs_tp,
                                                                     top_n_attentions, attn_weights, data[i],
                                                                     is_mention=True)
                        note_id_to_attn_weight_tp[note_id_val] = code_to_attention_tp

                        code_idxs_fp = predicted_code_idxs - true_label_idx
                        code_to_attention_fp = get_code_to_attention(self.data_iterator, self.kernel_size, code_idxs_fp,
                                                                     top_n_attentions, attn_weights, data[i],
                                                                     is_mention=True)
                        note_id_to_attn_weight_fp[note_id_val] = code_to_attention_fp

                        code_idxs_fn = true_label_idx - predicted_code_idxs
                        code_to_attention_fn = get_code_to_attention(self.data_iterator, self.kernel_size, code_idxs_fn,
                                                                     top_n_attentions, attn_weights, data[i],
                                                                     is_mention=True)
                        note_id_to_attn_weight_fn[note_id_val] = code_to_attention_fn

                del data, target, output, alpha
        # PUT STUFF BACK TO GPU FROM CPU: model
        if self.device != prev_device:
            self.device = prev_device
            print("Moving model back to {}".format(str(self.device)))
            self.model = self.model.to(self.device)

        if save_output:
            to_pickle(self.model_dir + "/test_scores.pickle", note_id_to_score)
            if save_attention:
                to_pickle(self.model_dir + "/test_attention_weights_tp.pickle", note_id_to_attn_weight_tp)
                to_pickle(self.model_dir + "/test_attention_weights_fp.pickle", note_id_to_attn_weight_fp)
                to_pickle(self.model_dir + "/test_attention_weights_fn.pickle", note_id_to_attn_weight_fn)

        torch.cuda.empty_cache()
        return np.mean(losses, dtype=np.float64), note_id_to_score


    def get_code_scores_all(self, output_scores):
        """
        This method selects code scores for all codes.
        :param output_scores: output score array containing the scores of all the code labels.
        :return: returns the dictionary containing the scores of all codes.
        """
        out_score = dict()
        for i in range(len(output_scores)):
            out_score[self.data_iterator.data_fields.LABEL.vocab.itos[i]] = output_scores[i]
        return out_score


    def get_predicted_code_idxs(self, code_labels, output_scores, thresh=0.5):
        """
        This method outputs the scores of the input code_labels in the out_score dict.
        :param code_labels: set of code labels for whom we wish to obtain predictions.
        :param output_scores: output score array containing the scores of all the code labels.
        :param thresh: threshold using for prediction of a code.
        :return: predicted_code_idxs, a subset of code indices corresponding to input code_labels such that the
                 only the predicted code_label indices are included.
        """
        predicted_code_idxs = set()
        for code in code_labels:
            code_idx = self.data_iterator.data_fields.LABEL.vocab.stoi[code]
            if output_scores[code_idx] >= thresh:
                predicted_code_idxs.add(code_idx)
        return predicted_code_idxs


    def save_model_params(self):
        """
        Saves the necessary model and class params to the json file at the {model_dir} directory
        """
        param_dict = self.__dict__.copy()
        param_dict.pop("data_iterator")
        # param_dict.pop("train_batches_gen")
        # param_dict.pop("test_batches_gen")
        # param_dict.pop("validate_batches_gen")
        param_dict.pop("model")
        param_dict.pop("optimizer")
        param_dict.pop("device")
        to_json(self.model_dir + "/params.json", param_dict)


    def save_model(self):
        """
        Saves the trained model at the {model_dir} directory
        """
        sd = self.model.cpu().state_dict()
        torch.save(sd, self.model_dir + SAVED_MODEL_NAME)
        self.model.to(self.device)


################################################################################################
################################################################################################
################################################################################################


################################################################################################
# logger instance to emit metrics for hyperparameter tuning.
################################################################################################
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

__THRESH_TEST_FREQ = 5
__THRESH_FREQ = 10


def get_note_id_to_list_labels_from(train_valid_examples, args):
    """
    From list of examples obtain a note_id to cpt_labels dictionary for use as ground truth labels for metric
    computation.
    :param train_valid_examples: list of torchtext.data.example.Example objects that contain note_id and cpt_labels as
                                 two of the attributes
    :param args: Command line arguments for entry_point.py script.                                 
    :return: dictionary with key as note_id and value as unique cpt labels.
    """
    temp = []
    for ex in train_valid_examples:
        if args.code_set == CPT_CODE_SET:
            temp.append({ex.note_id: ex.cpt_labels})
        elif args.code_set == ICD10_CODE_SET:
            temp.append({ex.note_id: ex.icd_labels})
        else:
            raise ValueError("code_set {} is not supported.".format(args.code_set))
    return dict(ChainMap(*temp))


def training(args):
    """
    Performs training/validation hyper parameter tuning loop with stratified k-fold cross validation.
    Also logs the average F1 metric which will be used for hyperparameter tuning by Amazon Sagemaker.
    :param args: Command line arguments for entry_point.py script.
    :return:
    """
    ## DATA LOADING.
    data_loader = DataLoader(args.specialty, args.code_set, rest_api=False)
    train_data, validate_data, _ = data_loader.load_datasets()
    train_valid_examples = train_data.examples + validate_data.examples
    y = []
    for example in train_valid_examples:
        if args.code_set == CPT_CODE_SET:
            y.append(frozenset(example.cpt_labels))
        elif args.code_set == ICD10_CODE_SET:
            y.append(frozenset(example.icd_labels))
        else:
            raise ValueError("code_set {} is not supported.".format(args.code_set))
    note_id_to_list_labels = get_note_id_to_list_labels_from(train_valid_examples, args)

    ## Stratified k-fold cross-validation.
    skf = StratifiedKFold(n_splits=5)
    average_recall_per_fold = []

    f_num = 1
    
    model_name = CNN
    
    print("\n\nRunning for model type: {}\n\n".format(model_name))
    
    for i, (train_indices, validate_indices) in enumerate(skf.split(y)):
        print("Fold number: {}/5".format(f_num))

        train_data.examples = [train_valid_examples[j] for j in train_indices]
        validate_data.examples = [train_valid_examples[j] for j in validate_indices]
        train_batches_gen, validate_batches_gen = data_loader.get_train_validate_batch_generators(
            args.batch_size,
            train_data,
            validate_data,
            sort=args.text_sort
        )
        
        model_training = ModelTraining(None, specialty=args.specialty, code_set=args.code_set,
                                       kernel_size=args.kernel_size, conv_out_size=args.conv_out_size,
                                       model_name=model_name, cuda_device_number="0", epochs=args.num_epochs,
                                       batch_size=args.batch_size, weight_decay=args.weight_decay,
                                       learning_rate=args.learning_rate, early_stopping=args.early_stopping,
                                       epsilon=args.epsilon, patience=args.patience, dropout=args.dropout,
                                       data_iterator=data_loader.data_iterator)

        loss_each_epoch_train_, loss_each_epoch_val_, model_directory, val_note_id_to_score = \
            model_training.train_model(train_batches_gen, validate_batches_gen)

        # Compute precision/recall/f1 results for val_note_id_to_score and save the best one along with the
        # corresponding model and hyperparameter choices.
        result_df = compute_metrics(args.specialty, args.code_set, note_id_to_list_labels,
                                    val_note_id_to_score)

        result_df = result_df[result_df[__TEST_FREQ] > __THRESH_TEST_FREQ]
        result_df.to_csv(model_directory + "/code_labels_f1_thresholds_{}_{}_cross_valid.csv".format(
            __THRESH_TEST_FREQ,
            __THRESH_FREQ),
                         index=False)
        average_recall_per_fold.append(np.mean(result_df[__RECALL]))
        f_num += 1

    average_recall = np.mean(average_recall_per_fold)
    ################################################################################################
    ################################################################################################
    # Note: Following is the corresponding code in the accompanying jupyter notebook,
    #       metric_definitions = [{'Name': 'average Recall score',
    #                              'Regex': 'Validation set: Average Recall score: ([0-9\\.]+)'}]
    # The above 'Regex' can only work given the following string to logger.info() function below.
    ################################################################################################
    ################################################################################################
    logger.info('Validation set: Average Recall score: {:.4f}\n'.format(average_recall))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Static Hyperparameters
    parser.add_argument("--specialty", type=str, default=DefaultHyperparameters.SPECIALTY, metavar="SP",
                        help="specialty (default: {})".format(DefaultHyperparameters.SPECIALTY))
    parser.add_argument("--code-set", type=str, default=DefaultHyperparameters.CODE_SET, metavar="CS",
                        help="code set (default: {})".format(DefaultHyperparameters.CODE_SET))

    # Model Hyperparameters
    parser.add_argument("--learning-rate", type=float, default=DefaultHyperparameters.LEARNING_RATE, metavar="LR",
                        help="learning rate (default: {})".format(DefaultHyperparameters.LEARNING_RATE))
    parser.add_argument("--batch-size", type=int, default=DefaultHyperparameters.BATCH_SIZE, metavar="BSZ",
                        help="batch size (default: {})".format(DefaultHyperparameters.BATCH_SIZE))
    parser.add_argument("--early-stopping", type=bool, default=DefaultHyperparameters.EARLY_STOPPING, metavar="ES",
                        help="early stopping (default: {})".format(DefaultHyperparameters.EARLY_STOPPING))
    parser.add_argument("--num-epochs", type=int, default=DefaultHyperparameters.NUM_EPOCHS, metavar="N",
                        help="num epochs (default:{})".format(DefaultHyperparameters.NUM_EPOCHS))
    parser.add_argument("--text-sort", type=bool, default=DefaultHyperparameters.TEXT_SORT, metavar="TS",
                        help="text sort (default: {})".format(DefaultHyperparameters.TEXT_SORT))
    parser.add_argument("--kernel-size", type=int, default=DefaultHyperparameters.KERNEL_SIZE, metavar="KS",
                        help="kernel size (default: {})".format(DefaultHyperparameters.KERNEL_SIZE))
    parser.add_argument("--conv-out-size", type=int, default=DefaultHyperparameters.CONV_OUT_SIZE, metavar="COS",
                        help="conv out size (default: {})".format(DefaultHyperparameters.CONV_OUT_SIZE))
    parser.add_argument("--dropout", type=float, default=DefaultHyperparameters.DROPOUT, metavar="DO",
                        help="dropout (default: {})".format(DefaultHyperparameters.DROPOUT))
    parser.add_argument("--weight-decay", type=float, default=DefaultHyperparameters.WEIGHT_DECAY, metavar="WD",
                        help="weight decay (default: {})".format(DefaultHyperparameters.WEIGHT_DECAY))
    parser.add_argument("--epsilon", type=float, default=DefaultHyperparameters.EPSILON_ES, metavar="EP",
                        help="epsilon (default: {})".format(DefaultHyperparameters.EPSILON_ES))
    parser.add_argument("--patience", type=int, default=DefaultHyperparameters.PATIENCE_ES, metavar="PES",
                        help="patience (default: {})".format(DefaultHyperparameters.PATIENCE_ES))

    # Sagemaker environment variables
    parser.add_argument("--model-dir", type=str, default=os.environ[SagemakerEnvironment.SM_MODEL_DIR])
    parser.add_argument("--data-dir", type=str, default=os.environ[SagemakerEnvironment.SM_CHANNEL_TRAINING])
    parser.add_argument("--num-gpus", type=int, default=os.environ[SagemakerEnvironment.SM_NUM_GPUS])

    training(parser.parse_args())
