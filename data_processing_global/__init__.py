import os
import pandas as pd
from utils.file_io_utils import from_pickle

dirname = os.path.dirname

# Currently using 2019 version for CPT and EM codes.
data_files_path = dirname(dirname(os.path.realpath(__file__))) + "/data_files_global/2019/"
cpt_level_1_codes = from_pickle(data_files_path + "cpt_level_1_codes.pickle")
cpt_level_2_codes = from_pickle(data_files_path + "cpt_level_2_codes.pickle")
set_cpt_codes = cpt_level_1_codes.union(cpt_level_2_codes)
set_em_codes = from_pickle(data_files_path + "em_codes.pickle")
cpt_level_1_code_description_df = pd.read_csv(data_files_path + "cpt_code_level_1_description.csv")
cpt_level_2_code_description_df = pd.read_csv(data_files_path + "cpt_code_level_2_description.csv")
# Column names for above data frames
__DESCRIPTOR = "Descriptor"
__CPT_CODE = "CPT Code"
__HCPCS_II_CODE = "HCPCS II Code"

# Currently using 2019 version for ICD10 codes.
data_files_path = dirname(dirname(os.path.realpath(__file__))) + "/data_files_global/2019/"
set_icd10_cm_codes = from_pickle(data_files_path + "icd_10_cm_codes.pickle")
set_icd10_dot_codes = from_pickle(data_files_path + "icd_10_dot_codes.pickle")
set_icd10_codes = set_icd10_cm_codes.union(set_icd10_dot_codes)
